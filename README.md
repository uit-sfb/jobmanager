# Jobmanager

This is an api service designed to manage jobs, executors and groups.

For a technical introduction to the architecture and concepts please see the [design document](docs/design.md).

## Docker service

First make sure that you have `METAPIPE_HOME` environment variable set. For instance, follow those steps:
 - add `export METAPIPE_HOME=~/.metapipe` at the end of your `~/.bashrc file`
 - execute `source ~/.bashrc`
 - close your terminal and open a new one
 


To start jobmanager as a Docker Compose service:
 - copy `docker/docker-compose.yml` somewhere and tailor it to your needs
 - copy `docker/conf_example.yml` to the directory bind-mounted in `docker-compose.yml` (`$METAPIPE_HOME/jobman` if you did not modify it) and rename it `conf.yml`.
 - run:
```
docker-compose -f <pathTo_docker-compose.yml> up -d
```

The GUI should then be available at `localhost:8080/gui` (or any port you re-mapped it to in docker-compose.yml).

Note: requires [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/) installed on the server.

## Build and launch

Build and launch the project using maven and running the jar file.

~~~
mvn clean package

java -jar target/jobmanager-1.0-SNAPSHOT.jar server src/test/resources/test-config.yml
~~~
### Authorization

To run with authorization activated set the `auth:disabled` flag to false in the config file at
_src/test/resources/test-config.yml_

If the `auth:disabled` flag is set to false, the tests will not work and has to be skipped, this can be done with

```mvn clean package -Dmaven.test.skip=true```

The authorization end-point can be configured to test against production environment, vagrant stack or localhost.
Change the _tokenIntrospectionEndpoint_ in the config file to the target end-point.

To assert that the test are working, disable the authentication.

Without authorization the jobmanager will accept all requests. It will log all the requests as either authenticated
or

```no.uit.metapipe.jobmanager.util.auth.AuthCtx: Allowing unauthorized request (authorization is disabled in config).```

### Database and migrations

#### Unit testing and local development

For testing, JobManager uses the H2 in-memory database for the
 convenience of the developer. In this case the schema for the in-memory
 database is auto generated on application startup.
 
The configuration setting for running H2 with in-memory auto-generated
schema looks like this:

    database:
        properties:
            hibernate.hbm2ddl.auto: create-drop
            hibernate.hbm2ddl.import_files: import.sql

#### Schema migration and testing for production deployment

The production database uses PostgreSQL and contains data for all the 
users. The production database schema can not be managed automatically 
by Hibernate and therefore needs to be migrated by an admin/developer
before the application is put into production so that the database schema
matches the schema that is expected by the application.

To make the schema migrations more manageable we use a tool called 
[Flyway](https://flywaydb.org/) to manage these migrations.

If another table is added to the database it can be useful to look at the
Hibernate generated SQL before writing the migration script. This will
be written to the application log on application startup when H2 is run
in the test configuration that is described above.

The database migrations can be found in 
[src/main/resources/db/migration](src/main/resources/db/migration).

Each migration is an SQL file with a unique version. The format is as 
follows: `V#.#__Name_of_migration.sql`. For example: `V1.0__Initial_schema.sql`.
Please note the double underscores (__).

When Flyway runs the migrations each script is run in succession, one after
another, so **it is very important that each migration script takes into account the state
that the database is in when the script is run**. The state will be determined by 
1) the previous migration scripts, 2) the data that is already in the database.

In order to run a database migration the following properties needs to 
be commented out of the config-file: `hibernate.hbm2ddl.auto`, `hibernate.hbm2ddl.import_files`.

This should lead to the following database configuration:

    database:
        properties:
            # hibernate.hbm2ddl.auto: create-drop
            # hibernate.hbm2ddl.import_files: import.sql

The steps required to test a migration is:

1. Update the configuration file as described above
2. Delete remains of previous database: `rm dev.db.*`
3. Run unit tests and verify that they fail `mvn test`
4. Run the migration scripts: `java -jar target/jobmanager-1.0-SNAPSHOT.jar db migrate src/test/resources/test-config.yml`
5. Run unit tests and verify that they pass `mvn test`

Before committing the changes to version control:

1. Revert the config-file to back to the test configuration: `git checkout src/test/resources/test-config.yml`
2. Ensure that all tests are passing: `mvn test`

**Please make sure that you do not check the config file with the commented out lines
into version control. It will cause headache and will most likely fail on Jenkins CI.**

### GUI usage

The jobmanager gui is available at `localhost:8080/gui`. 
To access the gui a valid Elixir AAI account has to be provided when logging in.

Three scripts are provided for creating jobs. These are located in _src/main/resources/simpleGUI/_

* testScript.sh - Launches the built jar file and populates the jobmanager - Does not work with authentication
* testScript_justCURL.sh - Only populates the jobmanager - Does not work with authentication
* testScript_JC_auth.sh - Only populates the jobmanager. Works with authentication. 
The environment variable $access_token has to be exported.
