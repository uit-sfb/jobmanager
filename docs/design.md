# Introduction

The META-pipe Job Manager sits in between the end-user facing GUI and the
pipeline executors (Execution Managers) as is illustrated in the following diagram:

![architecture](img/architecture.png)

The Job Manager functions as a data store for the GUI as well as a lifecycle
manager for the jobs that have been submitted by the end-user. The Job
Manager provides the following core features:

- API for GUI applications
- API for Execution Managers
- GUI and API for admin tasks such as restarting and cancelling of jobs

The purpose of the job service is to

- record information about a job submitted by an end-user
- make the job available to the Execution Managers
- aid a META-pipe administrator in detecting and handling failures and automatically
retry failed jobs when it makes sense
- isolate system failures from the end-user

## Failure handling

Due to the scale, walltime and complexity of jobs, failures at the Execution Manager level are quite common.
Here are some reasons for Execution Manager failure:

- Pipeline bugs
- Re-deployment of META-pipe (new version of bugfix)
- Configuration update
- Compute infrastructure reboots
- Worker walltime expires
- Invalid user input that passed initial input validation
- Shared filesystem becomes unavailable
- Power outage
- Data center flood (broken water pipe)

While most failed jobs can be recovered automatically by restarting the job at a later
time, there are some jobs that require manual intervention by an administrator
or a developer in order to fix a deployment issue or a bug.

In order to meet these requirements the Job Manager implements the following workflow:


![failure-workflow](img/failure_workflow.png)

### Failure detection

The following methods are employed to detect failures at the Execution
Managers:

- Execution Manager self-reporting: if an EM detects a failure it can
report it to the Job Manager and terminate its execution.
- Heart beat time-out: when the self-reporting doesn't work this is the
fallback mechanism that ensures that the failure is eventually detected.
Self-reporting may fail in different scenarios, including: power outage,
abrupt process termination (sigkill), network partitioning, out-of-memory errors.


## Protocol

A successful run:

![success-protocol](img/job_success.png)

In case of failure:

![success-failure](img/job_failure.png)


## Job and attempt lifecycles

In META-pipe each job relates to a sequence of attempts. The user creates
a Job and the Job Manager then creates an Attempt to do the job.

![job-attempts](img/job_attempt.png)

### Jobs

A META-pipe job is the end-user defined description of the desired result.
A job consists of input files and parameters. The parameters should be
as close to the GUI state as possible, as observed by the end user.

A "Job" is the entity that the end-user's GUI can see and interact with on 
the Job Manager via the /users API.

A jobs state is mostly inferred from the state of the latest attempt,
although the Cancelled state may be set explicitly by an administrator.
A job can never reach the Cancelled state automatically.
 
Note that the Finished and Cancelled states are both final.

![job-lifecycle](img/job_lifecycle.png)

### Attempts

A META-pipe attempt is a literal attempt at executing a job. An attempt is what is
picked up by the Execution Manager and it has its own state.

Below is a state diagram depicting the valid states of an attempt.
A newly created attempt starts by being queued on the Job Manager (queued locally).
Once it's picked up by the Execution Manager the state changes to Assigned Executor.
Once its been assigned to an executor it can reach any of the failure states as
well as any of the reachable states on the left column.

An attempt may be cancelled at any point by an administrator in order to
stop its execution.

Note that the cancelled state and the failure states are all final.

![attempt-lifecycle](img/attempt_lifecycle.png)

## Authentication and authorization

The Job Manager implements an OAuth 2.0 Resource Server as defined in
 [RFC-6749 The OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)

It expects an OAuth 2.0 Bearer Token embedded in each HTTP request and
it uses [RCF-7662 OAuth 2.0 Token Introspection](https://tools.ietf.org/html/rfc7662)
to contact our authorization server in order to get information about
the permissions that have been granted to each request.