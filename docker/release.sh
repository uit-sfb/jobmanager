#!/usr/bin/env bash

TAG=latest

DIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT_DIR=$(dirname "$DIR")
docker build -t registry.gitlab.com/uit-sfb/jobmanager:$TAG -f "$ROOT_DIR/docker/Dockerfile" "$ROOT_DIR"
docker push registry.gitlab.com/uit-sfb/jobmanager:$TAG