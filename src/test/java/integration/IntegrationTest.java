package integration;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import no.uit.metapipe.jobmanager.api.*;
import no.uit.metapipe.jobmanager.core.AttemptState;

import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class IntegrationTest extends AbstractIntegrationTest {

    // Users ===========================================================================================================

    @Test
    public void userJobSuccessfulTest() {

        Response response;
        List<UserJobDto> jobList;
        String userID = randomString(10);
        String jobID = randomString(10);

        response = getGetUserJobs(userID);

        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(0);

        response = putCreateUserJob(userID, jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        response = getGetUserJobs(userID);
        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(1);

        response = getGetUserJob(userID, jobID);
        assertThat(response.readEntity(UserJobDto.class).getJobId()).isEqualTo(jobID);

        response = putCreateUserJob(userID, randomString(10));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        response = getGetUserJobs(userID);
        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(2);

    }

    @Test
    public void userJobAlreadyExistsTest() {

        Response response;
        List<UserJobDto> jobList;
        String userName = randomString(10);
        String jobID = randomString(10);

        response = getGetUserJobs(userName);

        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(0);

        response = putCreateUserJob(userName, jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        response = getGetUserJobs(userName);
        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(1);

        response = getGetUserJob(userName, jobID);
        assertThat(response.readEntity(UserJobDto.class).getJobId()).isEqualTo(jobID);

        response = putCreateUserJob(userName, jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CONFLICT.getStatusCode());

        response = getGetUserJobs(userName);
        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList.size()).isEqualTo(1);

    }

    @Test
    public void userJobCreateParametersTest() {

        Response response;
        String userID = randomString(10);
        String jobID = randomString(10);

        response = putCreateUserJob(userID, null);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateUserJob(userID, "kjkj space");
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateUserJob(userID, "kjkj\nkjhkj");
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateUserJob(null, jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateUserJob("kjkj space", jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateUserJob("kjkj\nkjhkj", jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

    }

    // Groups ==========================================================================================================

    // TODO: implement when groups methods are implemented

    // Jobs ============================================================================================================

    @Test
    public void getJobsSuccessfulTest() {

        Response response;
        List<FullJobDto> jobList;
        boolean temp = false;

        String jobID = randomString(10);
        putCreateUserJob(randomString(10), jobID);

        response = getGetJobs();
        jobList = response.readEntity(new GenericType<List<FullJobDto>>(){});
        for (FullJobDto aJobList : jobList) {
            if (aJobList.getJobId().equals(jobID)) {
                temp = true;
            }
        }
        assertThat(temp).isTrue();

        response = getGetJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

    }

    @Test
    public void getJobNotFound() {

        Response response;
        List<FullJobDto> jobList;
        String jobID = randomString(10);
        boolean temp = false;

        response = getGetJobs();
        jobList = response.readEntity(new GenericType<List<FullJobDto>>(){});
        for (FullJobDto aJobList : jobList) {
            if (aJobList.getJobId().equals(jobID)) {
                temp = true;
            }
        }
        assertThat(temp).isFalse();

        response = getGetJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void cancelJobSuccessful() {

        Response response;
        String userName = randomString(10);
        String jobID = randomString(10);

        putCreateUserJob(userName, jobID);
        response = postCancelJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

    }

    @Test
    public void cancelJobNotFound() {

        Response response;
        String jobID = randomString(10);

        response = postCancelJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void restartJobSuccessful() {

        Response response;
        String userName = randomString(10);
        String jobID = randomString(10);

        putCreateUserJob(userName, jobID);
        for (int i = 0; i < 10; i++) {
            putCreateAttempt(jobID, randomString(10), createTestAttempt(randomString(10), randomString(10)));
        }
        response = postRestartJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        List<AttemptDto> attempts;
        response = getGetAttempts(jobID);
        attempts = response.readEntity(new GenericType<List<AttemptDto>>(){});

        assertThat(attempts.size()).isEqualTo(11);
        assertThat(attempts.get(10).getState()).isNotEqualTo(AttemptState.CANCELLED.name());
        for (int i = 0; i < 10; i++) {
            assertThat(attempts.get(i).getState()).isIn(AttemptState.CANCELLED.name());
        }

    }

    @Test
    public void restartJobFails() {

        Response response;
        String userName = randomString(10);
        String jobID = randomString(10);

        putCreateUserJob(userName, jobID);
        postCancelJob(jobID);
        response = postRestartJob(jobID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());

    }

    @Test
    public void holdJobSuccessful() {
        Response response;
        String userName = randomString(10);
        String jobId = randomString(10);

        putCreateUserJob(userName, jobId);
        response = getGetUserJob(userName, jobId);
        UserJobDto job = response.readEntity(UserJobDto.class);
        assertThat(!job.getHold());
        response = putHoldJob(jobId, "true");
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        response = getGetUserJob(userName, jobId);
        job = response.readEntity(UserJobDto.class);
        assertThat(job.getHold());
    }

    @Test
    public void holdJobFails() {
        Response response;
        String userName = randomString(10);
        String jobId = randomString(10);

        putCreateUserJob(userName, jobId);
        response = putHoldJob(jobId, "this_should_be_wrong");
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        response = getGetUserJob(userName, jobId);
        UserJobDto job = response.readEntity(UserJobDto.class);
        assertThat(!job.getHold());
    }

    // Attempts =========================================================================================================

    @Test
    public void attemptsSuccessfulTests() {

        Response response;
        String attemptId = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);
        AttemptDto a;

        putCreateUserJob(randomString(10), jobID);

        response = putCreateAttempt(jobID, attemptId, createTestAttemptDto(attemptId, executorID));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        List<AttemptDto> attempts;
        response = getGetAttempts(jobID);
        attempts = response.readEntity(new GenericType<List<AttemptDto>>(){});
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(attempts.get(0).getAttemptId()).isEqualTo(attemptId);
        assertThat(attempts.get(0).getState()).isEqualTo(AttemptState.QUEUED_LOCALLY.name());

        response = getGetAttempt(jobID, attemptId);
        a = response.readEntity(AttemptDto.class);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(a.getAttemptId()).isEqualTo(attemptId);
        assertThat(a.getState()).isEqualTo(AttemptState.QUEUED_LOCALLY.name());

        response = putSetAttemptState(jobID, attemptId, AttemptState.EXECUTOR_FAILED.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.CONFLICT.getStatusCode());

        response = getGetAttemptState(jobID, attemptId);
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.QUEUED_LOCALLY.name());
    }

    @Test
    public void attemptCreateJobNotFound() {

        Response response;
        String attemptId = randomString(10);

        response = putCreateAttempt(randomString(10), attemptId, createTestAttemptDto(attemptId, randomString(10)));
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void attemptCreateParametersTest() {

        Response response;
        String jobID = randomString(10);
        String attemptId = randomString(10);

        putCreateUserJob(randomString(10), jobID);

        response = putCreateAttempt(jobID, attemptId, createTestAttempt(attemptId, null));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        response = putCreateAttempt(jobID, null, createTestAttempt(attemptId, randomString(10)), true);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateAttempt(jobID, "kjkj space", createTestAttempt(attemptId, randomString(10)), true);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        response = putCreateAttempt(jobID, "kjkj\nkjhkj", createTestAttempt(attemptId, randomString(10)), true);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

    }

    @Test
    public void attemptCreateAlreadyExists() {

        Response response;
        String userID = randomString(10);
        String jobID = randomString(10);
        String attemptID = randomString(10);
        String executorID = randomString(10);

        putCreateUserJob(userID, jobID);
        response = putCreateAttempt(jobID, attemptID, createTestAttemptDto(attemptID, executorID));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        response = putCreateAttempt(jobID, attemptID, createTestAttemptDto(attemptID, executorID));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CONFLICT.getStatusCode());

    }

    @Test
    public void attemptNotFound() {

        Response response;
        String userName = randomString(10);
        String jobID = randomString(10);
        String attemptId = randomString(10);

        putCreateUserJob(userName, jobID);
        response = getGetAttempt(jobID, attemptId);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void attemptSetGetStateNotFound() {

        Response response;
        String userName = randomString(10);
        String jobID = randomString(10);
        String attemptId = randomString(10);

        putCreateUserJob(userName, jobID);
        response = putSetAttemptState(jobID, attemptId, AttemptState.EXECUTOR_FAILED.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        response = getGetAttemptState(jobID, attemptId);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    // Executors =======================================================================================================


    @Test
    public void executorQueueGetNonexistingExecutor() {

        List<ExecutorJobDto> executorJobDtoS;
        Response response;

        response = getGetExecutorQueue(randomString(10));
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
    }

    @Test
    public void executorAttemptGetSuccessful() {

        AttemptDto a;
        Response response;
        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = getGetExecutorAttempt(executorID, jobID, attemptID);
        a = response.readEntity(AttemptDto.class);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(a).isNotNull();
        assertThat(a.getAttemptId()).isEqualTo(attemptID);
        assertThat(a.getExecutorId()).isEqualTo(executorID);

    }

    @Test
    public void executorAttemptGetNotFound() {

        Response response;

        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = getGetExecutorAttempt(randomString(10), jobID, attemptID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = getGetExecutorAttempt(executorID, randomString(10), attemptID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = getGetExecutorAttempt(executorID, jobID, randomString(10));
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void executorAttemptProperties() {
        String attemptID = randomString(10);
        String executorID = "test-executor";
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        Map<String, String> properties = new HashMap<>();
        properties.put("test", "test");

        Response createPropertyResponse = client.target(String.format("http://localhost:%d/executors/test-executor/jobs/%s/attempts/%s/properties", RULE.getLocalPort(), jobID, attemptID))
                .request()
                .post(Entity.json(properties));
        assertThat(createPropertyResponse.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());

        AttemptDto result = getGetExecutorAttempt(executorID, jobID, attemptID).readEntity(AttemptDto.class);
        assertThat(result.getProperties()).isEqualTo(properties);
    }

    @Test
    public void executorAttemptStateGetSuccessful() {

        Response response;

        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = getGetExecutorAttemptState(executorID, jobID, attemptID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.QUEUED_LOCALLY.name());

    }

    @Test
    public void executorAttemptStateGetNotFound() {

        Response response;

        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = getGetExecutorAttemptState(randomString(10), jobID, attemptID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = getGetExecutorAttemptState(executorID, randomString(10), attemptID);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = getGetExecutorAttemptState(executorID, jobID, randomString(10));
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void executorAttemptStateSetSuccessful() {

        Response response;
        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = putSetExecutorAttemptState(executorID, jobID, attemptID, AttemptState.ASSIGNED_EXECUTOR.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.ASSIGNED_EXECUTOR.name());

    }

    @Test
    public void executorAttemptStateSetNotFound() {

        Response response;

        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = putSetExecutorAttemptState(randomString(10), jobID, attemptID, AttemptState.CANCELLED.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = putSetExecutorAttemptState(executorID, randomString(10), attemptID, AttemptState.FINISHED.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = putSetExecutorAttemptState(executorID, jobID, randomString(10), AttemptState.WORKFLOW_FAILED.name());
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    @Test
    public void executorOutputSetSuccessful() {

        ObjectNode j = new ObjectNode(JsonNodeFactory.instance);
        DatasetRefDto datasetRefDto;
        Response response;
        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        j.put("readBearerToken", "ReadBearerToken");
        j.put("writeBearerToken", "WriteBearerToken");
        j.put("url", "someurl");
        response = putSetOutputDataset(executorID, jobID, attemptID, "somekey", j);
        datasetRefDto = response.readEntity(DatasetRefDto.class);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(datasetRefDto).isNotNull();
        assertThat(datasetRefDto).isNotEqualTo(Optional.empty());
        assertThat(datasetRefDto.getReadBearerToken()).isEqualTo("ReadBearerToken");
        assertThat(datasetRefDto.getWriteBearerToken()).isEqualTo("WriteBearerToken");
        assertThat(datasetRefDto.getUrl()).isEqualTo("someurl");

    }

    @Test
    public void executorOutputSetNotFound() {

        ObjectNode j = new ObjectNode(JsonNodeFactory.instance);
        Response response;

        j.put("readBearerToken", "ReadBearerToken");
        j.put("writeBearerToken", "WriteBearerToken");
        j.put("url", "someurl");

        String attemptID = randomString(10);
        String executorID = randomString(10);
        String jobID = randomString(10);

        createAndPutUserJobAndAttempt(jobID, attemptID, executorID);

        response = putSetOutputDataset(randomString(10), jobID, attemptID, "somekey", j);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = putSetOutputDataset(executorID, randomString(10), attemptID, "somekey", j);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        response = putSetOutputDataset(executorID, jobID, randomString(10), "somekey", j);
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }



}
