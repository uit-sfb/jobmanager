package integration;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;
import no.uit.metapipe.jobmanager.api.AttemptDto;
import no.uit.metapipe.jobmanager.api.DatasetRefDto;
import no.uit.metapipe.jobmanager.api.UserJobDto;
import no.uit.metapipe.jobmanager.core.Attempt;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import static org.assertj.core.api.Assertions.assertThat;

import static no.uit.metapipe.jobmanager.util.Util.randomString;

public abstract class AbstractIntegrationTest {
    String tag = "test";
    Client client;

    @ClassRule
    public static DropwizardAppRule<JobManagerConfig> RULE = new IntegrationTestFixture();

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() {
        this.client.close();
    }

    UserJobDto createTestJob(String jobID, String userID, String groupID) {
        UserJobDto job = new UserJobDto();
        job.setJobId(jobID);
        job.setTag(tag);
        job.setUserId(userID);
        job.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")));
        job.setInputs(new HashMap<String, DatasetRefDto>(){{
            DatasetRefDto d = new DatasetRefDto(); d.setUrl("http://storage/input.fas"); put("input.fas", d);
        }});
        job.setGroupId(groupID);

        return job;
    }

    Attempt createTestAttempt(String attemptID, String executorID) {
        Attempt attempt = new Attempt();
        attempt.setAttemptId(attemptID);
        attempt.setExecutorId(executorID);
        attempt.setTag("test");
        return attempt;
    }

    AttemptDto createTestAttemptDto(String attemptID, String executorID) {
        AttemptDto attempt = new AttemptDto();
        attempt.setAttemptId(attemptID);
        attempt.setExecutorId(executorID);
        attempt.setTag("test");
        return attempt;
    }

    void createAndPutUserJobAndAttempt(String jobID, String attemptID, String executorID) {
        putCreateUserJob(randomString(10), jobID);
        putCreateAttempt(jobID, attemptID, createTestAttemptDto(attemptID, executorID));
    }

    Response putCreateUserJob(String userID, String jobID) {
        return client.target(String.format("http://localhost:%d/users/" + userID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(createTestJob(jobID, userID, ""), MediaType.APPLICATION_JSON_TYPE));
    }

    Response putHoldJob(String jobId, String value) {
        return client.target(String.format("http://localhost:%d/jobs/"+ jobId + "/hold", RULE.getLocalPort()))
                .request()
                .put(Entity.entity(value, MediaType.TEXT_PLAIN));
    }

    Response getGetUserJobs(String userID) {
        return client.target(String.format("http://localhost:%d/users/" + userID + "/jobs", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetUserJob(String userID, String jobID) {
        return client.target(String.format("http://localhost:%d/users/" + userID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetJobs() {
        return client.target(String.format("http://localhost:%d/jobs", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetJob(String jobID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetAttempt(String jobID, String attemptID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID, RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetAttempts(String jobID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response putCreateAttempt(String jobID, String attemptID, Attempt attempt, boolean allowBadRequest) {
        Response response = client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(AttemptDto.fromAttempt(attempt, true, true), MediaType.APPLICATION_JSON_TYPE));
        if(!allowBadRequest) {
            assertThat(response.getStatus()).isNotEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
        return response;
    }

    Response putCreateAttempt(String jobID, String attemptID, Attempt attempt) {
        return putCreateAttempt(jobID, attemptID, attempt, false);
    }

    Response putCreateAttempt(String jobID, String attemptID, AttemptDto attempt) {
        Response response = client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(attempt, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus()).isNotEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        return response;
    }

    Response getGetAttemptState(String jobID, String attemptID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID + "/state", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response putSetAttemptState(String jobID, String attemptID, String state) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID + "/state", RULE.getLocalPort()))
                .request()
                .put(Entity.text(state));
    }

    Response postCancelJob(String jobID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/cancel", RULE.getLocalPort()))
                .request()
                .post(Entity.text(""));
    }

    Response postRestartJob(String jobID) {
        return client.target(String.format("http://localhost:%d/jobs/" + jobID + "/restart", RULE.getLocalPort()))
                .request()
                .post(Entity.text(""));
    }

    Response getGetExecutorQueue(String executorID) {
        Response response = client.target(String.format("http://localhost:%d/executors/" + executorID + "/queue", RULE.getLocalPort()))
                .queryParam("tags", "test")
                .request()
                .get();
        assertThat(response.getStatus()).isNotEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        return response;
    }

    Response getAllLocallyQueuedAttemptsQueue() {
        return client.target(String.format("http://localhost:%d/executors/queue", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response postPopExecutorQueue(String executorID, int limit) {
        Response response = client.target(String.format("http://localhost:%d/executors/" + executorID + "/queue", RULE.getLocalPort()))
                .queryParam("limit", limit)
                .queryParam("tags", "test")
                .request()
                .post(Entity.text(""));
        assertThat(response.getStatus()).isNotEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        return response;
    }

    Response getGetExecutorAttempt(String executorID, String jobID, String attemptID) {
        return client.target(String.format("http://localhost:%d/executors/" + executorID + "/jobs/" + jobID + "/attempts/" + attemptID, RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetExecutorAttemptState(String executorID, String jobID, String attemptID) {
        return client.target(String.format("http://localhost:%d/executors/" + executorID + "/jobs/" + jobID + "/attempts/" + attemptID + "/state", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response putSetExecutorAttemptState(String executorID, String jobID, String attemptID, String state) {
        return client.target(String.format("http://localhost:%d/executors/" + executorID + "/jobs/" + jobID + "/attempts/" + attemptID + "/state", RULE.getLocalPort()))
                .request()
                .put(Entity.text(state));
    }

    Response putSetOutputDataset(String executorID, String jobID, String attemptID, String datasetKey, ObjectNode j) {
        return client.target(String.format("http://localhost:%d/executors/" + executorID + "/jobs/" + jobID + "/attempts/" + attemptID + "/outputs/" + datasetKey, RULE.getLocalPort()))
                .request()
                .put(Entity.json(j));
    }

    Response getGetGroupJobs(String groupID) {
        return client.target(String.format("http://localhost:%d/groups/" + groupID + "/jobs", RULE.getLocalPort()))
                .request()
                .get();
    }

    Response getGetGroupJob(String groupID, String jobID) {
        return client.target(String.format("http://localhost:%d/groups/" + groupID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .get();
    }

    Response putCreateGroupJob(String groupID, String jobID) {
        return client.target(String.format("http://localhost:%d/groups/" + groupID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(createTestJob(jobID, "", groupID), MediaType.APPLICATION_JSON_TYPE));
    }

}
