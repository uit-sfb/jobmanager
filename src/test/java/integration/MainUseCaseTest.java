package integration;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;
import no.uit.metapipe.jobmanager.api.*;
import no.uit.metapipe.jobmanager.core.*;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.HashMap;
import java.util.List;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class MainUseCaseTest {

    private Client client;

    @ClassRule
    public static DropwizardAppRule<JobManagerConfig> RULE = new IntegrationTestFixture();

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() {
        this.client.close();
    }

    @Test
    public void mainUseCaseTest() {
        String tag = "ffwd";
        String userID = randomString(10);
        String jobID = randomString(10);
        String attemptID = randomString(10);
        String executorID = randomString(10);
        UserJobDto job = new UserJobDto();
        UserJobDto userJobDto;
        AttemptDto attemptDto = new AttemptDto();
        attemptDto.setTag(tag);
        List<UserJobDto> jobList;
        List<ExecutorJobDto> executorJobDtoS;
        ObjectNode j = new ObjectNode(JsonNodeFactory.instance);
        Response response;

        // User creates and submits a job
        job.setJobId(jobID);
        job.setTag(tag);
        job.setUserId(userID);
        job.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")));
        job.setInputs(new HashMap<String, DatasetRefDto>(){{
            DatasetRefDto d = new DatasetRefDto(); d.setUrl("http://storage.metapipe.uit.no/users/TestUser/job-1/input.fas"); put("input.fas", d);
        }});
        job.getProperties().put("test.property", "hello world");

        response = client.target(String.format("http://localhost:%d/users/" + userID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(job, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        // Hack: JobManager creates an attempt for the job
        attemptDto.setAttemptId(attemptID);
        response = client.target(String.format("http://localhost:%d/jobs/" + jobID + "/attempts/" + attemptID, RULE.getLocalPort()))
                .request()
                .put(Entity.entity(attemptDto, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        // An executor takes the attemps from the queue
        response = client.target(String.format("http://localhost:%d/executors/" + executorID + "/queue", RULE.getLocalPort()))
                .queryParam("limit", -1)
                .queryParam("tags", tag)
                .request()
                .post(Entity.text(""));
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS.size()).isEqualTo(1);
        assertThat(executorJobDtoS.get(0).getInputs().keySet().contains("input.fas"));
        attemptDto = executorJobDtoS.get(0).getAttempt();
        assertThat(attemptDto).isNotNull();
        assertThat(attemptDto.getState()).isEqualTo(AttemptState.ASSIGNED_EXECUTOR.name());
        assertThat(attemptDto.getPriority()).isEqualTo(1);

        response = client.target(String.format(executorJobDtoS.get(0).getAttemptUri() + "/state", RULE.getLocalPort()))
                .request()
                .put(Entity.text(AttemptState.QUEUED_EXECUTOR.name()));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.QUEUED_EXECUTOR.name());

        // Executor starts the job
        response = client.target(String.format(executorJobDtoS.get(0).getAttemptUri() + "/state", RULE.getLocalPort()))
                .request()
                .put(Entity.text(AttemptState.RUNNING.name()));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.RUNNING.name());

        // User asks JobManager for job to check status
        response = client.target(String.format("http://localhost:%d/users/" + userID + "/jobs", RULE.getLocalPort()))
                .request()
                .get();
        jobList = response.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(jobList).isNotNull();
        assertThat(jobList.size()).isEqualTo(1);
        assertThat(jobList.get(0).getState()).isEqualTo(JobState.SUBMITTED.name());
        response = client.target(String.format("http://localhost:%d/users/" + userID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        userJobDto = response.readEntity(UserJobDto.class);
        assertThat(userJobDto.getState()).isEqualTo(JobState.SUBMITTED.name());

        // Executor finishes the job
        j.put("url", "http://storage.metapipe.uit.no/users/TestUser/job-1/output.gb");
        response = client.target(String.format(executorJobDtoS.get(0).getAttemptUri() + "/outputs/" + "output.gb", RULE.getLocalPort()))
                .request()
                .put(Entity.json(j));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        j.put("url", "http://storage.metapipe.uit.no/users/TestUser/job-1/output-merged.gb");
        response = client.target(String.format(executorJobDtoS.get(0).getAttemptUri() + "/outputs/" + "output-merged.gb", RULE.getLocalPort()))
                .request()
                .put(Entity.json(j));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        response = client.target(String.format(executorJobDtoS.get(0).getAttemptUri() + "/state", RULE.getLocalPort()))
                .request()
                .put(Entity.text(AttemptState.FINISHED.name()));
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(AttemptState.FINISHED.name());

        // User checks finished job
        response = client.target(String.format("http://localhost:%d/users/" + userID + "/jobs/" + jobID, RULE.getLocalPort()))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        userJobDto = response.readEntity(UserJobDto.class);
        assertThat(userJobDto.getState()).isEqualTo(JobState.FINISHED.name());
        assertThat(userJobDto.getOutputs().keySet().contains("output.gb")).isTrue();
        assertThat(userJobDto.getOutputs().keySet().contains("output-merged.gb")).isTrue();
        assertThat(userJobDto.getProperties().get("test.property")).isEqualTo("hello world");
    }

}
