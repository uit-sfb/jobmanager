package integration;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.jobmanager.JobManagerApplication;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;

public class IntegrationTestFixture extends DropwizardAppRule<JobManagerConfig> {
    public IntegrationTestFixture() {
        super(JobManagerApplication.class, ResourceHelpers.resourceFilePath("test-config.yml"));
    }
}
