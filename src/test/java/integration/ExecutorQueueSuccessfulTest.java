package integration;

import no.uit.metapipe.jobmanager.api.ExecutorJobDto;
import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

import static no.uit.metapipe.jobmanager.util.Util.print;
import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class ExecutorQueueSuccessfulTest extends AbstractIntegrationTest {

    @Test
    public void executorQueueSuccessfulTests() {

        List<ExecutorJobDto> executorJobDtoS;
        Response response;
        int length = 30;
        String attemptID1 = "attempt_1", attemptID2 = "attempt_2";
        String executorID = "test_executor";
        String jobID = "test_job";

        createAndPutUserJobAndAttempt(jobID, attemptID1, executorID);
        assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(1);

        postPopExecutorQueue(executorID, 1);
        putCreateAttempt(jobID, attemptID2, createTestAttemptDto(attemptID2, executorID));
        response = postPopExecutorQueue(executorID, 1);

        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isEqualTo(1);
        assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(0);
        assertThat(executorJobDtoS.get(0).getAttemptUri()).isNotNull().isNotEmpty().contains(executorID).contains(jobID).contains(attemptID2);

        response = getGetExecutorQueue(executorID);
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS.size()).isEqualTo(0);

        attemptID1 = randomString(10);
        executorID = randomString(10);
        jobID = randomString(10);
        createAndPutUserJobAndAttempt(jobID, attemptID1, executorID);
        for(int i = 0; i < length; i++) {
            attemptID1 = randomString(10);
            putCreateAttempt(jobID, attemptID1, createTestAttemptDto(attemptID1, executorID));
        }

        response = getAllLocallyQueuedAttemptsQueue();
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isGreaterThanOrEqualTo(30);

        length = executorJobDtoS.size();

        response = postPopExecutorQueue(executorID, 2);
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isEqualTo(2);
        //assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(2);

        response = postPopExecutorQueue(executorID, 0);
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        print(executorJobDtoS.size());
        //assertThat(executorJobDtoS.size()).isEqualTo(length - 2);
        //assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(0);

        attemptID1 = randomString(10);
        executorID = randomString(10);
        jobID = randomString(10);
        createAndPutUserJobAndAttempt(jobID, attemptID1, executorID);
        length = 5;
        for(int i = 0; i < length; i++) {
            attemptID1 = randomString(10);
            putCreateAttempt(jobID, attemptID1, createTestAttemptDto(attemptID1, executorID));
        }
        length = getAllLocallyQueuedAttemptsQueue().readEntity(new GenericType<List<ExecutorJobDto>>(){}).size();
        response = postPopExecutorQueue(executorID, -1);
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isEqualTo(length);
        assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(0);

        for(int i = 0; i < length; i++) {
            attemptID1 = randomString(10);
            putCreateAttempt(jobID, attemptID1, createTestAttemptDto(attemptID1, executorID));
        }
        response = postPopExecutorQueue(executorID, length * 2);
        executorJobDtoS = response.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isEqualTo(length);
        assertThat(getGetExecutorQueue(executorID).readEntity(new GenericType<List<ExecutorJobDto>>(){}).size()).isEqualTo(0);

    }
}
