package no.uit.metapipe.jobmanager.workers;

import no.uit.metapipe.jobmanager.config.JobStateManagerConfig;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import no.uit.metapipe.jobmanager.service.JobManagerService;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class StateObserverImplTest {
    JobStateManagerConfig config = new JobStateManagerConfig();

    JobRepo mockJobRepo;
    AttemptRepo mockAttemptRepo;
    JobManagerService mockJobManagerService;

    StateObserverImpl stateObserver;
    AuthCtx authCtx = AuthCtx.disabled();

    @Before
    public void before() {
        mockJobRepo = mock(JobRepo.class);
        mockAttemptRepo = mock(AttemptRepo.class);
        mockJobManagerService = mock(JobManagerService.class);
        stateObserver = new StateObserverImpl(mockJobRepo, mockAttemptRepo, config, mockJobManagerService);
    }

    Attempt timedOutAttempt(AttemptState initialState) {
        Attempt attempt = Attempt.fromInitialState(initialState);
        attempt.setAttemptId("timed-out-attempt");
        attempt.setLastHeartbeat(Date.from(Instant.EPOCH));

        Job job = new Job();
        job.setJobId("test-job");
        job.getAttempts().add(attempt);
        attempt.setJob(job);
        return attempt;
    }

    void timedOutShouldBeDetected(AttemptState state) throws Exception {
        stub(mockAttemptRepo.saveOrUpdate(any())).toReturn(null);

        Attempt attempt = timedOutAttempt(state);
        stateObserver.dispatch(attempt);

        assertThat(attempt.getState()).isEqualTo(AttemptState.TIMED_OUT);
        verify(mockAttemptRepo).saveOrUpdate(attempt);
    }

    void nonTimedOutShouldBeUnchanged(AttemptState state) throws Exception {
        stub(mockAttemptRepo.saveOrUpdate(any())).toReturn(null);

        Attempt attempt = timedOutAttempt(state);
        attempt.updateHeartbeat();
        stateObserver.dispatch(attempt);
        assertThat(attempt.getState()).isEqualTo(state);
    }

    @Test
    public void newJobsShouldProduceAnAttempt() throws Exception {
        Job job = spy(new Job());
        job.setJobId("test-job");

        stateObserver.onNewJob(job);
        assertThat(job.getAttempts().size()).isEqualTo(1);

        verify(job).createAttempt(any());
        verify(mockJobRepo).saveOrUpdate(job);
    }

    @Test
    public void assignedExecutorAttemptsShouldTimeOut() throws Exception {
        AttemptState state = AttemptState.ASSIGNED_EXECUTOR;
        timedOutShouldBeDetected(state);
        nonTimedOutShouldBeUnchanged(state);
    }

    @Test
    public void queuedExecutorAttemptsShouldTimeOut() throws Exception {
        AttemptState state = AttemptState.QUEUED_EXECUTOR;
        timedOutShouldBeDetected(state);
        nonTimedOutShouldBeUnchanged(state);
    }

    @Test
    public void runningAttemptsShouldTimeOut() throws Exception {
        AttemptState state = AttemptState.RUNNING;
        timedOutShouldBeDetected(state);
        nonTimedOutShouldBeUnchanged(state);
    }

    @Test
    public void timedOutJobsShouldBeRestarted() throws Exception {
        Attempt attempt = timedOutAttempt(AttemptState.TIMED_OUT);
        int numAttemptsBefore = attempt.getJob().getAttempts().size();
        stateObserver.onTimedOut(attempt);
        int numAttemptsAfter = attempt.getJob().getAttempts().size();
        assertThat(numAttemptsAfter == numAttemptsBefore + 1);
    }
}
