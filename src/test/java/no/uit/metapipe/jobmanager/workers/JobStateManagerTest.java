package no.uit.metapipe.jobmanager.workers;

import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class JobStateManagerTest {
    JobRepo mockJobRepo;
    AttemptRepo mockAttemptRepo;
    StateObserver mockObserver;
    JobStateManagerThread jobStateManagerThread;

    @Before
    public void before() {
        mockJobRepo = mock(JobRepo.class);
        mockAttemptRepo = mock(AttemptRepo.class);
        mockObserver = spy(StateObserver.class);
        jobStateManagerThread = new JobStateManagerThread(mockJobRepo, mockAttemptRepo, mockObserver);
    }

    Attempt registerAttemptWithMock(AttemptState state) {
        Attempt attempt = Attempt.fromInitialState(state);
        List<Attempt> attemptList = new LinkedList<>();
        attemptList.add(attempt);
        //when(mockAttemptRepo.findByState(state, -1)).thenReturn(attemptList);
        when(mockAttemptRepo.findByFinalized(false, 100)).thenReturn(attemptList);
        return attempt;
    }

    @Test
    public void allHandlersShouldBeCalled() throws Exception {
        // To scheduler
        Attempt attempt = registerAttemptWithMock(AttemptState.QUEUED_LOCALLY);
        jobStateManagerThread.run();
        verify(mockObserver).onQueuedLocally(attempt);

        // Failing/cancelling states
        attempt = registerAttemptWithMock(AttemptState.TIMED_OUT);
        jobStateManagerThread.run();
        verify(mockObserver).onTimedOut(attempt);

        attempt = registerAttemptWithMock(AttemptState.WORKFLOW_FAILED);
        jobStateManagerThread.run();
        verify(mockObserver).onWorkflowFailed(attempt);

        attempt = registerAttemptWithMock(AttemptState.EXECUTOR_FAILED);
        jobStateManagerThread.run();
        verify(mockObserver).onExecutorFailed(attempt);

        // Assigned executor
        attempt = registerAttemptWithMock(AttemptState.ASSIGNED_EXECUTOR);
        jobStateManagerThread.run();
        verify(mockObserver).onAssignedExecutor(attempt);

        attempt = registerAttemptWithMock(AttemptState.QUEUED_EXECUTOR);
        jobStateManagerThread.run();
        verify(mockObserver).onQueuedExecutor(attempt);

        attempt = registerAttemptWithMock(AttemptState.RUNNING);
        jobStateManagerThread.run();
        verify(mockObserver).onRunning(attempt);
    }
}
