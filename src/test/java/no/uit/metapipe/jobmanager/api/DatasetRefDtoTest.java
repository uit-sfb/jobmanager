package no.uit.metapipe.jobmanager.api;

import no.uit.metapipe.jobmanager.core.DatasetRef;
import org.junit.Test;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class DatasetRefDtoTest {

    @Test
    public void datasetRefValidTest() {

        DatasetRef datasetRef = new DatasetRef();
        DatasetRefDto datasetRefDto;

        datasetRef.setKey(randomString(10));
        datasetRef.setUrl(randomString(10));
        datasetRef.setReadBearerToken(randomString(10));
        datasetRef.setWriteBearerToken(randomString(10));

        datasetRefDto = DatasetRefDto.fromDatasetRef(datasetRef, false, false);
        assertThat(datasetRefDto).isNotNull();
        assertThat(datasetRefDto.getUrl()).isEqualTo(datasetRef.getUrl());
        assertThat(datasetRefDto.getReadBearerToken()).isNull();
        assertThat(datasetRefDto.getWriteBearerToken()).isNull();

        datasetRefDto = DatasetRefDto.fromDatasetRef(datasetRef, true, false);
        assertThat(datasetRefDto.getReadBearerToken()).isNotNull();
        assertThat(datasetRefDto.getReadBearerToken()).isEqualTo(datasetRef.getReadBearerToken());
        assertThat(datasetRefDto.getWriteBearerToken()).isNull();

        datasetRefDto = DatasetRefDto.fromDatasetRef(datasetRef, false, true);
        assertThat(datasetRefDto.getReadBearerToken()).isNull();
        assertThat(datasetRefDto.getWriteBearerToken()).isNotNull();
        assertThat(datasetRefDto.getWriteBearerToken()).isEqualTo(datasetRef.getWriteBearerToken());

    }

}
