package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import org.junit.Test;

import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class FullJobDtoTest {

    @Test
    public void fullJobDtoValidTest() {

        Job testJob = new Job();
        FullJobDto jobDto;
        Map<String, DatasetRef> inputs = new HashMap<String, DatasetRef>(){{
            DatasetRef d = new DatasetRef(); d.setUrl("http://storage/input.fas");
            put("input.fas", d);
        }};
        Map<String, DatasetRef> outputs = new HashMap<>();
        Attempt attempt1 = new Attempt();
        List<Attempt> attempts = new LinkedList<>();

        testJob.setJobId(randomString(10));
        testJob.setGroupId(randomString(10));
        testJob.setUserId(randomString(10));
        testJob.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")).toString());

        jobDto = FullJobDto.fromJob(testJob);

        assertThat(jobDto).isNotNull();
        assertThat(jobDto.getJobId()).isEqualTo(testJob.getJobId());
        assertThat(jobDto.getGroupId()).isEqualTo(testJob.getGroupId());
        assertThat(jobDto.getTimeSubmitted()).isEqualTo(Long.toString(testJob.getCreationTimestamp().getTime()));
        assertThat(jobDto.getState()).isNotNull();
        assertThat(jobDto.getState()).isNotEmpty();
        assertThat(jobDto.getParameters()).isNotNull();
        assertThat(jobDto.getParametersAsString()).isEqualTo(testJob.getParameters());
        assertThat(jobDto.getInputs()).isEmpty();
        assertThat(jobDto.getOutputs()).isEmpty();
        assertThat(jobDto.getAttempts()).isEmpty();

        testJob.setInputDatasets(inputs);

        outputs.put("output.fas", new DatasetRef());
        attempt1.setOutputDatasets(outputs);
        attempts.add(attempt1);
        attempts.add(attempt1);
        testJob.setAttempts(attempts);

        jobDto = FullJobDto.fromJob(testJob);

        assertThat(jobDto.getInputs()).isNotEmpty();
        assertThat(jobDto.getInputs().get("input.fas")).isNotNull();
        assertThat(jobDto.getOutputs()).isNotEmpty();
        assertThat(jobDto.getOutputs().get("output.fas")).isNotNull();
        assertThat(jobDto.getAttempts()).isNotEmpty();
        assertThat(jobDto.getAttempts().size()).isEqualTo(2);
        assertThat(jobDto.getAttempts().get(0)).isNotNull();

    }

}
