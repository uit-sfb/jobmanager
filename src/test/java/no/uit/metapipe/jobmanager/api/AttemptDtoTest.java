package no.uit.metapipe.jobmanager.api;

import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static no.uit.metapipe.jobmanager.api.AttemptDto.fromAttempt;
import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AttemptDtoTest {

    @Test
    public void attemptDtoValidTest() throws Exception {
        Attempt attempt = spy(new Attempt());
        when(attempt.getCreationTimestamp()).thenReturn(new Date());

        AttemptDto attemptDto;
        Map<String, DatasetRef> outputs = new HashMap<>();

        attempt.setExecutorId(randomString(10));
        attempt.setAttemptId(randomString(10));

        attemptDto = fromAttempt(attempt, true, true);
        assertThat(attemptDto).isNotNull();
        assertThat(attemptDto.getExecutorId()).isEqualTo(attempt.getExecutorId());
        assertThat(attemptDto.getState()).isEqualTo(attempt.getState().name());
        assertThat(attemptDto.getAttemptId()).isEqualTo(attempt.getAttemptId());
        assertThat(attemptDto.getLastHeartbeat()).isNull();
        assertThat(attemptDto.getOutputs()).isEmpty();

        attempt.setLastHeartbeat(new Date());
        outputs.put("output.fas", new DatasetRef());
        outputs.put("output1.fas", new DatasetRef());
        attempt.setOutputDatasets(outputs);

        attemptDto = fromAttempt(attempt, true, true);
        assertThat(attemptDto.getLastHeartbeat()).isEqualTo(Long.toString(attempt.getLastHeartbeat().getTime()));
        assertThat(attemptDto.getOutputs()).isNotEmpty();
        assertThat(attemptDto.getOutputs().size()).isEqualTo(2);
        assertThat(attemptDto.getOutputs().get("output.fas")).isNotNull();
    }
}
