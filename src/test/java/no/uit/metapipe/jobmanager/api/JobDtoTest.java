package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import no.uit.metapipe.jobmanager.core.Job;
import org.junit.Test;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class JobDtoTest {

    @Test
    public void jobDtoValidTest() {
        Job testJob = new Job();
        UserJobDto jobDto;

        testJob.setJobId(randomString(10));
        testJob.setGroupId(randomString(10));
        testJob.setUserId(randomString(10));
        testJob.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")).toString());

        jobDto = JobDto.fromJob(testJob);

        assertThat(jobDto).isNotNull();
        assertThat(jobDto.getJobId()).isEqualTo(testJob.getJobId());
        assertThat(jobDto.getGroupId()).isEqualTo(testJob.getGroupId());
        assertThat(jobDto.getTimeSubmitted()).isEqualTo(Long.toString(testJob.getCreationTimestamp().getTime()));
        assertThat(jobDto.getState()).isNotNull();
        assertThat(jobDto.getState()).isNotEmpty();
        assertThat(jobDto.getParameters()).isNotNull();
        assertThat(jobDto.getParametersAsString()).isEqualTo(testJob.getParameters());
    }

    @Test
    public void jobDtoInvalidJSONParametersTest() {
        Job testJob = new Job();
        IllegalStateException error = null;

        testJob.setJobId(randomString(10));
        testJob.setGroupId(randomString(10));
        testJob.setUserId(randomString(10));
        testJob.setParameters(randomString(20));

        try {
            JobDto.fromJob(testJob);
        } catch (IllegalStateException e) {
            error = e;
        }
        assertThat(error).isNotNull();
    }

}
