package no.uit.metapipe.jobmanager.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.testing.junit.ResourceTestRule;
import no.uit.metapipe.jobmanager.config.AuthConfig;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;
import no.uit.metapipe.jobmanager.api.*;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.exceptions.AlreadyExistsException;
import no.uit.metapipe.jobmanager.exceptions.AuthorizationException;
import no.uit.metapipe.jobmanager.exceptions.NotFoundException;
import no.uit.metapipe.jobmanager.service.JobManagerService;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import no.uit.metapipe.jobmanager.util.auth.OAuth2IntrospectionAuthenticator;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsLastArg;
import static org.mockito.Mockito.*;

public class JobManagerResourceTest {

    private static JobManagerService jobService = mock(JobManagerService.class);
    private static JobManagerConfig jobManagerConfig = mock(JobManagerConfig.class);

    private static final String stranger = "stranger";
    private static final String goodUser1 = "goodUser1";
    private static final String userID2 = "goodUser2";
    private static final String badJob = "badJob";
    private static final String goodJob1 = "goodJob1";
    private static final String goodJob2 = "goodJob2";
    private static final String baddAttempt = "badAttempt";
    private static final String goodAttempt1 = "goodAttempt1";
    private static final String goodAttempt2 = "goodAttempt2";
    private static final String executorTetris = "tetris";
    private static final String executorSkynet = "skynet";
    private static final String tag = "tag";

    private static Job job2AlreadyExists;

    private static ResourceTestRule getResourceTestRule() {
        AuthFilter<String, Principal> oauthCredentialAuthFilter = new OAuthCredentialAuthFilter.Builder<>()
                .setAuthenticator(new OAuth2IntrospectionAuthenticator(new AuthConfig(true)))
                .setPrefix("Bearer")
                .buildAuthFilter();
        return ResourceTestRule.builder()
                .addProvider(new AuthDynamicFeature(oauthCredentialAuthFilter))
                .addProvider(new AuthValueFactoryProvider.Binder<>(AuthCtx.class))
                .addResource(new JobManagerResource(jobService, jobManagerConfig))
                .build();
    }

    @ClassRule
    public static final ResourceTestRule resources = getResourceTestRule();

    @Before
    public void setup() throws AuthorizationException {
        AuthCtx authCtx = AuthCtx.disabled();

        Set<String> tags = new HashSet<>();
        tags.add("test");

        reset(jobService);

        Job job1 = createTestJob(goodJob1, goodUser1, null);
        job2AlreadyExists = createTestJob(goodJob2, userID2, null);

        Attempt attempt1 = createTestAttempt(goodJob1, goodUser1, goodAttempt1, executorSkynet);
        Attempt attempt2AlreadyExists = createTestAttempt(goodJob1, userID2, goodAttempt2, executorSkynet);
        List<Attempt> attemptList = Arrays.asList(attempt1, attempt2AlreadyExists);

        job1.getAttempts().add(attempt1);
        job1.getAttempts().add(attempt2AlreadyExists);
        try {
            when(jobManagerConfig.getAuth()).thenReturn(new AuthConfig(true));
            when(jobService.findJobById(authCtx, badJob)).thenReturn(Optional.empty());
            when(jobService.findJobById(authCtx, goodJob1)).thenReturn(Optional.of(job1));
            when(jobService.findJobById(authCtx, goodJob2)).thenReturn(Optional.of(job2AlreadyExists));
            when(jobService.findJobByUserIdAndJobId(anyObject(), Matchers.eq(goodUser1), Matchers.eq(goodJob1))).thenReturn(Optional.of(job1));
            when(jobService.findJobByUserIdAndJobId(anyObject(), Matchers.eq(stranger), Matchers.eq(goodJob1))).thenReturn(Optional.empty());
            when(jobService.findJobByUserIdAndJobId(anyObject(), Matchers.eq(goodUser1), Matchers.eq(badJob))).thenReturn(Optional.empty());
            when(jobService.findJobsByUserId(anyObject(), Matchers.eq(stranger))).thenReturn(new LinkedList<>());
            when(jobService.findJobsByUserId(anyObject(), eq(goodUser1))).thenReturn(Collections.singletonList(job1));
            when(jobService.findAllJobs(authCtx)).thenReturn(Arrays.asList(job1, job2AlreadyExists));
            when(jobService.getExecutorQueue(anyObject(), eq(executorSkynet), eq(tags))).thenReturn(attemptList);
            when(jobService.getExecutorQueue(anyObject(), eq(executorTetris), eq(tags))).thenReturn(Collections.emptyList());
            when(jobService.popExecutorQueue(anyObject(), eq(executorSkynet), eq(tags), eq(2))).thenReturn(attemptList);
            when(jobService.popExecutorQueue(anyObject(), eq(executorSkynet), eq(tags), eq(1))).thenReturn(Collections.singletonList(attempt1));
            when(jobService.findExecutorAttemptByIds(anyObject(), eq(executorSkynet), eq(goodJob1), eq(goodAttempt1))).thenReturn(Optional.of(attempt1));
            when(jobService.findExecutorAttemptByIds(anyObject(), Matchers.eq(executorTetris), Matchers.eq(badJob), Matchers.eq(baddAttempt))).thenReturn(Optional.empty());
            when(jobService.createAttempt(authCtx, badJob, goodAttempt1, tag, Optional.of(executorSkynet))).thenThrow(new NotFoundException());
            when(jobService.createAttempt(authCtx, badJob, goodAttempt2, tag, Optional.of(executorSkynet))).thenThrow(new NotFoundException());
            when(jobService.createAttempt(authCtx, goodJob1, goodAttempt1, tag, Optional.of(executorSkynet))).thenReturn(attempt1);
            when(jobService.createAttempt(authCtx, goodJob1, goodAttempt2, tag, Optional.of(executorSkynet))).thenThrow(new AlreadyExistsException());
            when(jobService.updateAttemptState(eq(authCtx), Matchers.eq(goodJob1), Matchers.eq(baddAttempt), any(String.class))).thenThrow(new NotFoundException());
            when(jobService.updateAttemptState(eq(authCtx), Matchers.eq(goodJob1), Matchers.eq(goodAttempt1), any(String.class))).then(returnsLastArg());
            when(jobService.updateAttemptState(eq(authCtx), Matchers.eq(goodJob1), Matchers.eq(goodAttempt2), any(String.class))).then(returnsLastArg());
            doThrow(new NotFoundException()).when(jobService).cancelJob(authCtx, badJob);
            when(jobService.updateAttemptStateForExecutor(Matchers.eq(authCtx), Matchers.eq(executorSkynet), Matchers.eq(goodJob1), Matchers.eq(goodAttempt1), any(String.class))).then(returnsLastArg());
            when(jobService.updateAttemptStateForExecutor(Matchers.anyObject(), Matchers.eq(executorTetris), Matchers.eq(badJob), Matchers.eq(baddAttempt), any(String.class))).thenThrow(new NotFoundException());
            doThrow(new NotFoundException()).when(jobService).setOutputDataset(Matchers.anyObject(), Matchers.eq(executorTetris), Matchers.eq(badJob), Matchers.eq(baddAttempt), any(DatasetRef.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        when(jobService.findAttemptByIds(authCtx, goodJob1, baddAttempt)).thenReturn(Optional.empty());
        when(jobService.findAttemptByIds(authCtx, goodJob1, goodAttempt1)).thenReturn(Optional.of(attempt1));
        when(jobService.findAttemptByIds(authCtx, goodJob1, goodAttempt2)).thenReturn(Optional.of(attempt2AlreadyExists));

    }

    private UserJobDto createTestUserJobDto(String jobID, String userID, String groupID) {
        Job j = createTestJob(jobID, userID, groupID);
        UserJobDto job = UserJobDto.fromJob(j);
        job.setTimeSubmitted(Long.toString((new Date()).getTime()));
        try {
            job.setParameters((new ObjectMapper()).readTree(j.getParameters()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return UserJobDto.fromJob(createTestJob(jobID, userID, groupID));
    }

    private Job createTestJob(String jobID, String userID, String groupID) {
        Job job = spy(new Job());
        when(job.getCreationTimestamp()).thenReturn(new Date());

        job.setJobId(jobID);
        job.setUserId(userID);
        job.setTag(tag);
        job.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")).toString());
        job.setInputDatasets(new HashMap<String, DatasetRef>(){{
            DatasetRef d = new DatasetRef(); d.setUrl("http://storage/input.fas");
            put("input.fas", d);
        }});
        job.setGroupId(groupID);

        return job;
    }

    private Attempt createTestAttempt(String jobID, String userID, String attemptID, String executorID) {
        Attempt attempt = spy(new Attempt());
        when(attempt.getCreationTimestamp()).thenReturn(new Date());

        attempt.setAttemptId(attemptID);
        attempt.setTag("test");
        attempt.setExecutorId(executorID);
        attempt.setJob(createTestJob(jobID, userID, null));
        return attempt;
    }

    private AttemptDto createTestAttemptDto(String jobID, String userID, String attemptID, String executorID) {
        return AttemptDto.fromAttempt(createTestAttempt(jobID, userID, attemptID, executorID), true, true);
    }

    private void testOneGoodJobParameters(JobDto job) {
        assertThat(job.getUserId()).isEqualTo(goodUser1);
        assertThat(job.getJobId()).isEqualTo(goodJob1);
        assertThat(job.getParameters()).isNotEmpty();
        assertThat(job.getTimeSubmitted()).isNotEmpty();
        if(job.getClass().isInstance(UserJobDto.class)) {
            assertThat(((UserJobDto)job).getInputs()).isNotEmpty();
        }
    }

    // Users ===========================================================================================================

    @Test
    public void successful_userjob_creation_and_retrieving() {

        UserJobDto userJob;
        List<UserJobDto> userJobList;
        Response r;

        // createUserJob
        assertThat(resources.client().target("/users/" + goodUser1 + "/jobs/" + goodJob1)
                .request()
                .put(Entity.entity(createTestUserJobDto(goodJob1, goodUser1, null), MediaType.APPLICATION_JSON_TYPE))
                .getStatus())
                .isEqualTo(Response.Status.CREATED.getStatusCode());

        // getUserJob
        r = resources.client().target("/users/" + goodUser1 + "/jobs/" + goodJob1).request().get();
        userJob = r.readEntity(UserJobDto.class);
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        testOneGoodJobParameters(userJob);

        // getUserJobs
        r = resources.client().target("/users/" + goodUser1 + "/jobs/").request().get();
        userJobList = r.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(userJobList).isNotNull();
        assertThat(userJobList).isNotEmpty();
        testOneGoodJobParameters(userJobList.get(0));

    }

    @Test
    public void failing_userjob_creation_and_retrieving() {

        List<UserJobDto> userList;
        Response r;

        // getUserJob: userID or jobID not found
        r = resources.client().target("/users/" + stranger + "/jobs/" + goodJob1).request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        r = resources.client().target("/users/" + goodUser1 + "/jobs/" + badJob).request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // getUserJobs: empty list
        r = resources.client().target("/users/" + stranger + "/jobs/").request().get();
        userList = r.readEntity(new GenericType<List<UserJobDto>>(){});
        assertThat(userList).isNotNull();
        assertThat(userList).isEmpty();

    }

    // Groups ==========================================================================================================

    // TODO

    // Jobs ============================================================================================================

    @Test
    @Ignore
    public void successful_jobs_tests() {

        FullJobDto fullJob;
        List<FullJobDto> jobList;
        AttemptDto attempt;
        Response r;

        // getJob
        r = resources.client().target("/jobs/" + goodJob1).request().get();
        fullJob = r.readEntity(FullJobDto.class);
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        testOneGoodJobParameters(fullJob);

        // getJobs
        r = resources.client().target("/jobs").request().get();
        jobList = r.readEntity(new GenericType<List<FullJobDto>>(){});
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(jobList).isNotNull();
        assertThat(jobList).isNotEmpty();
        testOneGoodJobParameters(jobList.get(0));

        // createAttempt
        assertThat(resources.client().target("/jobs/" + goodJob1 + "/attempts/" + goodAttempt1)
                .request()
                .put(Entity.entity(createTestAttemptDto(goodJob1, goodUser1, goodAttempt1, executorSkynet), MediaType.APPLICATION_JSON_TYPE))
                .getStatus())
                .isEqualTo(Response.Status.CREATED.getStatusCode());



        // getAttempt
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + goodAttempt2).request().get();
        attempt = r.readEntity(AttemptDto.class);
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(attempt).isNotNull();
        assertThat(attempt.getAttemptId()).isEqualTo(goodAttempt2);

        // getAttemptState
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + goodAttempt1 + "/state").request().get();
        attempt.setState(r.readEntity(String.class));
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(attempt).isNotNull();
        assertThat(attempt.getState()).isEqualTo(AttemptState.QUEUED_LOCALLY.name());

        // setAttemptState
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + goodAttempt1 + "/state").request().put(Entity.text(AttemptState.EXECUTOR_FAILED.name()));
        assertThat(r.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(r.readEntity(String.class)).isEqualTo(AttemptState.EXECUTOR_FAILED.name());

        // cancelJob
        r = resources.client().target("/jobs/" + goodJob1 + "/cancel").request().post(Entity.text(""));
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

    }

    @Test
    @Ignore
    public void failing_jobs_tests() {

        Response r;

        // getJob
        r = resources.client().target("/jobs/" + badJob).request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // createAttempt
        assertThat(resources.client().target("/jobs/" + badJob + "/attempts/" + goodAttempt1)
                .queryParam("tags", "test")
                .request()
                .put(Entity.entity(createTestAttemptDto(badJob, goodUser1, goodAttempt1, executorSkynet), MediaType.APPLICATION_JSON_TYPE))
                .getStatus())
                .isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(resources.client().target("/jobs/" + goodJob1 + "/attempts/" + goodAttempt2)
                .queryParam("tags", "test")
                .request()
                .put(Entity.entity(createTestAttemptDto(goodJob1, goodUser1, goodAttempt2, executorSkynet), MediaType.APPLICATION_JSON_TYPE))
                .getStatus())
                .isEqualTo(Response.Status.CONFLICT.getStatusCode());

        // getAttempt
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + baddAttempt).request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // getAttemptState
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + baddAttempt + "/state").request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // setAttemptState
        r = resources.client().target("/jobs/" + goodJob1 + "/attempts/" + baddAttempt + "/state").request().put(Entity.text(AttemptState.EXECUTOR_FAILED.name()));
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // cancelJob
        r = resources.client().target("/jobs/" + badJob + "/cancel").request().post(Entity.text(""));
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

    // Executors =======================================================================================================

    @Test
    @Ignore
    public void successful_executors_tests() {

        List<ExecutorJobDto> executorJobDtoS;
        Response r;
        AttemptDto a;
        DatasetRefDto datasetRefDto;
        ObjectNode j = new ObjectNode(JsonNodeFactory.instance);

        // getExecutorQueue
        r = resources.client().target("/executors/" + executorSkynet + "/queue").request().get();
        executorJobDtoS = r.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        //assertThat(executorJobDtoS).isNotEmpty();
        //assertThat(executorJobDtoS.size()).isEqualTo(2);
        //assertThat(executorJobDtoS.get(0).getAttempt().getAttemptId()).isEqualTo(goodAttempt1);
        //assertThat(executorJobDtoS.get(0).getJobId()).isEqualTo(goodJob1);

        // popExecutorQueue
        r = resources.client().target("/executors/" + executorSkynet + "/queue").queryParam("limit", "2").request().post(Entity.json(null));
        executorJobDtoS = r.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isNotEmpty();
        assertThat(executorJobDtoS.size()).isEqualTo(2);
        assertThat(executorJobDtoS.get(1).getAttempt().getAttemptId()).isEqualTo(goodAttempt2);
        assertThat(executorJobDtoS.get(1).getJobId()).isEqualTo(goodJob1);
        r = resources.client().target("/executors/" + executorSkynet + "/queue").queryParam("limit", "1").request().post(Entity.json(null));
        executorJobDtoS = r.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS.size()).isEqualTo(1);
        assertThat(executorJobDtoS.get(0).getAttempt().getAttemptId()).isEqualTo(goodAttempt1);

        // getExecutorAttempt
        r = resources.client().target("/executors/" + executorSkynet + "/jobs/" + goodJob1 + "/attempts/" + goodAttempt1).request().get();
        a = r.readEntity(AttemptDto.class);
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(a).isNotNull();
        assertThat(a.getAttemptId()).isEqualTo(goodAttempt1);
        assertThat(a.getExecutorId()).isEqualTo(executorSkynet);

        // getExecutorAttemptState
        r = resources.client().target("/executors/" + executorSkynet + "/jobs/" + goodJob1 + "/attempts/" + goodAttempt1 + "/state").request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(r.readEntity(String.class)).isEqualTo(AttemptState.QUEUED_LOCALLY.name());

        // setExecutorAttemptState
        r = resources.client().target("/executors/" + executorSkynet + "/jobs/" + goodJob1 + "/attempts/" + goodAttempt1 + "/state").request().put(Entity.text(AttemptState.RUNNING.name()));
        assertThat(r.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(r.readEntity(String.class)).isEqualTo(AttemptState.RUNNING.name());

        // setOutputDataset
        j.put("readBearerToken", "ReadBearerToken");
        j.put("writeBearerToken", "WriteBearerToken");
        j.put("url", "someurl");
        r = resources.client().target("/executors/" + executorSkynet + "/jobs/" + goodJob1 + "/attempts/" + goodAttempt1 + "/outputs/somekey").request().put(Entity.json(j));
        datasetRefDto = r.readEntity(DatasetRefDto.class);
        assertThat(r.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(datasetRefDto).isNotNull();
        assertThat(datasetRefDto).isNotEqualTo(Optional.empty());
        assertThat(datasetRefDto.getReadBearerToken()).isEqualTo("ReadBearerToken");
        assertThat(datasetRefDto.getWriteBearerToken()).isEqualTo("WriteBearerToken");
        assertThat(datasetRefDto.getUrl()).isEqualTo("someurl");

    }

    @Test
    public void failing_executors_tests() {

        List<ExecutorJobDto> executorJobDtoS;
        Response r;

        // getExecutorQueue
        r = resources.client().target("/executors/" + executorTetris + "/queue").queryParam("tags", "test").request().get();
        assertThat(r.getStatus() == 200);
        executorJobDtoS = r.readEntity(new GenericType<List<ExecutorJobDto>>(){});
        assertThat(executorJobDtoS).isNotNull();
        assertThat(executorJobDtoS).isEmpty();

        // getExecutorAttempt
        r = resources.client().target("/executors/" + executorTetris + "/jobs/" + badJob + "/attempts/" + baddAttempt).request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // getExecutorAttemptState
        r = resources.client().target("/executors/" + executorTetris + "/jobs/" + badJob + "/attempts/" + baddAttempt + "/state").request().get();
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // setExecutorAttemptState
        r = resources.client().target("/executors/" + executorTetris + "/jobs/" + badJob + "/attempts/" + baddAttempt + "/state").request().put(Entity.text(AttemptState.RUNNING.name()));
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // setOutputDataset
        r = resources.client().target("/executors/" + executorTetris + "/jobs/" + badJob + "/attempts/" + baddAttempt + "/outputs/somekey").request().put(Entity.json(new ObjectNode(JsonNodeFactory.instance)));
        assertThat(r.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

    }

}
