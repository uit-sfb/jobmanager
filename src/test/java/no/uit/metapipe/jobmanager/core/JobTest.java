package no.uit.metapipe.jobmanager.core;

import no.uit.metapipe.jobmanager.exceptions.IllegalStateTransitionException;
import org.junit.Test;

import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static org.assertj.core.api.Assertions.assertThat;

public class JobTest {

    private String attemptID;
    private Job testJob;
    private Map<String, DatasetRef> outputDatasets;

    private void setAttemptStateAndAssertJobState(Job testJob, Attempt attempt, AttemptState aState, JobState expectedJobState) {
        try {
            attempt.setState(aState);
        } catch (IllegalStateTransitionException e) {
            throw new IllegalArgumentException("Invalid state parameter aState: " + aState, e);
        }
        assertThat(testJob.getState()).isEqualTo(expectedJobState);
    }

    private Attempt createAndInitAttempt() {
        Attempt a = new Attempt();
        a.setAttemptId(attemptID);
        a.setExecutorId(randomString(10));
        a.setJob(testJob);
        a.setCreationTimestamp(new Date());
        a.setOutputDatasets(outputDatasets);

        return a;
    }

    private Attempt createRunningAttempt() {
        Attempt attempt = createAndInitAttempt();
        try {
            attempt.setState(AttemptState.ASSIGNED_EXECUTOR);
            attempt.setState(AttemptState.RUNNING);
        } catch (IllegalStateTransitionException e) {
            throw new IllegalStateException("should not happen", e);
        }
        return attempt;
    }

    @Test
    public void jobValidTest() {

        List<Attempt> attempts = new LinkedList<>();
        Attempt attempt;
        DatasetRef dRef = new DatasetRef();
        long dRefID = Long.parseLong(randomString(5, "1234567890"));
        String dRefKey = randomString(10);

        testJob = new Job();
        testJob.setAttempts(attempts);
        testJob.setCreationTimestamp(new Date());

        assertThat(testJob.getState()).isEqualTo(JobState.SUBMITTED);
        assertThat(testJob.getJobOutputs()).isEmpty();
        assertThat(testJob.getLatestAttempt()).isEqualTo(Optional.empty());

        outputDatasets = new HashMap<>();

        outputDatasets.put(dRefKey, dRef);

        attemptID = "job_valid_attempt_1";
        attempt = createAndInitAttempt();
        attempts.add(attempt);

        attemptID = "job_valid_attempt_2";
        attempt = createAndInitAttempt();
        attempts.add(attempt);
        assertThat(testJob.getLatestAttempt()).isNotEqualTo(Optional.empty());
        assertThat(testJob.getLatestAttempt().get().getAttemptId()).isEqualTo(attemptID);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.ASSIGNED_EXECUTOR, JobState.SUBMITTED);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.RUNNING, JobState.SUBMITTED);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.FINISHED, JobState.FINISHED);

        attempt = createRunningAttempt();
        attempts.add(attempt);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.EXECUTOR_FAILED, JobState.DELAYED);

        attempt = createRunningAttempt();
        attempts.add(attempt);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.TIMED_OUT, JobState.DELAYED);

        attempt = createRunningAttempt();
        attempts.add(attempt);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.WORKFLOW_FAILED, JobState.DELAYED);

        attempt = createRunningAttempt();
        attempts.add(attempt);
        setAttemptStateAndAssertJobState(testJob, attempt, AttemptState.CANCELLED, JobState.DELAYED);

        assertThat(testJob.getJobOutputs()).isNotEmpty();
        assertThat(testJob.getJobOutputs().containsKey(dRefKey)).isTrue();
    }
}
