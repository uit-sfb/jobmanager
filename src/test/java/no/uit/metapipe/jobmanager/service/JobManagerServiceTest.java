package no.uit.metapipe.jobmanager.service;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.DatasetRefRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import no.uit.metapipe.jobmanager.db.TagInfoRepo;
import no.uit.metapipe.jobmanager.exceptions.AlreadyExistsException;
import no.uit.metapipe.jobmanager.exceptions.AuthorizationException;
import no.uit.metapipe.jobmanager.exceptions.JobManagerException;
import no.uit.metapipe.jobmanager.exceptions.NotFoundException;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsLastArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JobManagerServiceTest {

    private static JobRepo jobRepo = mock(JobRepo.class);
    private static AttemptRepo attemptRepo = mock(AttemptRepo.class);
    private static DatasetRefRepo datasetRefRepo = mock(DatasetRefRepo.class);
    private static TagInfoRepo tagInfoRepo = new TagInfoRepo(new ArrayList<>());
    private static final JobManagerService jobManagerService = new JobManagerService(jobRepo, attemptRepo, datasetRefRepo, tagInfoRepo);
    private static final AuthCtx authCtx = AuthCtx.disabled();

    private static final String userID0 = "stranger";
    private static final String userID1 = "goodUser1";
    private static final String userID2 = "goodUser2";
    private static final String jobID0 = "badJob";
    private static final String jobID1 = "goodJob1";
    private static final String jobID2 = "goodJob2";
    private static final String attemptID0 = "badAttempt";
    private static final String attemptID1 = "goodAttempt1";
    private static final String attemptID2 = "goodAttempt2";
    private static final String executorID0 = "tetris";
    private static final String executorID1 = "executor";

    private static final String tag = "test";

    private Job job1;
    private Job job2AlreadyExists;
    private Set<String> tags;

    private static Job createTestJob(String jobID, String userID) {

        Job job = new Job();
        job.setJobId(jobID);
        job.setUserId(userID);
        job.setParameters((new ObjectNode(JsonNodeFactory.instance)).set("parameters", (new ObjectNode(JsonNodeFactory.instance)).put("hello", "world")).toString());
        job.setInputDatasets(new HashMap<String, DatasetRef>(){{
            DatasetRef d = new DatasetRef(); d.setUrl("http://storage/input.fas"); put("input.fas", d);
        }});

        return job;
    }

    private static Attempt createTestAttempt(Job job, String attemptID) {

        Attempt attempt = new Attempt();
        attempt.setAttemptId(attemptID);
        attempt.setJob(job);
        attempt.setExecutorId(executorID1);

        return attempt;
    }

    @Before
    public void setup() {
        tags = new HashSet<>();
        tags.add(tag);

        job1 = createTestJob(jobID1, userID1);
        job2AlreadyExists = createTestJob(jobID2, userID2);

        Attempt attempt1 = createTestAttempt(job1, attemptID1);
        Attempt attempt2AlreadyExists = createTestAttempt(job2AlreadyExists, attemptID2);
        List<Attempt> attemptList = Arrays.asList(attempt1, attempt2AlreadyExists);

        when(jobRepo.findByJobId(jobID0)).thenReturn(Optional.empty());
        when(jobRepo.findByJobId(jobID1)).thenReturn(Optional.empty());
        when(jobRepo.findByJobId(jobID2)).thenReturn(Optional.of(job2AlreadyExists));
        when(jobRepo.saveOrUpdate(any(Job.class))).then(returnsLastArg());

        when(attemptRepo.findByJobIdAndAttemptId(jobID0, attemptID0)).thenReturn(Optional.empty());
        when(attemptRepo.findByJobIdAndAttemptId(jobID2, attemptID1)).thenReturn(Optional.empty());
        when(attemptRepo.findByJobIdAndAttemptId(jobID2, attemptID2)).thenReturn(Optional.of(attempt2AlreadyExists));
        when(attemptRepo.saveOrUpdate(any(Attempt.class))).then(returnsLastArg());

        when(attemptRepo.findByState(AttemptState.QUEUED_LOCALLY, 2)).thenReturn(attemptList);
        when(attemptRepo.findByState(AttemptState.QUEUED_LOCALLY, -9000)).thenReturn(Collections.emptyList());

    }

    // Jobs ============================================================================================================

    @Test
    public void successful_jobs_tests() throws AlreadyExistsException, NotFoundException, AuthorizationException {

        Job job;

        // createJob
        job = jobManagerService.createJob(job1);
        assertThat(job).isNotNull();
        assertThat(job.getJobId()).isEqualTo(jobID1);
        assertThat(job.getUserId()).isEqualTo(userID1);
        assertThat(job.getPriority()).isEqualTo(JobManagerService.DEFAULT_JOB_PRIORITY);

        // findJobByUserIdAndJobId
        job = jobManagerService.findJobByUserIdAndJobId(authCtx, userID2, jobID2).get();
        assertThat(job).isNotNull();
        assertThat(job.getJobId()).isEqualTo(jobID2);
        assertThat(job.getUserId()).isEqualTo(userID2);

        // cancelJob
        jobManagerService.cancelJob(authCtx, jobID2);
        assertThat(job2AlreadyExists.isCancelled()).isTrue();
    }

    @Test
    public void failing_jobs_tests() {

        Exception exception = null;

        // createJob
        try {
            jobManagerService.createJob(job2AlreadyExists);
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof AlreadyExistsException).isTrue();
        exception = null;

        // findJobByUserIdAndJobId
        try {
            jobManagerService.findJobByUserIdAndJobId(authCtx, userID0, jobID0).get();
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NoSuchElementException).isTrue();
        exception = null;

        // cancelJob
        try {
            jobManagerService.cancelJob(authCtx, jobID0);
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
    }

    // Attempts ============================================================================================================

    @Test
    public void successful_attempts_tests() throws JobManagerException, AuthorizationException {

        Attempt a;
        String s;

        // createAttempt
        a = jobManagerService.createAttempt(authCtx, jobID2, attemptID1, tag, Optional.of(executorID1));
        assertThat(a).isNotNull();
        assertThat(a.getAttemptId()).isEqualTo(attemptID1);
        assertThat(a.getExecutorId()).isEqualTo(executorID1);
        assertThat(a.getJob().getJobId()).isEqualTo(jobID2);

        // findExecutorAttemptByIds
        a = jobManagerService.findExecutorAttemptByIds(authCtx, executorID1, jobID2, attemptID2).get();
        assertThat(a).isNotNull();
        assertThat(a.getAttemptId()).isEqualTo(attemptID2);
        assertThat(a.getExecutorId()).isEqualTo(executorID1);
        assertThat(a.getJob().getJobId()).isEqualTo(jobID2);

        // updateAttemptState
        s = jobManagerService.updateAttemptState(authCtx, jobID2, attemptID2, AttemptState.ASSIGNED_EXECUTOR.name());
        assertThat(s).isNotNull();
        assertThat(s).isEqualTo(AttemptState.ASSIGNED_EXECUTOR.name());

        // updateAttemptStateForExecutor
        s = jobManagerService.updateAttemptStateForExecutor(authCtx, executorID1, jobID2, attemptID2, AttemptState.RUNNING.name());
        assertThat(s).isNotNull();
        assertThat(s).isEqualTo(AttemptState.RUNNING.name());

    }

    @Test
    public void failing_attempts_tests() {

        Exception exception = null;

        // createAttempt, job not found
        try {
            jobManagerService.createAttempt(authCtx, jobID0, attemptID1, tag, Optional.of(executorID1));
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
        exception = null;

        // createAttempt, attempt already exists
        try {
            jobManagerService.createAttempt(authCtx, jobID2, attemptID2, tag, Optional.of(executorID1));
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof AlreadyExistsException).isTrue();
        exception = null;

        // findExecutorAttemptByIds, attempt not found
        try {
            jobManagerService.findExecutorAttemptByIds(authCtx, executorID1, jobID0, attemptID0).get();
        } catch (Exception e1) {
            assertThat(e1 instanceof NoSuchElementException).isTrue();
        // findExecutorAttemptByIds, wrong executor
            try {
                jobManagerService.findExecutorAttemptByIds(authCtx, executorID0, jobID2, attemptID2).get();
            } catch (Exception e2) {
                assertThat(e2 instanceof NoSuchElementException).isTrue();
            }
        }

        // updateAttemptState
        try {
            jobManagerService.updateAttemptState(authCtx, jobID0, attemptID0, AttemptState.RUNNING.name());
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
        exception = null;

        // updateAttemptStateForExecutor, attempt not found
        try {
            jobManagerService.updateAttemptStateForExecutor(authCtx, executorID1, jobID0, attemptID0, AttemptState.RUNNING.name());
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
        exception = null;

        // updateAttemptStateForExecutor, wrong executor
        try {
            jobManagerService.updateAttemptStateForExecutor(authCtx, executorID0, jobID2, attemptID2, AttemptState.RUNNING.name());
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
    }

    // Executors =======================================================================================================

    @Test
    @Ignore
    public void successful_executors_tests() throws AuthorizationException {

        // popExecutorQueue
        assertThat(jobManagerService.popExecutorQueue(authCtx, executorID1, tags, 2).size()).isEqualTo(2);

    }

    @Test
    public void failing_executors_tests() throws AuthorizationException {

        Exception exception = null;

        // popExecutorQueue, -9000 is just for simulation of empty list
        assertThat(jobManagerService.popExecutorQueue(authCtx, executorID0, tags, -9000).size()).isEqualTo(0);

        // setOutputDataset, attempt not found
        try {
            jobManagerService.setOutputDataset(authCtx, executorID1, jobID0, attemptID0, new DatasetRef());
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();

        // setOutputDataset, wrong executor
        try {
            jobManagerService.setOutputDataset(authCtx, executorID0, jobID2, attemptID2, new DatasetRef());
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
        assertThat(exception instanceof NotFoundException).isTrue();
    }

}
