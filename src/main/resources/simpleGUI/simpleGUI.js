
let webpageName = "META-pipe JOB-MANAGER ADMIN GUI"
document.title = webpageName;

// let jobFetchUrl = '/jobs';
let jobFetchUrl = '/jobs/recentlyActive';

let timeDateFormat = 'd/m/Y H:i:s.v'


function createButtonTooltip(button, tooltip) {
    return (
        <ReactBootstrap.OverlayTrigger placement="top" overlay={
            <ReactBootstrap.Tooltip id={tooltip + '_tooltip'}>
                {tooltip}
            </ReactBootstrap.Tooltip>
        }>
            {button}
        </ReactBootstrap.OverlayTrigger>
    );
}

let Button = React.createClass({
    handleClick: function (event) {
        let r = true;
        let requestType;
        if (this.props.type === "Job") {
            requestType = "POST";
            if (this.props.commandURL.indexOf("/cancel") > -1) {
                r = confirm('Confirm CANCEL ' + this.props.type + ' with ID="' + this.props.id + '"');
            }
        }
        else {
            requestType = "PUT";
        }
        if (r === true) {
            $.ajax({
                url: this.props.commandURL,
                dataType: "html",
                mimeType: "text/html",
                type: requestType,
                data: "CANCELLED",
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
                }.bind(this),
                success: function () {
                    this.props.updateFunction();
                }.bind(this),
                error: function (xhr, status, err) {
                    this.props.updateFunction();
                    console.error(this.props.commandURL, status, err.toString());
                }.bind(this)
            });
        }

    },
    render: function () {
        let button = null;
        if (this.props.type !== "Attempt") {
            let icon;
            if (this.props.commandURL.indexOf("/cancel") > -1) {
                icon = <i className="fa fa-times-circle fa-fw" aria-hidden="true"/>;
            } else {
                icon = <i className="fa fa-repeat fa-fw" aria-hidden="true"/>;
            }
            button = (
                <ReactBootstrap.Button onClick={this.handleClick} bsStyle={this.props.bsStyle} bsSize="xsmall">
                    {icon} {this.props.buttonText}
                </ReactBootstrap.Button>
            );
        }
        else {
            let icon = <i className="fa fa-times fa-fw" aria-hidden="true"/>
            button = (
                <ReactBootstrap.Button onClick={this.handleClick} bsStyle={this.props.bsStyle} bsSize="xsmall">
                    {icon} {this.props.buttonText}
                </ReactBootstrap.Button>
            );
        }
        if (this.props.showTooltip) {
            button = createButtonTooltip(button, this.props.tooltipText);
        }
        return button;
    }
});

let AttemptListItem = React.createClass({
    href() {
        return '/jobs/' + this.props.job.jobId + '/attempts/' + this.props.attempt.attemptId;
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        let shouldUpdate = true;
        //Fast and restricted method for object comparison
        if (JSON.stringify(this.props.attempt) === JSON.stringify(nextProps.attempt)) {
            shouldUpdate = false;
            if (this.props.attempt.state !== 'RUNNING' || nextProps.attempt.state !== 'RUNNING') {
                shouldUpdate = false;
            }
        }
        return shouldUpdate;
    },

    render: function () {
        let attempt = this.props.attempt;
        let style = null;
        let buttonCancel;
        let href = this.href();
        let updateFunction = this.props.updateFunction;

        //Styles
        if (attempt.state === "FINISHED") {
            style = 'info';
        }
        else if (attempt.state === "CANCELLING") {
            style = 'warning';
        }
        else if (attempt.state === "QUEUED_LOCALLY" || attempt.state === "ASSIGNED_EXECUTOR" || attempt.state === "QUEUED_EXECUTOR") {
            //No style needed
        }
        else if (attempt.state === "RUNNING") {
            style = 'success';
        }
        else if (attempt.state === 'CANCELLED' || attempt.state === "TIMED_OUT" || attempt.state === "WORKFLOW_FAILED" || attempt.state === "EXECUTOR_FAILED") {
            style = 'danger';
        }

        //Cancel button
        if (attempt.state !== 'CANCELLED' && attempt.state !== 'CANCELLING') {
            buttonCancel = <Button updateFunction={updateFunction} id={attempt.attemptId} type="Attempt"
                                   commandURL={href + '/state'} buttonText=' ' bsStyle="danger" showTooltip={true}
                                   tooltipText='Cancel attempt'/>;
        }
        else {
            buttonCancel =
                <ReactBootstrap.Button bsStyle="danger" disabled bsSize="xsmall">
                    <i className="fa fa-times fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
        }


        let lastHeartbeat;
        let timePassed;
        if (attempt.lastHeartbeat !== null) {
            lastHeartbeat = (new Date(parseInt(attempt.lastHeartbeat))).format(timeDateFormat);
            countdown.setLabels(
                ' ms| s| m| h| d||||||',
                ' ms| s| m| h| d||||||',
                ', ',
                ', ',
                '- ');
            timePassed = countdown(parseInt(attempt.lastHeartbeat), null, countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS).toString();
        }
        else {
            lastHeartbeat = '<not defined>';
            timePassed = "- ";
        }
        let duplicateButton = <CreateAttempt updateFunction={this.props.updateFunction} job={this.props.job}
                                             func="duplicate"
                                             execId={attempt.executorId} tag={attempt.tag}/>;
        
        let timeStarted = "-";
        if (attempt.timeStarted != null) {
            timeStarted = (new Date(parseInt(attempt.timeStarted))).format(timeDateFormat)
        }

        return (
            <tbody>
            <tr className={style} style={{whiteSpace: 'nowrap', width: '1%'}}>
                <td>{buttonCancel} {duplicateButton}</td>
                <td>{attempt.attemptId}</td>
                <td>{attempt.state}</td>
                <td>{(new Date(parseInt(attempt.timeCreated))).format(timeDateFormat)}</td>
                <td>{timeStarted}</td>
                <td>{attempt.tag}</td>
                <td>{attempt.priority}</td>
                <td>{attempt.executorId}</td>
                <td>{lastHeartbeat} (Time Passed: {timePassed})</td>
            </tr>
            </tbody>
        );
    }
});

let CreateAttempt = React.createClass({
    getInitialState: function () {
        return {executorId: "", tag: "", error: ""};
    },
    handleChange: function (event) {
        if (event.target.name === "executorId") {
            this.setState({executorId: event.target.value});
        }
        else if (event.target.name === "tag") {
            this.setState({tag: event.target.value});
        }
        this.state.error = "";
    },

    handleClick: function (event) {
        //let randomID = "job_" + this.props.job.jobId + "_attempt_";
        let randomID = "attempt_";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let execId, tagTag;
        for (let i = 0; i < 10; i++) {
            randomID += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        if (this.props.func === "duplicate") {
            execId = this.props.execId;
            tagTag = this.props.tag;
        }
        else {
            execId = this.state.executorId;
            tagTag = this.state.tag;
        }
        let dataJ;
        if (execId === "") {
            dataJ = JSON.stringify({executorId: null, tag: tagTag});
        }
        else {
            dataJ = JSON.stringify({executorId: execId, tag: tagTag});
        }
        let url = '/jobs/' + this.props.job.jobId + '/attempts/' + randomID;
        $.ajax({
            url: url,
            contentType: "application/json",
            type: 'PUT',
            data: dataJ,
            cache: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
            }.bind(this),
            success: function () {
                this.state.error = "";
                this.setState({executorId: ""});
                this.setState({tag: ""});
                this.props.updateFunction();
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(url, status, err.toString());
                this.state.error = <i><font size="2" color="red"> Check Executor ID and Tag! </font></i>;
                this.props.updateFunction();
            }.bind(this)
        });
    },
    render: function () {
        let button;
        let returnVal;
        if (this.props.func === "create") {
            if (this.props.job.state === "CANCELLED") {
                button = <ReactBootstrap.Button componentClass={ReactBootstrap.InputGroup.Button} bsStyle="info"
                                                bsSize="xsmall" disabled>
                    <i className="fa fa-plus-circle fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            }
            else {
                button =
                    <ReactBootstrap.Button componentClass={ReactBootstrap.InputGroup.Button} onClick={this.handleClick}
                                           bsStyle="info" bsSize="xsmall">
                        <i className="fa fa-plus-circle fa-fw" aria-hidden="true"/>
                    </ReactBootstrap.Button>;
                button = createButtonTooltip(button, 'Create new attempt');
            }
            returnVal = <div>
                {button}
                &nbsp;&nbsp;
                <input name="executorId" onChange={this.handleChange} type="text" value={this.state.executorId}
                       placeholder="Executor ID"/>
                &nbsp;&nbsp;
                <input name="tag" onChange={this.handleChange} type="text" value={this.state.tag} placeholder="Tag"/>
                &nbsp;&nbsp;
                {this.state.error}
            </div>;
        }
        else if (this.props.func === "duplicate") {
            if (this.props.job.state === "CANCELLED") {
                button = <ReactBootstrap.Button componentClass={ReactBootstrap.InputGroup.Button} bsStyle="info"
                                                bsSize="xsmall" disabled>
                    <i className="fa fa-plus-circle fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            }
            else {
                button =
                    <ReactBootstrap.Button componentClass={ReactBootstrap.InputGroup.Button} onClick={this.handleClick}
                                           bsStyle="info" bsSize="xsmall">
                        <i className="fa fa-plus-circle fa-fw" aria-hidden="true"/>
                    </ReactBootstrap.Button>;
                button = createButtonTooltip(button, 'Duplicate attempt');
            }
            returnVal = button;
        }
        return (
            <b> {returnVal} </b>
        );
    }
});

let JobDetailsDef = {
    getInitialState: function () {
        return {drawModalDetails: false};
    },

    openClose(e) {
        this.setState({drawModalDetails: !this.state.drawModalDetails});
    },

    shouldComponentUpdate(nextProps, nextState) {
        return JSON.stringify(this.props.job) !== JSON.stringify() || this.state.drawModalDetails !== nextState.drawModalDetails;
    },

    createJsonModal() {
        return (
            <ReactBootstrap.Modal show={this.state.drawModalDetails} onHide={this.openClose} bsSize="large">
                <ReactBootstrap.Modal.Header closeButton>
                    <ReactBootstrap.Modal.Title>JSON description for
                        job: {this.props.job.jobId}</ReactBootstrap.Modal.Title>
                </ReactBootstrap.Modal.Header>
                <ReactBootstrap.Modal.Body>
                    <pre>
                      <code style={{display: "block", overflow: "auto", maxHeight: '50em'}}>
                        {JSON.stringify(this.props.job, null, 2)}
                      </code>
                    </pre>
                </ReactBootstrap.Modal.Body>
                <ReactBootstrap.Modal.Footer>
                    <ReactBootstrap.Button onClick={this.openClose}>Close</ReactBootstrap.Button>
                </ReactBootstrap.Modal.Footer>
            </ReactBootstrap.Modal>
        );
    },

    render: function () {
        let attempts = this.props.job.attempts;
        attempts.sort((a, b) => a.timeCreated > b.timeCreated ? -1 : 1);
        let listItems = [];
        let divStyle = {width: '100%'};
        for (let attemptId in attempts) {
            if (attempts.hasOwnProperty(attemptId)) {
                listItems.push(<AttemptListItem key={attemptId} attempt={attempts[attemptId]} job={this.props.job}
                                                updateFunction={this.props.updateFunction}/>);
            }
        }
        let jsonInfoButton = (
            <ReactBootstrap.Button bsStyle="primary" bsSize="xsmall" onClick={this.openClose}>
                <i className="fa fa-info fa-fw" aria-hidden="true"/>
            </ReactBootstrap.Button>
        );

        jsonInfoButton = createButtonTooltip(jsonInfoButton, 'Show JSON');

        let jsonModal = this.createJsonModal();
        return (
            <div className="jobDetails" style={{overflow: "hidden"}}>
                <h4 className="jobDetailsTitle" style={{marginLeft: '1%', marginRight: '1%'}}>
                    <b><i>Job Details. Job id: {this.props.job.jobId} {jsonInfoButton}</i></b>
                </h4>
                <ReactBootstrap.Grid fluid={true}>
                    <ReactBootstrap.Row>
                        <ReactBootstrap.Col md={12}>
                            <div style={{paddingBottom: '10px', paddingTop: '10px'}}>
                                <CreateAttempt updateFunction={this.props.updateFunction} job={this.props.job}
                                               func="create"/>
                            </div>
                            <div style={{border: '2px solid'}}>
                                <ReactBootstrap.Table striped bordered condensed responsive>
                                    <thead style={{backgroundColor: 'LightGrey'}}>
                                    <tr>
                                        <th/>
                                        <th>Id</th>
                                        <th>State</th>
                                        <th>Created</th>
                                        <th>Started</th>
                                        <th>Tag</th>
                                        <th>Priority</th>
                                        <th>ExecutorId</th>
                                        <th>LastHeartbeat</th>
                                    </tr>
                                    </thead>
                                    {listItems}
                                </ReactBootstrap.Table>
                            </div>
                        </ReactBootstrap.Col>
                    </ReactBootstrap.Row>
                </ReactBootstrap.Grid>
                {jsonModal}
            </div>
        );
    }
};

let JobListItemDef = {
    getInitialState: function () {
        return {
            drawModalDetails: false,
            drawDropdownDetails: false
        };
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        return JSON.stringify(this.props.job) !== JSON.stringify(nextProps.job)
            || this.state.drawModalDetails !== nextState.drawModalDetails
            || this.state.drawDropdownDetails !== nextState.drawDropdownDetails;
    },

    href() {
        return '/jobs/' + this.props.job.jobId;
    },
    reloadJob: function () {
        let url = this.href();
        $.ajax({
            url: url,
            dataType: 'json',
            cache: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
            }.bind(this),
            success: function (data) {
                jobListData[this.props.job.jobId] = data;

            }.bind(this),
            error: function (xhr, status, err) {
                console.error(url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function () {
        {/*this.props.job.attempts.sort((a,b) => a.timeCreated > b.timeCreated ? -1 : 1);*/
        }
        {/*this.loadAttemptsFromServer();
         this.setState({intervalId: setInterval(this.loadAttemptsFromServer, pollIntervalValue)});*/
        }
    },
    componentWillUnmount() {
        {/*clearInterval(this.state.intervalId);*/
        }
    },
    clickHandler(e) {
        e.preventDefault();
        this.setState({drawDropdownDetails: !this.state.drawDropdownDetails});
    },
    close(e) {
        this.setState({drawModalDetails: !this.state.drawModalDetails});
    },

    stopBubble(e) {
        e.stopPropagation();
    },

    holdJob(hold) {
        return function(e) {
            $.ajax({
                url: this.href() + '/hold',
                type: "PUT",
                data: hold.toString(),
                contentType: 'text/plain',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
                }.bind(this),
                success: function () {
                    this.reloadJob();
                }.bind(this),
                error: function (xhr, status, err) {
                    this.reloadJob();
                    console.error(this.props.commandURL, xhr.status, err.toString());
                }.bind(this)
            });
        }.bind(this)
    },

    createHoldButton(hold, disabled) {
        let button;
        if (!hold && !disabled) {
            button =
                <ReactBootstrap.Button bsStyle="warning" bsSize="xsmall" onClick={this.holdJob(true)}>
                    <i className="fa fa-pause fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            button = createButtonTooltip(button, "Set job on hold")
        } else if (!hold && disabled) {
            button =
                <ReactBootstrap.Button bsStyle="warning" bsSize="xsmall" disabled>
                    <i className="fa fa-pause fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
        } else if (hold && !disabled) {
            button =
                <ReactBootstrap.Button bsStyle="warning" bsSize="xsmall" onClick={this.holdJob(false)}>
                    <i className="fa fa-play fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            button = createButtonTooltip(button, "Continue job")
        } else if (hold && disabled) {
            button =
                <ReactBootstrap.Button bsStyle="warning" bsSize="xsmall" disabled>
                    <i className="fa fa-play fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
        }
        return button;
    },

    render() {
        let job = this.props.job;
        let style = null;
        let buttonCancel;
        let buttonRestart;
        let infoButton;
        let buttonHold = this.createHoldButton(job.hold, job.state === 'CANCELLED' || job.state === 'FINISHED');
        let href = this.href();
        let updateFunction = this.reloadJob;
        let drawModalDetails = this.state.drawModalDetails;
        let attempts = this.props.job.attempts;
        let timeStarted = "-";
        
        
        if (attempts.length > 0 && attempts[0] && attempts[0].timeStarted != null) {
            timeStarted = (new Date(parseInt(attempts[0].timeStarted))).format(timeDateFormat)
        }

        if (job.state !== 'CANCELLED') {
            buttonCancel =
                <Button updateFunction={updateFunction} id={job.jobId} type="Job" commandURL={href + '/cancel'}
                        bsStyle="danger" showTooltip={true} tooltipText={'Cancel job ' + job.jobId}/>;
            buttonRestart =
                <Button updateFunction={updateFunction} id={job.jobId} type="Job" commandURL={href + '/restart'}
                        bsStyle="warning" showTooltip={true} tooltipText={'Restart job ' + job.jobId}/>;


            //Styles
            if (job.state === "FINISHED") {
                style = 'info';
                buttonCancel =
                    <ReactBootstrap.Button bsStyle="danger" bsSize="xsmall" disabled>
                        <i className="fa fa-plus-times fa-fw" aria-hidden="true"/>
                    </ReactBootstrap.Button>;
            }
            else if (job.state === "SUBMITTED") {
                if (attempts.length > 0) {
                    if (attempts[0]) {
                        if (attempts[0].state === 'RUNNING') {
                            style = 'success';
                        }
                    }
                }
            }
            else if (job.state === "DELAYED") {
                style = 'danger';
            }
        }
        else {
            buttonCancel =
                <ReactBootstrap.Button bsStyle="danger" bsSize="xsmall" disabled>
                    <i className="fa fa-plus-times fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            buttonRestart =
                <ReactBootstrap.Button bsStyle="warning" bsSize="xsmall" disabled>
                    <i className="fa fa-plus-repeat fa-fw" aria-hidden="true"/>
                </ReactBootstrap.Button>;
            buttonCancel = createButtonTooltip(buttonCancel, 'Cancel the job');
            buttonRestart = createButtonTooltip(buttonRestart, 'Restart the job');
        }

        infoButton = <ReactBootstrap.Button bsStyle="primary" bsSize="xsmall" onClick={this.close}>
            <i className="fa fa-info fa-fw" aria-hidden="true"/>
        </ReactBootstrap.Button>;
        infoButton = createButtonTooltip(infoButton, 'Display job info in a modal');

        //Necessary overlay for the content of the popup
        let idPopover = (
            <ReactBootstrap.Popover style={{maxWidth: '100%'}} title="Id" id="popover-trigger-focus">
                <p>{job.jobId}</p>
            </ReactBootstrap.Popover>);
        //Replace the id if it is over a certain length.
        let newJobId = job.jobId;
        if (newJobId.length > 25) {
            newJobId = newJobId.substring(0, 22) + '...';
        }

        let jobDetails = <div/>;
        if (this.state.drawDropdownDetails) {
            jobDetails = <JobDetails job={job} updateFunction={updateFunction} restartButton={buttonRestart}
                                     cancelButton={buttonCancel}/>;
        }

        let jobDetailsModal = null;
        if (this.state.drawModalDetails) {
            jobDetailsModal = (
                <ReactBootstrap.Modal show={drawModalDetails} onHide={this.close} bsSize="large">
                    <ReactBootstrap.Modal.Header closeButton>
                        <ReactBootstrap.Modal.Title>{job.jobId}</ReactBootstrap.Modal.Title>
                    </ReactBootstrap.Modal.Header>
                    <ReactBootstrap.Modal.Body>
                        <JobDetails job={job} updateFunction={updateFunction} restartButton={buttonRestart}
                                    cancelButton={buttonCancel}/>;
                    </ReactBootstrap.Modal.Body>
                    <ReactBootstrap.Modal.Footer>
                        <ReactBootstrap.Button onClick={this.close}>Close</ReactBootstrap.Button>
                    </ReactBootstrap.Modal.Footer>
                </ReactBootstrap.Modal>
            );
        }

        let userInfoExists = "";
        if (job.userInfo !== undefined) {
            userInfoExists = " / " + (job.userInfo.name !== null ? job.userInfo.name : "< - >") + " / " +  (job.userInfo.organization !== null ? job.userInfo.organization : "< - >");
        }

        return (
            <tbody>
                <tr className={style} style={{whiteSpace: 'nowrap', width: '1%'}} onClick={this.clickHandler}>
                    <td onClick={this.stopBubble}>{buttonCancel} {buttonRestart} {buttonHold} {infoButton}</td>
                    <td>{newJobId}</td>
                    <td>{job.state}</td>
                    <td>{(new Date(parseInt(job.timeSubmitted))).format(timeDateFormat)}</td>
                    <td>{timeStarted}</td>
                    <td>{job.userId} {userInfoExists}</td>
                    <td>{job.groupId}</td>
                    <td>{job.tag}</td>
                    <td>{job.priority}</td>
                    <td>{attempts.length > 0 ? attempts[0].state : "-"}</td>
                    <td>{attempts.length > 0 ? attempts[0].executorId : "-"}</td>
                </tr>
                <ReactBootstrap.Collapse in={this.state.drawDropdownDetails}>
                    <tr>
                        <td colSpan={12}>
                            <div style={{border: '4px solid', marginLeft: '1%', marginRight: '1%', paddingBottom: '1%'}}>
                                {jobDetails}
                            </div>
                        </td>
                    </tr>
                </ReactBootstrap.Collapse>
                {jobDetailsModal}
            </tbody>
        );
    }
};

let JobsListDef = {
    loadJobsFromServer: function (always) {
        $.ajax({
            url: jobFetchUrl,
            dataType: 'json',
            cache: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
            }.bind(this),
            success: function (data) {
                data.sort((a, b) => a.timeSubmitted > b.timeSubmitted ? -1 : 1);
                this.setState({jobData: data});
                this.updateList();
            }.bind(this),
            error: function (xhr, status, err) {
                console.error('/jobs/recentlyActive', status, err.toString());
            }.bind(this)
        }).always(always);
    },
    getInitialState: function () {
        return {findByJobID: "", findByState: "", findByUserID: "", findByGroupID: "", findByExecID: "", jobData: []};
    },

    componentDidMount: function () {
        this.setState({jobData: this.props.jobs});
        // this.updateList();
    },

    componentWillMount: function () {
        let loadJobsFromServer = this.loadJobsFromServer;

        let refresher = function() {
            setTimeout(function() {
                loadJobsFromServer(refresher);
            }, pollIntervalValue);
        }

        loadJobsFromServer(refresher);
    },

    logout: function () {
        OauthClient.invalidateToken();
    },
    handleChange: function (event) {
        switch (event.target.className) {
            case "findByJobID":
                this.setState({findByJobID: event.target.value});
                break;
            case "findByState":
                this.setState({findByState: event.target.value});
                break;
            case "findByUserID":
                this.setState({findByUserID: event.target.value});
                break;
            case "findByGroupID":
                this.setState({findByGroupID: event.target.value});
                break;
            case "findByExecID":
                this.setState({findByExecID: event.target.value});
                break;
        }
        this.loadJobsFromServer();
        this.updateList();
    },
    checkFindByValue: function (findByValue, jobValue) {
        return findByValue === "" ||
            (jobValue !== null && jobValue.toLowerCase().indexOf(findByValue.toLowerCase()) !== -1);

    },
    checkFindByExecID: function (findByValue, attempts) {
        if (findByValue === "") {
            return true;
        }
        for (let attempt in attempts) {
            if (attempts.hasOwnProperty(attempt)) {
                if (attempts[attempt].executorId !== null && attempts[attempt].executorId.toLowerCase().indexOf(findByValue.toLowerCase()) !== -1) {
                    return true;
                }
            }
        }
        return false;
    },

    sort(event) {
        console.log(event.target)
    },

    updateList: function () {
        this.forceUpdate();
    },
    createInput: function (className, value) {
        return <input className={className} onChange={this.handleChange} type="text" value={value}
                      style={{width: '11%'}}/>;
    },
    render: function () {
        let logoutButton = (<ReactBootstrap.Button bsSize="small" onClick={this.logout}><b>Logout</b></ReactBootstrap.Button>);

        let jobs = this.state.jobData;
        let newList = [];
        let redJobs = 0;
        let attempts;
        let runningCount = 0;
        for (let jobId in jobs) {
            if (jobs.hasOwnProperty(jobId)) {
                attempts = jobs[jobId].attempts;
                if (jobs[jobId].state === "DELAYED") {
                    redJobs++;
                }
                if (attempts) {
                    attempts.sort((a, b) => a.timeCreated > b.timeCreated ? -1 : 1);
                    if (attempts[0]) {
                        if (attempts[0].state === 'RUNNING') {
                            runningCount++;
                        }
                    }
                    jobs[jobId].attempts = attempts;
                }
                if (jobs.hasOwnProperty(jobId) && (
                        this.checkFindByValue(this.state.findByJobID, jobs[jobId].jobId) &&
                        this.checkFindByValue(this.state.findByState, jobs[jobId].state) &&
                        this.checkFindByValue(this.state.findByUserID, jobs[jobId].userId) &&
                        this.checkFindByValue(this.state.findByGroupID, jobs[jobId].groupId) &&
                        this.checkFindByExecID(this.state.findByExecID, attempts)
                    )) {
                    newList.push(<JobListItem key={jobId} job={jobs[jobId]} updateFunction={this.loadJobsFromServer}/>);
                }
            }
        }

        let header;
        let h1Style;
        if (redJobs === 0) {
            h1Style = {
                textAlign: "center",
                borderTop: '3px solid',
                marginTop: '0px'
            };
        }
        else {
            h1Style = {
                backgroundColor: 'LightCoral',
                textAlign: "center",
                borderTop: '2px solid',
                marginTop: '0px'
            };
        }

        let searchBar = (
            <p style={{textAlign: "center", margin: "0"}}>
                &nbsp;&nbsp;
                <b><u>Filter by:</u></b>&nbsp;
                JobID {this.createInput("findByJobID", this.state.findByJobID)}&nbsp;
                State {this.createInput("findByState", this.state.findByState)}&nbsp;
                UserID {this.createInput("findByUserID", this.state.findByUserID)}&nbsp;
                GroupID {this.createInput("findByGroupID", this.state.findByGroupID)}&nbsp;
                ExecutorID {this.createInput("findByExecID", this.state.findByExecID)}
                &nbsp;&nbsp;
            </p>
        );

        /* Old Header */
        /*
        header = (
            <div style={h1Style}>
                <ReactBootstrap.Row className="show-grid" style={{width: "100%", paddingTop: "1em", paddingBottom: "1em"}}>
                    <ReactBootstrap.Col xs={12} sm={2} md={2} lg={1} style={{padding: "0"}}>
                        {logoutButton}
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col xs={12} sm={12} md={12} lg={5}  style={{padding: "0", paddingTop: "5px"}}>
                        <b>META-pipe JobManager GUI - {jobs.length} jobs ({runningCount} Running) - Status: {redJobs} jobs delayed </b>
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col xs={12} sm={10} md={10} lg={5}  style={{padding: "0"}}>
                        {searchBar}
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </div>
        );
        */
        
        // <span style={{paddingRight: "10%"}}><b><u>META-pipe JobManager Admin GUI</u></b></span>
        
        header = (
            <div style={h1Style}>
                <ReactBootstrap.Row className="show-grid" style={{width: "100%", margin: "0", paddingTop: "0.5em", paddingBottom: "0.5em", borderBottom: '1px solid'}}>
                    <ReactBootstrap.Col xs={2} sm={1} md={1} lg={1} style={{paddingTop: "0px", fontSize: "120%"}}>
                        {logoutButton}
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col xs={10} sm={7} md={7} lg={5} style={{paddingTop: "4px", paddingBottom: "4px", fontSize: "120%"}}>
                        <span style={{paddingRight: "10%"}}><b><u>{webpageName}</u></b></span>
                        <span><b><u>Delayed jobs: {redJobs} </u></b></span>
                    </ReactBootstrap.Col>
                    <div style={{paddingTop: "4px", fontSize: "120%"}}>
                        <ReactBootstrap.Col xs={6} sm={2} md={2} lg={3}>
                            <b>Total jobs: {jobs.length}</b>
                        </ReactBootstrap.Col>
                        <ReactBootstrap.Col xs={6} sm={2} md={2} lg={3}>
                            <b>Running: {runningCount}</b>
                        </ReactBootstrap.Col>
                    </div>
                </ReactBootstrap.Row>
                <ReactBootstrap.Row className="show-grid" style={{width: "100%", paddingTop: "0.75em", paddingBottom: "0.75em", margin: "0"}}>
                    <ReactBootstrap.Col xs={12} sm={12} md={12} lg={12}  style={{padding: "0"}}>
                        {searchBar}
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </div>
        );

        return (
            <div>
                {header}
                <div style={{
                    borderRight: '4px solid',
                    borderTop: '4px solid',
                    borderLeft: '4px solid',
                    borderBottom: '4px solid'
                }}>

                    <ReactBootstrap.Table striped bordered condensed responsive>
                        <thead>
                        <tr>
                            <th/>
                            <th onClick={this.sort}>Id</th>
                            <th>State</th>
                            <th>Submitted</th>
                            <th>Started</th>
                            <th>User</th>
                            <th>GroupID</th>
                            <th>Tag</th>
                            <th>Priority</th>
                            <th>Latest Attempt State</th>
                            <th>Latest Attempt ExecutorID</th>
                        </tr>
                        </thead>
                        {newList}
                    </ReactBootstrap.Table>
                </div>
            </div>
        );
    }
};

//TODO: Create a logout function
let OauthClient = {
    loggedIn: false,
    isAdmin: false,
    active: false,
    clientId: 'system_web',

    getToken: function () {
        return localStorage.getItem('metapipeToken');
    },

    getAuthHost: function () {
        let params = new URLSearchParams(location.search);
        let host = '';
        if (params.has('authHost')) {
            host = params.get('authHost');
            if (host === 'localhost') {
                if (params.has("port")) {
                    host = 'http://' + host + ':' + params.get('port');
                } else {
                    //Taken from test-config.yml
                    host = 'http://' + host + '8085';
                }
            } else if (host === 'vagrant') {
                host = 'https://auth.metapipe.vagrant.test'
            }
        } else {
            host = 'https://auth.metapipe.uit.no';
        }
        return host;
    },

    authUrl: function () {
        let endUserAuthorizationEndpoint = this.getAuthHost() + "/oauth2/authorize";
        return encodeURI(endUserAuthorizationEndpoint +
            "?response_type=token" +
            "&client_id=" + this.clientId +
            "&redirect_uri=" + window.location);
    },

    //Warning: recursive function as callback from isTokenActive()
    authorize: function (recurred) {
        let token = this.matchToken(document.location.hash);
        let localToken = this.getToken();
        if (token) {
            this.validateToken(token);
            //Found a token in the URI
            if (!localToken) {
                //Found no token in local storage
                //Decode here so the stored token is in the right format for oauth2
                token = decodeURIComponent(token);
                this.setToken(token);
                this.loggedIn = true;
            } else {
                //Found token in local storage
                //Decode here so we can match the actual stored token and so it is in the right format for oauth2
                token = decodeURIComponent(token);
                //Check if a new token has been fetched
                if (localToken !== token) {
                    this.setToken(token);
                }
                //Log in status is set to true when the token exists.
                this.loggedIn = true;
            }
        } else {
            //Did not find a token in the URI
            //Check for locally stored token, and check if it is active.
            if (localToken) {
                this.loggedIn = this.active;
                if (this.loggedIn === false && !recurred) {
                    this.isTokenActive(localToken)
                }
            } else {
                this.loggedIn = false;
            }
        }
    },

    setToken: function (token) {
        localStorage.setItem('metapipeToken', token);
    },

    setAdmin: function (val) {
        this.isAdmin = val;
    },

    validateToken: function (token) {
        //Do some checks to see if token is valid by calling the auth server
        //Refresh the token if expired
    },

    isTokenActive: function (token) {
        $.ajax({
            url: this.getAuthHost() + '/oauth2/introspect',
            type: 'POST',
            data: 'token=' + token,
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }.bind(this),
            success: function (data) {
                this.active = data.active;
                this.authorize(true);
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.getAuthHost() + '/oauth2/introspect', status, err.toString());
            }.bind(this)
        });
    },

    refreshToken: function () {
        //Call the auth server api to refresh the getToken
    },

    matchToken: function (hash) {
        //TODO: Fix regex to support all fields in https://tools.ietf.org/html/rfc6749#section-4.2.2
        let match = hash.match(/access_token=(.*)/);
        return !!match && match[1];
    },

    invalidateToken: function () {
        $.ajax({
            url: this.getAuthHost() + '/oauth2/revoke',
            type: "POST",
            data: "token=" + this.getToken(),
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + this.getToken());
            }.bind(this),
            success: function (data) {
                console.log('Token revoked');
                console.log(data);
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.getAuthHost() + '/oauth2/revoke', status, err.toString());
            }.bind(this)
        });
        localStorage.removeItem('metapipeToken');
        this.loggedIn = false;
        this.isAdmin = false;
        this.redirectToGui();
    },

    redirectToGui: function () {
        let loc = window.location;
        loc.assign(loc.origin + loc.pathname + loc.search);
    }
};

//TODO: Use npm and webpack for client-oauth2?
let RequireLoginDef = {

    getInitialState: function () {
        return ({
            jobData: []
        })
    },

    //Run the auth queries before rendering dom element
    componentWillMount: function () {
        this.onLogin();
    },

    onLogin: function () {
        OauthClient.authorize(false);
        if (OauthClient.getToken()) {
            $.ajax({
                url: jobFetchUrl,
                dataType: 'json',
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + OauthClient.getToken());
                }.bind(this),
                success: function (data) {
                    OauthClient.setAdmin(true);
                    this.setState({jobData: data});
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error('/jobs when checking for admin access', xhr.status, err.toString());
                    if (xhr.status === 403) {
                        OauthClient.setAdmin(false);
                        this.forceUpdate();
                    }
                }.bind(this)
            });
        }
    },

    continueToGUI: function (event) {
        OauthClient.isAdmin = true;
        this.forceUpdate();
    },

    render: function () {
        let comp;
        if (OauthClient.loggedIn && OauthClient.getToken() && OauthClient.isAdmin) {
            let divStyleMain = {
                whiteSpace: "nowrap",
                minWidth: '100%',
                height: '100%',
                float: 'left'
            };
            comp = (
                <div className="jobMain" style={divStyleMain} textAlign='center'>
                    <div style={{backgroundColor: 'LightGrey'}}>
                        <JobsList jobs={this.state.jobData}/>
                    </div>
                </div>
            );

        } else if (OauthClient.loggedIn && !OauthClient.isAdmin) {
            comp =
                <div className="container">
                    <div className="ReactBootstrap.jumbotron">
                        <h2>Invalid Elixir Account</h2>
                        <p className="lead">
                            The account used to signed in is not a part of the admin group
                            <br/>
                            Please contact the Meta-pipe administrators for admin access.
                            <br/>
                            <a onClick={this.continueToGUI}>Click here to continue without admin access</a>
                        </p>
                    </div>
                </div>
        } else {
            comp = (
                <div className='container'>
                    <div className="ReactBootstrap.jumbotron">
                        <h1><img src='/gui/MMP_logo_23.png' style={{width: '2.5em', height: '2.5em'}}/> META-pipe 2.0
                            GUI</h1>
                        <h2>Please sign in</h2>
                        <p className="lead">
                            The admin GUI uses an Elixir account associated with an admin group
                        </p>
                        <p>
                            <a className='btn btn-lg btn-success' id="login-button" href={OauthClient.authUrl()}
                               role='button'
                               onClick={this.onLogin}>
                                &nbsp;
                                Log in with Elixir AAI
                            </a>
                        </p>
                    </div>
                </div>
            );
        }
        return (
            <div id="loginWrapper">
                {comp}
            </div>
        )
    }

};

let RequireLogin = React.createClass(RequireLoginDef);

let JobsList = React.createClass(JobsListDef);
let JobDetails = React.createClass(JobDetailsDef);
let JobListItem = React.createClass(JobListItemDef);
let pollIntervalValue = 1500;
let jobListData = [];

ReactDOM.render(
    <RequireLogin/>
    ,
    document.getElementById('content')
);


