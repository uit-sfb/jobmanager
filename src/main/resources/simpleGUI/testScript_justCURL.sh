#!/usr/bin/env bash


for ((i=1; i<=1000; i++)); do
   curl  -X PUT localhost:9876/users/aag007/jobs/job_mock_$i -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
done

curl  -X PUT localhost:9876/users/aag007/jobs/job_a -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_b -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_c -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_d -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_e -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_f -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_g -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_h -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_i -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_j -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_k -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_l -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_m -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_n -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_o -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag007/jobs/job_p -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"

curl  -X PUT localhost:9876/users/aag006/jobs/job_2 -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag006/jobs/job_2a -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag006/jobs/job_3 -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag006/jobs/job_3123456781234569567890 -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag006/jobs/job_312345678123479567890 -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/users/aag006/jobs/job_1 -d '{"tag": "test_tag", "inputs": {"input.fas": {"url": "http://storage/input.fas"}}, "outputs": {}, "parameters": {"hello":"world"}}' -i -H "Content-Type: application/json"



for ((i=1; i<=100; i++)); do
   curl  -X PUT localhost:9876/jobs/job_1/attempts/mock_$i -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
done

for ((i=1; i<=500; i++)); do
   curl  -X PUT localhost:9876/jobs/job_2a/attempts/mock_$i -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
done



curl  -X PUT localhost:9876/jobs/job_1/attempts/j1_a0 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_1/attempts/j1_a1 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"

curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a0/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a0/heartbeat -X POST -d ''

curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a1/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a1/state -X PUT -d 'QUEUED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a1/heartbeat -X POST -d ''

curl  -X PUT localhost:9876/jobs/job_1/attempts/j1_a2 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_1/attempts/j1_a3 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"

curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a2/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a2/state -X PUT -d 'RUNNING'

curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a3/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a3/state -X PUT -d 'RUNNING'
curl  localhost:9876/executors/stallo/jobs/job_1/attempts/j1_a3/heartbeat -X POST -d ''



curl  -X PUT localhost:9876/jobs/job_2a/attempts/j2a_a00 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_2a/attempts/j2a_a0 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_2a/attempts/j2a_a1 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_2a/attempts/j2a_a2 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"

curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a0/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a0/state -X PUT -d 'WORKFLOW_FAILED'

curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a1/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a1/state -X PUT -d 'TIMED_OUT'

curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a2/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_2a/attempts/j2a_a2/state -X PUT -d 'EXECUTOR_FAILED'



curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a0 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a1 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a2 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a3 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a4 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a5 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a6 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a7 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a8 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a9 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a10 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a11 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a12 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a13 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a14 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a15 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a16 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a17 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a18 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a19 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a20 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a21 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a22 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a23 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a24 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a25 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a26 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a27 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a28 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"
curl  -X PUT localhost:9876/jobs/job_3/attempts/j3_a29 -d '{"executorId": "stallo", "tag": "test_tag"}' -i -H "Content-Type: application/json"

curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a0/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a0/state -X PUT -d 'WORKFLOW_FAILED'

curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a1/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a1/state -X PUT -d 'TIMED_OUT'

curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a2/state -X PUT -d 'ASSIGNED_EXECUTOR'
curl  localhost:9876/executors/stallo/jobs/job_3/attempts/j3_a2/state -X PUT -d 'EXECUTOR_FAILED'



curl  localhost:9876/jobs/job_1 | jq .


