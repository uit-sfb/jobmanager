alter table Job
    add column hold boolean;
alter table Job
    add column state varchar(255);

create index Job_state_index on Job (state);

UPDATE Job SET hold = false WHERE hold is NULL;