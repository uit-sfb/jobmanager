alter table Attempt
    add column finalized boolean;

update Attempt set finalized = false where finalized is NULL;

create index Attempt_finalized_index on Attempt (finalized);