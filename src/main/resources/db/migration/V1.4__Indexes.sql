create index Attempt_DatasetRef_attempt_id_index on Attempt_DatasetRef (Attempt_id);

create index AttemptId_index on Attempt (attemptId);
create index Attempt_executorId_index on Attempt (executorId);
create index Attempt_state_index on Attempt (state);
create index Attempt_job_id_index on Attempt (job_id);
create index Attempt_tag on Attempt (tag);


create index Job_jobId_index on Job (jobId);
create index Job_userId_index on Job (userId);

create index Job_DatasetRef_Job_id_index on Job_DatasetRef (Job_id);

create index Job_Properties_Job_id_index on Job_Properties (Job_id);