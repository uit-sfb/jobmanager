alter table Job
    add column priority bigint;

alter table Attempt
    add column priority bigint;

UPDATE Job SET priority = 0 WHERE priority is NULL;

UPDATE Attempt SET priority = 0 WHERE priority is NULL;
