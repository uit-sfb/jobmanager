create table Job_Properties (
    Job_id int8 not null,
    key varchar(255) not null,
    value varchar(255),
    primary key (Job_id, key)
);