create table Attempt (
    id  bigserial not null,
    creationTimestamp timestamp,
    updateTimestamp timestamp,
    attemptId varchar(255),
    executorId varchar(255),
    lastHeartbeat timestamp,
    state varchar(255),
    job_id int8,
    primary key (id)
);

create table Attempt_DatasetRef (
    Attempt_id int8 not null,
    outputDatasets_id int8 not null,
    primary key (Attempt_id, outputDatasets_id)
);

create table DatasetRef (
    id  bigserial not null,
    creationTimestamp timestamp,
    updateTimestamp timestamp,
    key varchar(255),
    readBearerToken varchar(255),
    url varchar(255),
    writeBearerToken varchar(255),
    primary key (id)
);

create table Job (
    id  bigserial not null,
    creationTimestamp timestamp,
    updateTimestamp timestamp,
    cancelled boolean not null,
    groupId varchar(255),
    jobId varchar(255),
    parameters varchar(255),
    userId varchar(255),
    primary key (id)
);

create table Job_DatasetRef (
    Job_id int8 not null,
    inputDatasets_id int8 not null,
    primary key (Job_id, inputDatasets_id)
);

alter table Attempt_DatasetRef
    add constraint UK_35wa66rilq5dw1ios0ddd2w5f unique (outputDatasets_id);

alter table Job_DatasetRef
    add constraint UK_n7ikbuggd7m8pqciuxneopcy1 unique (inputDatasets_id);

alter table Attempt
    add constraint FKpdxa0gib0iwiglrg3waov3dwe
    foreign key (job_id)
    references Job;

alter table Attempt_DatasetRef
    add constraint FK2xcaw0ch2qk5vs8b95we0t9bl
    foreign key (outputDatasets_id)
    references DatasetRef;

alter table Attempt_DatasetRef
    add constraint FKpwe221k397lbjon5u3wkgk6ks
    foreign key (Attempt_id)
    references Attempt;

alter table Job_DatasetRef
    add constraint FK36lmokropf5bvqhe7l043odt6
    foreign key (inputDatasets_id)
    references DatasetRef;

alter table Job_DatasetRef
    add constraint FKa6gby3u2v0w7hrp4rkxx9jvte
    foreign key (Job_id)
    references Job;
