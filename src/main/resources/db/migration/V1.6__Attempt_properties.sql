create table Attempt_Properties (
    Attempt_id int8 not null,
    value varchar(255),
    key varchar(255) not null,
    primary key (Attempt_id, key)
);

alter table Attempt_Properties
       add constraint FKektdl66jhm3l59et9pwqyufbp
       foreign key (Attempt_id)
       references Attempt;