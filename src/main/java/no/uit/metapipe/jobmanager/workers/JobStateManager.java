package no.uit.metapipe.jobmanager.workers;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.hibernate.UnitOfWorkAwareProxyFactory;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;
import no.uit.metapipe.jobmanager.config.JobStateManagerConfig;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.core.JobState;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JobStateManager implements Managed {
    private final JobStateManagerConfig config;

    protected final ScheduledThreadPoolExecutor executor;
    protected final JobStateManagerThread stateManagerThread;

    protected boolean running = false;
    protected Future jobStateManagerThreadFuture;

    public JobStateManager(HibernateBundle hibernateBundle, JobStateManagerConfig config, JobRepo jobRepo, AttemptRepo attemptRepo, StateObserver stateObserver) {
        this.config = config;
        executor = new ScheduledThreadPoolExecutor(1); // single thread

        // The UnitOfWorkAwareProxyFactory is used to allow @UnitOfWork-annotations outside of the servlet container
        UnitOfWorkAwareProxyFactory proxyFactory = new UnitOfWorkAwareProxyFactory(hibernateBundle);
        stateManagerThread = proxyFactory.create(
                JobStateManagerThread.class,
                new Class[]{JobRepo.class, AttemptRepo.class, StateObserver.class},
                new Object[]{jobRepo, attemptRepo, stateObserver});
    }

    public void register(Environment environment) {
        environment.lifecycle().manage(this);
        environment.healthChecks().register(getClass().getName(), getHealthCheck());
    }

    @Override
    public void start() {
        if(isRunning()) {
            throw new IllegalStateException(String.format("%s is already running.", getClass().getName()));
        }
        jobStateManagerThreadFuture = executor.scheduleAtFixedRate(stateManagerThread,
                config.getIntervalSeconds(), config.getIntervalSeconds(), TimeUnit.SECONDS);
        running = true;
    }

    @Override
    public void stop() throws Exception {
        executor.shutdown();
        running = false;
    }

    public void runNow() {
        executor.execute(stateManagerThread);
    }

    public boolean isRunning() {
        return running;
    }

    public HealthCheck getHealthCheck() {
        return new HealthCheck() {
            @Override
            protected Result check() throws Exception {
                if(!isRunning()) {
                    return Result.unhealthy("not running");
                }
                if(executor.isShutdown() || executor.isTerminated() || executor.isTerminating()) {
                    return Result.unhealthy("Executor is either shut down, terminated or terminating.");
                }
                if(executor.getQueue().size() > 1) {
                    return Result.unhealthy("State management tasks are accumulating." +
                            " This could be a database performance issue or a misconfiguration" +
                            " (i.e. too short update time interval)");
                }
                if(jobStateManagerThreadFuture.isDone()) {
                    return Result.unhealthy("Thread appears to have crashed.");
                }
                return Result.healthy();
            }
        };
    }
}

class JobStateManagerThread implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final JobRepo jobRepo;
    private final AttemptRepo attemptRepo;
    private final StateObserver observer;

    public JobStateManagerThread(JobRepo jobRepo, AttemptRepo attemptRepo, StateObserver observer) {
        this.jobRepo = jobRepo;
        this.attemptRepo = attemptRepo;
        this.observer = observer;
    }

    @UnitOfWork
    void reallyCallOnNewJob() throws Exception {
        List<Job> jobs = jobRepo.findByNumAttempts(0);
        for(Job job : jobs) {
            observer.onNewJob(job);
        }
    }

    @UnitOfWork
    void reallyCallAttemptStateHandler(AttemptState state) throws Exception {
        List<Attempt> attempts = attemptRepo.findByState(state, -1);
        for(Attempt attempt : attempts) {
            if(attempt.getState() != state) {
                throw new IllegalStateException(String.format("Expected attempt to be in state %s, actual: %s",
                        state.name(), attempt.getState().name()));
            }
            observer.dispatch(attempt);
        }
    }

    @UnitOfWork
    void reallyCallAttemptFinalizedHandler() throws Exception {
        List<Attempt> attempts = attemptRepo.findByFinalized(false, 100);
        for(Attempt attempt : attempts) {

            if(attempt.getState().isFinal()) {
                attempt.setFinalized(true);
                attemptRepo.saveOrUpdate(attempt);
            }

            observer.dispatch(attempt);
        }
    }

    @UnitOfWork
    void reallyCallJobStateHandler(JobState state) throws Exception {
        List<Job> jobs = jobRepo.findJobsByStateOrNull(state, -1);
        for(Job job : jobs) {
            if(job.getState() != state) {
                throw new IllegalStateException(String.format("Expected attempt to be in state %s, actual: %s",
                        state.name(), job.getState().name()));
            }
            observer.dispatchJob(job);
        }
    }

    void callOnNewJob() {
        try {
            reallyCallOnNewJob();
        } catch (Exception e) {
            logger.warn("call to observer onNewJob() failed");
            e.printStackTrace();
        }
    }

    void callAttemptStateHandler(AttemptState state) {
        try {
            reallyCallAttemptStateHandler(state);
        } catch (Exception e) {
            logger.warn("call to observer state handler failed. State: " + state.name());
            e.printStackTrace();
        }
    }

    void callAttemptFinalizedHandler() {
        try {
            reallyCallAttemptFinalizedHandler();
        } catch (Exception e) {
            logger.warn("call to attemptFinalizedHandler failed");
            e.printStackTrace();
        }
    }

    void callJobStateHandler(JobState state) {
        try {
            reallyCallJobStateHandler(state);
        } catch (Exception e) {
            logger.warn("Call to job state observer handler failed. State: ", state.name());
            e.printStackTrace();
        }
    }

    public void run() {
        // To scheduler
        callOnNewJob();
        callAttemptStateHandler(AttemptState.QUEUED_LOCALLY);
        
        // Workaround for overloaded database.
        // Should be removed when the database optimization is done and switching to TIMED_OUT state is no longer an issue.
        //callAttemptStateHandler(AttemptState.RUNNING);

        // Non-finalized attempts
        callAttemptFinalizedHandler();

        //Job states
        callJobStateHandler(JobState.SUBMITTED);
        callJobStateHandler(JobState.DELAYED);
        callJobStateHandler(null);
    }
}