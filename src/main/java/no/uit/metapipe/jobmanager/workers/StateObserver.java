package no.uit.metapipe.jobmanager.workers;

import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class StateObserver {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    public void dispatch(Attempt attempt) throws Exception {
        switch (attempt.getState()) {
            case QUEUED_LOCALLY:
                onQueuedLocally(attempt);
                break;
            case TIMED_OUT:
                onTimedOut(attempt);
                break;
            case WORKFLOW_FAILED:
                onWorkflowFailed(attempt);
                break;
            case EXECUTOR_FAILED:
                onExecutorFailed(attempt);
                break;
            case ASSIGNED_EXECUTOR:
                onAssignedExecutor(attempt);
                break;
            case QUEUED_EXECUTOR:
                onQueuedExecutor(attempt);
                break;
            case RUNNING:
                onRunning(attempt);
                break;
            default:
                throw new IllegalArgumentException("Unsupported attempt state");
        }
    }

    public void dispatchJob(Job job) throws Exception {
        if (job.getState() == null) {
            onNull(job);
            logger.warn("Job had state NULL, this should only happen after first run");
            return;
        }
        switch (job.getState()) {
            case SUBMITTED:
                onSubmitted(job);
                break;
            case FINISHED:
            case CANCELLED:
                break;
            case DELAYED:
                onDelayed(job);
                break;
            default:
                throw new IllegalArgumentException("Unsupported job state");
        }
    }

    public abstract void onNewJob(Job job) throws Exception;

    public abstract void onQueuedLocally(Attempt attempt) throws Exception;
    public abstract void onTimedOut(Attempt attempt) throws Exception;
    public abstract void onWorkflowFailed(Attempt attempt) throws Exception;
    public abstract void onExecutorFailed(Attempt attempt) throws Exception;
    public abstract void onAssignedExecutor(Attempt attempt) throws Exception;
    public abstract void onQueuedExecutor(Attempt attempt) throws Exception;
    public abstract void onRunning(Attempt attempt) throws Exception;

    public abstract void onSubmitted(Job job) throws Exception;
    public abstract void onDelayed(Job job) throws Exception;
    public abstract void onNull(Job job) throws Exception;
}