package no.uit.metapipe.jobmanager.workers;

import no.uit.metapipe.jobmanager.config.JobStateManagerConfig;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import no.uit.metapipe.jobmanager.service.JobManagerService;
import no.uit.metapipe.jobmanager.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public class StateObserverImpl extends StateObserver {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    final JobRepo jobRepo;
    final AttemptRepo attemptRepo;
    final JobStateManagerConfig config;
    final JobManagerService jobManagerService;

    public StateObserverImpl(JobRepo jobRepo, AttemptRepo attemptRepo, JobStateManagerConfig config, JobManagerService jobManagerService) {
        this.jobRepo = jobRepo;
        this.attemptRepo = attemptRepo;
        this.config = config;
        this.jobManagerService = jobManagerService;
    }

    void scheduleAttempt(Attempt attempt) {
        // todo: find a suitable executor
        attempt.setExecutorId("default");
        attemptRepo.saveOrUpdate(attempt);
    }

    void checkLastHeartbeat(Attempt attempt) throws Exception {
        if (attempt.getLastHeartbeat() != null) {
            Date deadline = Date.from(attempt.getLastHeartbeat().toInstant().plusSeconds(config.getExecutorHeartbeatTimeoutSeconds()));
            if(Date.from(Instant.now()).after(deadline)) {
                attempt.setState(AttemptState.TIMED_OUT);
                attemptRepo.saveOrUpdate(attempt);
            }
        }
    }

    String createId() {
        return "so_" + Util.randomString(10);
    }

    @Override
    public void onNewJob(Job job) throws Exception {
        Attempt attempt = job.createAttempt(createId());
        jobRepo.saveOrUpdate(job);
        logger.info(String.format("Created new attempt for job: jobId: %s, attemptId: %s", job.getJobId(), attempt.getAttemptId()));
    }

    @Override
    public void onQueuedLocally(Attempt attempt) throws Exception {
        if(attempt.getExecutorId() == null) {
            scheduleAttempt(attempt);
        }
    }

    @Override
    public void onTimedOut(Attempt attempt) throws Exception {
        if(attempt.getJob().getLatestAttempt().get().getAttemptId().equals(attempt.getAttemptId())) {
            Job job = attempt.getJob();
            job.restart();
            jobRepo.saveOrUpdate(job);
        }
    }

    @Override
    public void onWorkflowFailed(Attempt attempt) throws Exception {
        /*
            There's nothing to do here -- the admin has to fix this state.
            We might implement notifications at some point, perhaps
            by sending an email.
         */
        //TODO: Integrate with mailing service, implement a handle for /notifyAdmin in newpango/mailer
    }

    @Override
    public void onExecutorFailed(Attempt attempt) throws Exception {
        // todo: restart the job after a timeout
    }

    @Override
    public void onAssignedExecutor(Attempt attempt) throws Exception {
        checkLastHeartbeat(attempt);
    }

    @Override
    public void onQueuedExecutor(Attempt attempt) throws Exception {
        checkLastHeartbeat(attempt);
    }

    @Override
    public void onRunning(Attempt attempt) throws Exception {
        checkLastHeartbeat(attempt);
    }

    @Override
    public void onSubmitted(Job job) throws Exception {
        // Alternative for workaround in JobStateManager.java
        //Optional<Attempt> latestAttemptOptional = job.getLatestAttempt();
        //if(latestAttemptOptional.isPresent() /*&& latestAttemptOptional.get().getState() == AttemptState.RUNNING*/) {
        //    dispatch(latestAttemptOptional.get());
        //}
        doJobUpdate(job);
    }

    @Override
    public void onDelayed(Job job) throws Exception {
        // Alternative for workaround in JobStateManager.java
        //Optional<Attempt> latestAttemptOptional = job.getLatestAttempt();
        //if(latestAttemptOptional.isPresent() /*&& latestAttemptOptional.get().getState() == AttemptState.RUNNING*/) {
        //    dispatch(latestAttemptOptional.get());
        //}
        doJobUpdate(job);
    }

    @Override
    public void onNull(Job job) throws Exception {
        logger.warn("Handling job: %s, has state null", job.getState().name());
        doJobUpdate(job);
    }

    private void doJobUpdate(Job job) {
        job.recalculateState();
        jobRepo.saveOrUpdate(job);
    }
}
