package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import no.uit.metapipe.jobmanager.core.Attempt;

public class ExecutorJobDto extends UserJobDto {
    private AttemptDto attempt;

    private String attemptUri = "";
    private String selfUri = "";

    public static ExecutorJobDto fromAttempt(Attempt attempt) {
        ExecutorJobDto executorJobDto = new ExecutorJobDto();
        executorJobDto.copyFromAttempt(attempt);
        return executorJobDto;
    }

    protected void copyFromAttempt(Attempt attempt) {
        copyFromJob(attempt.getJob());
        this.attempt = AttemptDto.fromAttempt(attempt, true, true);
    }

    public String getAttemptUri() {
        return attemptUri;
    }

    public void setAttemptUri(String attemptUri) {
        this.attemptUri = attemptUri;
    }

    public AttemptDto getAttempt() {
        return attempt;
    }

    public void setAttempt(AttemptDto attempt) {
        this.attempt = attempt;
    }

    @JsonIgnore
    public void setUrisByPrefix(String uriPrefix) {
        this.attemptUri = uriPrefix + "/executors/" + attempt.getExecutorId() + "/jobs/" + getJobId() + "/attempts/" + attempt.getAttemptId();
        this.selfUri = this.attemptUri + "/full";
    }

    public String getSelfUri() {
        return selfUri;
    }

    public void setSelfUri(String selfUri) {
        this.selfUri = selfUri;
    }
}
