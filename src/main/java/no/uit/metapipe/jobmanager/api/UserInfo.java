package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UserInfo {

    @JsonProperty("sub")
    String sub;
    @JsonProperty("name")
    String realName;
    @JsonProperty("email")
    String email;


    @JsonProperty("organization")
    String organization;
    @JsonProperty("organizationAffiliations")
    List<String> organizationAffiliations;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public List<String> getOrganizationAffiliations() {
        return organizationAffiliations;
    }

    public void setOrganizationAffiliations(List<String> organizationAffiliations) {
        this.organizationAffiliations = organizationAffiliations;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
