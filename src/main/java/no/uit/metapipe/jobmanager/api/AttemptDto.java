package no.uit.metapipe.jobmanager.api;

import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.exceptions.InvalidDtoException;

import java.util.HashMap;
import java.util.Map;

public class AttemptDto {
    String executorId;
    String state;
    String attemptId;
    String tag;
    String timeCreated;
    String timeStarted;
    String timeEnded;
    String lastHeartbeat;
    Long runtimeMillis;
    Long queueDurationMillis;

    Map<String, DatasetRefDto> outputs = new HashMap<>();
    Map<String, String> properties = new HashMap<>();

    long priority;

    public static AttemptDto fromAttempt(Attempt attempt, boolean outputsReadAccess, boolean outputsWriteAccess) {
        AttemptDto attemptDto = new AttemptDto();
        attemptDto.copyFromAttempt(attempt, outputsReadAccess, outputsWriteAccess);
        return attemptDto;
    }

    protected void copyFromAttempt(Attempt attempt, boolean outputsReadAccess, boolean outputsWriteAccess) {
        setExecutorId(attempt.getExecutorId());
        setState(attempt.getState().toString());
        setTag(attempt.getTag());
        setAttemptId(attempt.getAttemptId());
        setTimeCreated(Long.toString(attempt.getCreationTimestamp().getTime()));
        setRuntimeMillis(attempt.getRuntime().toMillis());
        setQueueDurationMillis(attempt.getQueueDuration().toMillis());

        if(attempt.getTimeStarted() != null) {
            setTimeStarted(Long.toString(attempt.getTimeStarted().getTime()));
        }

        if(attempt.getTimeEnded() != null) {
            setTimeEnded(Long.toString(attempt.getTimeEnded().getTime()));
        }

        if(attempt.getLastHeartbeat() != null) {
            setLastHeartbeat(Long.toString(attempt.getLastHeartbeat().getTime()));
        }

        Map<String, DatasetRef> outputs = attempt.getOutputDatasets();

        for(String key : outputs.keySet()) {
            DatasetRef datasetRef = outputs.get(key);
            DatasetRefDto datasetRefDto = DatasetRefDto.fromDatasetRef(datasetRef, outputsReadAccess, outputsWriteAccess);
            this.outputs.put(key, datasetRefDto);
        }

        setPriority(attempt.getPriority());

        for(String key : attempt.getProperties().keySet()) {
            this.properties.put(key, attempt.getProperties().get(key));
        }
    }

    public void validateNewAttempt() throws InvalidDtoException {
        if(tag == null) throw new InvalidDtoException("tag cannot be null");
    }

    public String getExecutorId() {
        return executorId;
    }

    public void setExecutorId(String executorId) {
        this.executorId = executorId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(String attemptId) {
        this.attemptId = attemptId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getTimeStarted() {
        return timeStarted;
    }

    public void setTimeStarted(String timeStarted) {
        this.timeStarted = timeStarted;
    }

    public String getTimeEnded() {
        return timeEnded;
    }

    public void setTimeEnded(String timeEnded) {
        this.timeEnded = timeEnded;
    }

    public String getLastHeartbeat() {
        return lastHeartbeat;
    }

    public void setLastHeartbeat(String lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public Long getRuntimeMillis() {
        return runtimeMillis;
    }

    public void setRuntimeMillis(Long runtimeMillis) {
        this.runtimeMillis = runtimeMillis;
    }


    public Long getQueueDurationMillis() {
        return queueDurationMillis;
    }

    public void setQueueDurationMillis(Long queueDurationMillis) {
        this.queueDurationMillis = queueDurationMillis;
    }

    public Map<String, DatasetRefDto> getOutputs() {
        return outputs;
    }

    public void setOutputs(Map<String, DatasetRefDto> outputs) {
        this.outputs = outputs;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
