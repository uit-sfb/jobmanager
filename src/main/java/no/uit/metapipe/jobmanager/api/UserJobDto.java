package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.exceptions.InvalidDtoException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserJobDto extends JobDto {
    private Map<String, DatasetRefDto> inputs = new HashMap<>();
    private Map<String, DatasetRefDto> outputs = new HashMap<>();

    protected void copyFromJob(Job job) {
        super.copyFromJob(job);

        Set<String> inputKeys = job.getInputDatasets().keySet();
        for(String key : inputKeys) {
            DatasetRefDto ref = DatasetRefDto.fromDatasetRef(job.getInputDatasets().get(key), false, false);
            this.inputs.put(key, ref);
        }

        Map<String, DatasetRef> jobOutputs = job.getJobOutputs();
        Set<String> outputKeys = jobOutputs.keySet();

        for(String key : outputKeys) {
            DatasetRefDto ref = DatasetRefDto.fromDatasetRef(jobOutputs.get(key), false, false);
            this.outputs.put(key, ref);
        }
    }

    public void validateSubmittedJob() throws InvalidDtoException {
        if(getTag() == null) {
            throw new InvalidDtoException("tag is missing");
        }
        if(getParameters() == null) {
            throw new InvalidDtoException("parameters are missing");
        }
    }

    public Map<String, DatasetRefDto> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, DatasetRefDto> inputs) {
        this.inputs = inputs;
    }

    public Map<String, DatasetRefDto> getOutputs() {
        return outputs;
    }

    public void setOutputs(Map<String, DatasetRefDto> outputs) {
        this.outputs = outputs;
    }
}
