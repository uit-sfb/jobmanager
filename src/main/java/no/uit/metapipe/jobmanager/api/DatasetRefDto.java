package no.uit.metapipe.jobmanager.api;


import com.fasterxml.jackson.annotation.JsonInclude;
import no.uit.metapipe.jobmanager.core.DatasetRef;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatasetRefDto {
    String url;
    String readBearerToken;
    String writeBearerToken;

    public static DatasetRefDto fromUrl(String url) {
        DatasetRefDto dto = new DatasetRefDto();
        dto.setUrl(url);
        return dto;
    }

    public static DatasetRefDto fromDatasetRef(DatasetRef datasetRef, boolean readAccess, boolean writeAccess) {
        DatasetRefDto dto = new DatasetRefDto();
        dto.setUrl(datasetRef.getUrl());
        if(readAccess) {
            dto.setReadBearerToken(datasetRef.getReadBearerToken());
        }
        if(writeAccess) {
            dto.setWriteBearerToken(datasetRef.getWriteBearerToken());
        }
        return dto;
    }

    public static DatasetRefDto fromReadAccessibleDataset(DatasetRef datasetRef) {
        return fromDatasetRef(datasetRef, true, false);
    }

    public static DatasetRefDto fromWriteAccessibleDataset(DatasetRef datasetRef) {
        return fromDatasetRef(datasetRef, false, true);
    }

    public static DatasetRefDto fromReadWriteAccessibleDataset(DatasetRef datasetRef) {
        return fromDatasetRef(datasetRef, true, true);
    }

    public void copyToDatasetRef(DatasetRef datasetRef, String key) {
        datasetRef.setKey(key);
        datasetRef.setUrl(getUrl());
        datasetRef.setReadBearerToken(getReadBearerToken());
        datasetRef.setWriteBearerToken(getWriteBearerToken());
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReadBearerToken() {
        return readBearerToken;
    }

    public void setReadBearerToken(String readBearerToken) {
        this.readBearerToken = readBearerToken;
    }

    public String getWriteBearerToken() {
        return writeBearerToken;
    }

    public void setWriteBearerToken(String writeBearerToken) {
        this.writeBearerToken = writeBearerToken;
    }
}
