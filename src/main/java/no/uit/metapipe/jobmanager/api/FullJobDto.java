package no.uit.metapipe.jobmanager.api;

import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;

import java.util.*;

public class FullJobDto extends UserJobDto {
    Long totalRuntimeMillis;
    Long totalQueueDurationMillis;

    List<AttemptDto> attempts = new LinkedList<>();

    private Map<String, DatasetRefDto> inputs = new HashMap<>();
    private Map<String, DatasetRefDto> outputs = new HashMap<>();

    public static FullJobDto fromJob(Job job) {
        FullJobDto fullJobDto = new FullJobDto();
        fullJobDto.copyFromJob(job);
        return fullJobDto;
    }

    public static FullJobDto fromJobUserInfo(Job job, UserInfo userInfo) {
        FullJobDto fullJobDto = new FullJobDto();
        fullJobDto.copyFromJobUserInfo(job, userInfo);
        return fullJobDto;
    }

    protected void copyFromJob(Job job) {
        super.copyFromJob(job);

        setTotalRuntimeMillis(job.getTotalRuntime().toMillis());
        setTotalQueueDurationMillis(job.getTotalQueueDuration().toMillis());

        for(Attempt attempt : job.getAttempts()) {
            attempts.add(AttemptDto.fromAttempt(attempt, true, true));
        }


        Set<String> inputKeys = job.getInputDatasets().keySet();
        for(String key : inputKeys) {
            DatasetRefDto ref = DatasetRefDto.fromDatasetRef(job.getInputDatasets().get(key), true, true);
            inputs.put(key, ref);
        }

        Map<String, DatasetRef> jobOutputs = job.getJobOutputs();
        Set<String> outputKeys = jobOutputs.keySet();

        for(String key : outputKeys) {
            DatasetRefDto ref = DatasetRefDto.fromDatasetRef(jobOutputs.get(key), true, true);
            this.outputs.put(key, ref);
        }
    }

    public Long getTotalRuntimeMillis() {
        return totalRuntimeMillis;
    }

    public void setTotalRuntimeMillis(Long totalRuntimeMillis) {
        this.totalRuntimeMillis = totalRuntimeMillis;
    }

    public Long getTotalQueueDurationMillis() {
        return totalQueueDurationMillis;
    }

    public void setTotalQueueDurationMillis(Long totalQueueDurationMillis) {
        this.totalQueueDurationMillis = totalQueueDurationMillis;
    }

    public List<AttemptDto> getAttempts() {
        return attempts;
    }

    public void setAttempts(List<AttemptDto> attempts) {
        this.attempts = attempts;
    }

    @Override
    public Map<String, DatasetRefDto> getInputs() {
        return inputs;
    }

    @Override
    public void setInputs(Map<String, DatasetRefDto> inputs) {
        this.inputs = inputs;
    }

    @Override
    public Map<String, DatasetRefDto> getOutputs() {
        return outputs;
    }

    @Override
    public void setOutputs(Map<String, DatasetRefDto> outputs) {
        this.outputs = outputs;
    }
}
