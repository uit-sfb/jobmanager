package no.uit.metapipe.jobmanager.api;

import no.uit.metapipe.jobmanager.core.Job;

public class SimplifiedJobDto {
    private String jobId;
    private String timeSubmitted;
    private String state;
    private String userId;
    private String groupId;
    private String tag;
    private long priority;
    private boolean hold;
    private UserInfo userInfo = null;

    public static JobDto fromJob(Job job) {
        JobDto jobDto = new JobDto();
        jobDto.copyFromJob(job);
        return jobDto;
    }

    protected void copyFromJob(Job job) {
        this.setJobId(job.getJobId());
        this.setTimeSubmitted(Long.toString(job.getCreationTimestamp().getTime()));
        this.setState(job.getState().toString());
        this.setUserId(job.getUserId());
        this.setGroupId(job.getGroupId());
        this.setTag(job.getTag());
        this.setPriority(job.getPriority());
        this.setHold(job.getHold());
    }

    protected void copyFromJobUserInfo(Job job, UserInfo userInfo) {
        copyFromJob(job);
        this.userInfo = userInfo;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTimeSubmitted() {
        return timeSubmitted;
    }

    public void setTimeSubmitted(String timeSubmitted) {
        this.timeSubmitted = timeSubmitted;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public boolean getHold() {
        return hold;
    }

    public void setHold(boolean hold) {
        this.hold = hold;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
