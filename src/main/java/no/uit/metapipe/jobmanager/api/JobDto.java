package no.uit.metapipe.jobmanager.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.uit.metapipe.jobmanager.core.Job;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JobDto extends SimplifiedJobDto {
    private JsonNode parameters;
    private Map<String, String> properties = new HashMap<>();

    public static UserJobDto fromJob(Job job) {
        UserJobDto jobDto = new UserJobDto();
        jobDto.copyFromJob(job);
        return jobDto;
    }

    protected void copyFromJob(Job job) {
        super.copyFromJob(job);
        updateProperties(job);
    }

    protected void copyFromJobUserInfo(Job job, UserInfo userInfo) {
        super.copyFromJobUserInfo(job, userInfo);
        updateProperties(job);
    }

    @JsonIgnore
    public String getParametersAsString() {
        return parameters.toString();
    }

    public JsonNode getParameters() {
        return parameters;
    }

    public void setParameters(JsonNode parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
    private void updateProperties(Job job) {
        for(String key : job.getProperties().keySet()) {
            properties.put(key, job.getProperty(key));
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            setParameters(mapper.readTree(job.getParameters()));
        } catch (IOException e) {
            throw new IllegalStateException("Job parameters is not a valid JSON object.", e);
        }
    }
}
