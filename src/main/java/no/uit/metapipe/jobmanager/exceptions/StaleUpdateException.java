package no.uit.metapipe.jobmanager.exceptions;

public class StaleUpdateException extends Exception {
    public StaleUpdateException(String message) {
        super(message);
    }
}
