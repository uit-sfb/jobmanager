package no.uit.metapipe.jobmanager.exceptions;

public class InvalidDtoException extends Exception {
    public InvalidDtoException(String message) {
        super(message);
    }
}
