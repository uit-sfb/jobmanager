package no.uit.metapipe.jobmanager.exceptions;

public class JobManagerException extends Exception {
    public JobManagerException() {
    }

    public JobManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobManagerException(String message) {
        super(message);
    }
}
