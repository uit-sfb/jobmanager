package no.uit.metapipe.jobmanager.exceptions;

public class AuthorizationException extends Exception {
    public AuthorizationException(String message) {
        super(message);
    }

    public AuthorizationException() {
        super("Not authorized");
    }
}
