package no.uit.metapipe.jobmanager.exceptions;

public class IllegalStateTransitionException extends JobManagerException {
    public IllegalStateTransitionException(String from, String to) {
        super(String.format("Illegal state transition from %s -> %s", from, to));
    }
}
