package no.uit.metapipe.jobmanager.db;


import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.AttemptState;
import org.hibernate.SessionFactory;
import org.hibernate.internal.util.collections.ConcurrentReferenceHashMap;
import org.hibernate.query.Query;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class AttemptRepo extends AbstractDAO<Attempt> {
    public AttemptRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Attempt saveOrUpdate(Attempt attempt) {
        currentSession().saveOrUpdate(attempt);
        return attempt;
    }

    public Optional<Attempt> findByAttemptId(String attemptId) {
        Query q = currentSession().getNamedQuery("findAttemptByAttemptId")
                .setParameter("attemptId", attemptId);
        List<Attempt> attempts = list(q);
        switch (attempts.size()) {
            case 0:
                return Optional.empty();
            case 1:
                return Optional.of(attempts.get(0));
            default:
                throw new IllegalStateException("An attempt ID should map to exactly one attempt.");
        }
    }

    public List<Attempt> findByJobId(String jobId) {
        Query q = currentSession().getNamedQuery("findAttemptsByJobId")
                .setParameter("jobId", jobId);
        return list(q);
    }

    public Optional<Attempt> findByJobIdAndAttemptId(String jobId, String attemptId) {
        Query q = currentSession().getNamedQuery("findAttemptByJobIdAndAttemptId")
                .setParameter("jobId", jobId)
                .setParameter("attemptId", attemptId);
        List<Attempt> attempts = list(q);
        switch (attempts.size()) {
            case 0:
                return Optional.empty();
            case 1:
                return Optional.of(attempts.get(0));
            default:
                throw new IllegalStateException("A job ID and an attempt ID should map to exactly one attempt.");
        }
    }

    public List<Attempt> findByExecutorIdAndState(String executorId, AttemptState state, int maxResults) {
        Query q = currentSession().getNamedQuery("findByExecutorIdAndState")
                .setParameter("state", state)
                .setParameter("executorId", executorId);

        if (maxResults >= 0) {
            q.setMaxResults(maxResults);
        }

        return list(q);
    }

    public List<Attempt> findByExecutorIdAndState(String executorId, AttemptState state) {
        return findByExecutorIdAndState(executorId, state, -1);
    }

    public List<Attempt> findByState(AttemptState state, int maxResults) {
        Query q = currentSession().getNamedQuery("findByState")
                .setParameter("state", state);

        if (maxResults >= 0) {
            q.setMaxResults(maxResults);
        }

        return list(q);
    }

    public List<Attempt> findByStateAndTags(AttemptState state, Set<String> tags, int maxResults) {
        if(tags.isEmpty()) {
            return new LinkedList<>();
        } else {
            Query q = currentSession().getNamedQuery("findByStateAndTags")
                    .setParameter("state", state)
                    .setParameterList("tags", tags);

            if (maxResults >= 0) {
                q.setMaxResults(maxResults);
            }

            return list(q);
        }
    }

    public List<Attempt> findByFinalized(boolean finalized, int maxResults) {
        Query q = currentSession().getNamedQuery("findByFinalized")
                .setParameter("finalized", finalized);
        if(maxResults >= 0) {
            q.setMaxResults(maxResults);
        }
        return list(q);
    }
}
