package no.uit.metapipe.jobmanager.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.core.JobState;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class JobRepo extends AbstractDAO<Job> {

    public JobRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Job saveOrUpdate(Job job) {
        currentSession().saveOrUpdate(job);
        return job;
    }

    public List<Job> findAll() {
        Query q = currentSession().getNamedQuery("findAllJobs");
        return list(q);
    }

    public Optional<Job> findByJobId(String jobId) {
        Session session = currentSession();
        Query q = session.getNamedQuery("jobByJobId")
                .setParameter("jobId", jobId);
        List<Job> jobs = list(q);
        if(jobs.size() != 1) {
            return Optional.empty();
        } else {
            return Optional.of(jobs.get(0));
        }
    }

    public List<Job> findByUserId(String userId) {
        Query q = currentSession().getNamedQuery("jobByUserId")
                .setParameter("userId", userId);
        return list(q);
    }

    public List<Job> findByNumAttempts(int numAttempts) {
        Query q = currentSession().getNamedQuery("jobsWithNumAttempts")
                .setParameter("numAttempts", numAttempts);
        return list(q);
    }

    public List<Job> findRecentlyActiveJobs() {
        Query q = currentSession()
                //.createSQLQuery("select * from job where id in (select job_id from (select distinct on (j.id) *, a.updatetimestamp as att from job j join attempt a on a.job_id = j.id where cancelled='f' order by j.id, a.attempts_order desc) s where (s.state != 'FINISHED'));")
                .createSQLQuery("select * from job where state in ('SUBMITTED', 'DELAYED')")
                .addEntity(Job.class);
        return list(q);
    }

    public List<Job> findJobsByStateOrNull(JobState state, int maxResults) {
        Query q;
        if (state == null) {
            q = currentSession().getNamedQuery("findJobsByStateNull");
        } else {
            q = currentSession().getNamedQuery("findJobsByState").setParameter("jobState", state);
        }
        if (maxResults >= 0) {
            q.setMaxResults(maxResults);
        }
        return list(q);
    }
}
