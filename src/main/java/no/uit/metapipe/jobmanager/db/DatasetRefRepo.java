package no.uit.metapipe.jobmanager.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import org.hibernate.SessionFactory;

public class DatasetRefRepo extends AbstractDAO<DatasetRef> {
    public DatasetRefRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public DatasetRef saveOrUpdate(DatasetRef datasetRef) {
        currentSession().saveOrUpdate(datasetRef);
        return datasetRef;
    }
}
