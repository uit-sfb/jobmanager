package no.uit.metapipe.jobmanager.db;

import no.uit.metapipe.jobmanager.config.JobTagConfig;
import no.uit.metapipe.jobmanager.core.TagInfo;

import java.util.List;
import java.util.Optional;

public class TagInfoRepo {
    private final List<JobTagConfig> tagConfigs;

    public TagInfoRepo(List<JobTagConfig> tagConfigs) {
        this.tagConfigs = tagConfigs;
    }

    public Optional<TagInfo> findByTagName(String tagName) {
        for(JobTagConfig c : tagConfigs) {
            if (c.getTag().equals(tagName)) {
                return Optional.of(TagInfo.fromJobTagConfig(c));
            }
        }
        return Optional.empty();
    }

}
