package no.uit.metapipe.jobmanager.config;

public class AuthConfig {
    String tokenIntrospectionEndpoint;
    String usersEndpoint;
    boolean disabled = false;

    public AuthConfig() {
    }

    public AuthConfig(boolean disabled) {
        this.disabled = disabled;
    }

    public String getTokenIntrospectionEndpoint() {
        return tokenIntrospectionEndpoint;
    }

    public String getUsersEndpoint() {return usersEndpoint;}

    public boolean isDisabled() {
        return disabled;
    }
}
