package no.uit.metapipe.jobmanager.config;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class JobTagConfig {
    @JsonProperty
    String tag;

    @JsonProperty
    long priority;

    public String getTag() {
        return tag;
    }

    public long getPriority() {
        return priority;
    }
}
