package no.uit.metapipe.jobmanager.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class JobManagerConfig extends Configuration {
    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    @JsonProperty
    private FlywayFactory flyway = new FlywayFactory();

    private String publicUri = "";

    @JsonProperty
    public JobStateManagerConfig stateManager = new JobStateManagerConfig();

    @JsonProperty
    AuthConfig auth;

    List<JobTagConfig> tags = new ArrayList<>();

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public FlywayFactory getFlywayFactory() {
        return flyway;
    }

    public void setFlywayFactory(FlywayFactory flywayFactory) {
        this.flyway = flywayFactory;
    }

    public String getPublicUri() {
        return publicUri;
    }

    public JobStateManagerConfig getStateManagerConfig() {
        return stateManager;
    }

    public List<JobTagConfig> getTags() {
        return tags;
    }

    public AuthConfig getAuth() {
        return auth;
    }
}
