package no.uit.metapipe.jobmanager.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobStateManagerConfig {
    @JsonProperty
    int intervalSeconds = 10;

    @JsonProperty
    int executorHeartbeatTimeoutSeconds = 600;

    public int getIntervalSeconds() {
        return intervalSeconds;
    }

    public int getExecutorHeartbeatTimeoutSeconds() {
        return executorHeartbeatTimeoutSeconds;
    }
}
