package no.uit.metapipe.jobmanager.util;

import java.security.SecureRandom;
import java.util.Random;

public class Util {

    public static void print(Object stuff) {
        System.out.print("\n||||| SYSTEM.OUT ||||| \n" + stuff + "\n\n");
    }

    public static String randomString(int length) {
        return randomString(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    }
    public static String randomString(int length, String characterSetString) {
        Random random = new SecureRandom();
        char[] characterSet = characterSetString.toCharArray();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }
    public static boolean isValidId(String id, boolean nullAllowed) {
        if((nullAllowed && id == null) || (id != null && !id.equals("null") && id.matches("^[a-zA-Z0-9-_]+$"))) {
            return true;
        }
        return false;
    }
    public static String validateId(String newID, boolean nullAllowed) throws IllegalArgumentException {
        if(isValidId(newID, nullAllowed)) {
            return newID;
        }
        throw new IllegalArgumentException();
    }
}
