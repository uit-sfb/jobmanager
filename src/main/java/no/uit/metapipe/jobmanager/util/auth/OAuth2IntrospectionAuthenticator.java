package no.uit.metapipe.jobmanager.util.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import no.uit.metapipe.jobmanager.config.AuthConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import java.security.Principal;
import java.util.Optional;

public class OAuth2IntrospectionAuthenticator implements Authenticator<String, Principal> {
    private final AuthConfig authConfig;
    private final Client client = ClientBuilder.newClient();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public OAuth2IntrospectionAuthenticator(AuthConfig authConfig) {
        this.authConfig = authConfig;
    }

    @Override
    public Optional<Principal> authenticate(String bearerToken) throws AuthenticationException {
        IntrospectionResponse response;
        try {
            response = client.target(authConfig.getTokenIntrospectionEndpoint()).property("Accept", "application/json").request()
                    .post(Entity.form(new Form("token", bearerToken)), IntrospectionResponse.class);
        } catch (Exception e) {
            logger.warn("Token introspection failed:", e);

            if(authConfig.isDisabled()) {
                logger.debug("Token instrospectino server is unavailable, but authorization is disabled.");
                AuthCtx authCtx = new AuthCtx(null, authConfig);
                return Optional.of(authCtx);
            } else {
                throw new AuthenticationException("Token introspection endpoint is unavailable", e);
            }
        }

        if(response.isActive()) {
            AuthCtx authCtx = new AuthCtx(response, authConfig);
            logger.debug("Authenticated request: " + authCtx.toString());
            return Optional.of(authCtx);
        } else {
            logger.debug("Authentication failed. Token introspection returned enabled=false.");
            return Optional.empty();
        }
    }
}
