package no.uit.metapipe.jobmanager.util.auth;

import no.uit.metapipe.jobmanager.api.UserJobDto;
import no.uit.metapipe.jobmanager.config.AuthConfig;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public class AuthCtx implements Principal {
    public static AuthCtx disabled() {
        AuthConfig config = new AuthConfig(true);
        return new AuthCtx(null, config);
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final IntrospectionResponse introspectionResponse;
    private final AuthConfig authConfig;
    private List<UriScope> authorizedScopes = new ArrayList<>(10);

    public AuthCtx(IntrospectionResponse introspectionResponse, AuthConfig authConfig) {
        this.introspectionResponse = introspectionResponse;
        this.authConfig = authConfig;

        this.initializeScopes();
    }

    public boolean isDisabled() {
        return authConfig.isDisabled();
    }


    // Authorization rules

    public boolean canPopExecutorQueue(String executorName) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("POST", String.format("jobs/executors/%s/queue", executorName)));
    }

    public boolean canGetExecutorQueue(String executorId) {
        return canPopExecutorQueue(executorId) || scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/executors/%s/queue", executorId)));
    }

    public boolean canGetAllLocallyQueuedAttempts() {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", "/executors/queue"));
    }

    public boolean canFindExecutorAttemptByIds(String executorId, String jobId, String attemptId) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/executors/%s/jobs/%s/attempts/%s", executorId, jobId, attemptId)));
    }

    public boolean canUpdateAttemptState(Attempt attempt) {
        // Executor API
        return scopeIsAuthorized(UriScope.fromMethodAndUri("PUT", String.format("jobs/executors/%s/jobs/%s/attempts/%s/state", attempt.getExecutorId(), attempt.getJob().getJobId(), attempt.getAttemptId())));
    }

    public boolean canSetOutputDataset(Attempt attempt) {
        String executorId = attempt.getExecutorId();
        String jobId = attempt.getJob().getJobId();
        String attemptId = attempt.getAttemptId();

        // Executor API
        return scopeIsAuthorized(UriScope.fromMethodAndUri("PUT", String.format("jobs/executors/%s/jobs/%s/attempts/%s/outputs/", executorId, jobId, attemptId)));
    }

    public boolean canUpdateHeartbeat(Attempt attempt) {
        String executorId = attempt.getExecutorId();
        String jobId = attempt.getJob().getJobId();
        String attemptId = attempt.getAttemptId();

        // Executor API
        return scopeIsAuthorized(UriScope.fromMethodAndUri("POST", String.format("jobs/executors/%s/jobs/%s/attempts/%s/heartbeat", executorId, jobId, attemptId)));
    }

    public boolean canReadUserJobs(String userId) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/users/%s/jobs", userId)));
    }

    public boolean canSubmitJobForUser(String userId, UserJobDto jobDto) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("PUT", String.format("jobs/users/%s/jobs/", userId)));
    }

    public boolean canReadAllJobs() {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", "jobs/jobs"));
    }

    public boolean canReadRecentlyActiveJobs() {
        return canReadAllJobs() || scopeIsAuthorized(UriScope.fromMethodAndUri("GET", "jobs/jobs/recentlyActive"));
    }

    public boolean canFindAttemptsByJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/jobs/%s/attempts", job.getJobId())));
    }

    public boolean canReadJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/jobs/%s", job.getJobId())));
    }

    public boolean canReadAttempt(Attempt attempt) {
        // via /jobs API
        String jobId = attempt.getJob().getJobId();
        return scopeIsAuthorized(UriScope.fromMethodAndUri("GET", String.format("jobs/jobs/{%s}/attempts/{%s}", jobId, attempt.getAttemptId())));
    }

    public boolean canCreateAttemptForJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("PUT", String.format("jobs/jobs/%s/attempts/", job.getJobId())));
    }

    public boolean canCancelJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("POST", String.format("jobs/jobs/%s/cancel", job.getJobId())));
    }

    public boolean canRestartJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("POST", String.format("jobs/jobs/%s/restart", job.getJobId())));
    }

    public boolean canHoldJob(Job job) {
        return scopeIsAuthorized(UriScope.fromMethodAndUri("PUT", String.format("jobs/jobs/%s/hold", job.getJobId())));
    }

    public boolean canUpdateAttemptProperties(Attempt attempt) {
        String executorId = attempt.getExecutorId();
        String jobId = attempt.getJob().getJobId();
        String attemptId = attempt.getAttemptId();

        // Executor API
        return scopeIsAuthorized(UriScope.fromMethodAndUri("POST", String.format("jobs/executors/%s/jobs/%s/attempts/%s/properties", executorId, jobId, attemptId)));
    }


    // Boilerplate

    @Override
    public boolean equals(Object another) {
        return this == another;
    }

    @Override
    public String toString() {
        String scope = "";
        if(this.introspectionResponse != null) {
            scope = introspectionResponse.getScope();
        }
        return String.format("AuthCtx [name=%s, disabled=%b, scope=%s]", getName(), isDisabled(), scope);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String getName() {
        if(authConfig.isDisabled()) {
            return "[disabled]";
        }
        return introspectionResponse.getSub();
    }


    // Private

    protected void initializeScopes() {
        if(this.isDisabled() && introspectionResponse == null) {
            return;
        }
        String scope = introspectionResponse.getScope();
        if(scope == null) {
            logger.debug("Scope returned from introspection response equals null.");
        } else {
            logger.debug("Scope: " + scope);
            for(String encodedScope : scope.split(" ")) {
                authorizedScopes.add(UriScope.fromString(encodedScope));
            }
        }
    }

    protected boolean scopeIsAuthorized(UriScope requiredScope) {
        for(UriScope providedScope : this.authorizedScopes) {
            if(requiredScope.isAuthorizedBy(providedScope)) {
                logger.debug(String.format("Access granted: Required scope %s, provided scope %s.", requiredScope.encodeString(), providedScope.encodeString()));
                return true;
            }
        }

        if(this.isDisabled()) {
            logger.warn(String.format("Allowing unauthorized request (authorization is disabled in config). Required scope: %s. %s", requiredScope.encodeString(), this.toString()));
            return true;
        } else {
            logger.debug("Access denied: required scope " + requiredScope.encodeString());
            return false;
        }
    }
}
