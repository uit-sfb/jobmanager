package no.uit.metapipe.jobmanager.resources;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import no.uit.metapipe.jobmanager.config.AuthConfig;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;
import no.uit.metapipe.jobmanager.api.*;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.api.UserInfo;
import no.uit.metapipe.jobmanager.exceptions.*;
import no.uit.metapipe.jobmanager.exceptions.NotFoundException;
import no.uit.metapipe.jobmanager.service.JobManagerService;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class JobManagerResource {

    private final JobManagerService jobService;
    private final JobManagerConfig jobManagerConfig;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public JobManagerResource(JobManagerService jobService, JobManagerConfig jobManagerConfig) {
        this.jobService = jobService;
        this.jobManagerConfig = jobManagerConfig;
    }

    private Response noSuchAttemptError() {
        return Response.status(Response.Status.NOT_FOUND).entity("not found: attemptId, jobId or executorId does not exist").build();
    }

    private Response noSuchJobError() {
        return Response.status(Response.Status.NOT_FOUND).entity("Job does not exist").build();
    }

    private Response illegalStateTransition(IllegalStateTransitionException e) {
        return Response.status(Response.Status.CONFLICT).entity(e.getMessage() + "\n").build();
    }

    private Response resourceCreated(Object resource) {
        return Response.status(Response.Status.CREATED).entity(resource).build();
    }

    private Response badRequest(String message) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(message)
                .build();
    }

    private Response forbidden(String message) {
        return Response.status(Response.Status.FORBIDDEN)
                .entity(message)
                .build();
    }

    private AuthCtx getAuthCtx(Optional<AuthCtx> authCtxOptional) throws AuthorizationException {
        if(authCtxOptional.isPresent()) {
            return authCtxOptional.get();
        } else {
            AuthConfig authConfig = jobManagerConfig.getAuth();
            if(authConfig.isDisabled()) {
                return new AuthCtx(null, authConfig);
            } else {
                throw new AuthorizationException("Unauthorized request");
            }
        }
    }

    @GET
    @Path("/hello")
    public Response hello() {
        return Response.status(Response.Status.OK)
                .entity("{\"hello\":\"Welcome to JobManager\"}\n")
                .build();
    }

    // Users ===========================================================================================================

    @GET
    @UnitOfWork
    @Timed
    @Path("/users/{userId}/jobs")
    public Response getUserJobs(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("userId") String userId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            List<Job> jobs = jobService.findJobsByUserId(authCtx, userId);
            List<UserJobDto> res = new LinkedList<>();
            for(Job job : jobs) {
                UserJobDto jobDto = UserJobDto.fromJob(job);
                res.add(jobDto);
            }
            return Response.ok(res).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/users/{userId}/jobs/{jobId}")
    public Response getUserJob(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("userId") String userId,
            @PathParam("jobId") String jobId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Job> jobOptional = jobService.findJobByUserIdAndJobId(authCtx, userId, jobId);
            if(jobOptional.isPresent()) {
                UserJobDto jobDto = UserJobDto.fromJob(jobOptional.get());
                return Response.ok(jobDto).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("no such job").build();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    @Timed
    @Path("/users/{userId}/jobs/{jobId}")
    public Response createUserJob(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("userId") String userId,
            @PathParam("jobId") String jobId,
            UserJobDto jobDto) {

        // TODO: 5/29/17 Refactor: most of this should be a service method

        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            if(!authCtx.canSubmitJobForUser(userId, jobDto)) {
                throw new AuthorizationException();
            }
            Job job = new Job();
            if(jobDto == null) {
                return badRequest("No job definition was contained within the request");
            }
            try {
                jobDto.validateSubmittedJob();
                job.setUserId(userId);
                job.setJobId(jobId);
                job.setTag(jobDto.getTag());
                job.setParameters(jobDto.getParametersAsString());
                job.setPriority(jobDto.getPriority());
            } catch (IllegalArgumentException e) {
                return badRequest("invalid id in one of the provided ids"); // todo: Let's not rely on a runtime exception
            } catch (InvalidDtoException e) {
                return badRequest(e.getMessage());
            }

            Map<String, DatasetRef> inputs = job.getInputDatasets();
            for(String key : jobDto.getInputs().keySet()) {
                DatasetRefDto datasetRefDto = jobDto.getInputs().get(key);
                DatasetRef datasetRef = new DatasetRef();
                datasetRefDto.copyToDatasetRef(datasetRef, key);
                inputs.put(key, datasetRef);
            }

            for(String key : jobDto.getProperties().keySet()) {
                job.setProperty(key, jobDto.getProperties().get(key));
            }

            try {
                jobService.createJob(job);
            } catch (AlreadyExistsException e) {
                return Response.status(Response.Status.CONFLICT)
                        .entity("Job ID already exists.")
                        .build();
            }
            return Response.status(Response.Status.CREATED)
                    .entity(UserJobDto.fromJob(job))
                    .build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    // Groups ==========================================================================================================

    // TODO: needs more thinking

    @GET
    @UnitOfWork
    @Timed
    @Path("/text")
    public String text(@Auth Optional<AuthCtx> authCtxOptional) {
        if(!authCtxOptional.isPresent()) {
            return "no auth";
        }

        AuthCtx authCtx = authCtxOptional.get();

        return authCtx.toString() + "\n";
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/groups/{groupId}/jobs") // TODO: not implemented
    public List<UserJobDto> getGroupJobs(@PathParam("groupId") String groupId) {
        List<UserJobDto> jobs = new LinkedList<>();
        jobs.add(new UserJobDto());
        return jobs;
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/groups/{groupId}/jobs/{jobId}") // TODO: not implemented
    public UserJobDto getGroupJob(@PathParam("groupId") String groupId, @PathParam("jobId") String jobId) {
        return new UserJobDto();
    }

    @PUT
    @UnitOfWork
    @Timed
    @Path("/groups/{groupId}/jobs/{jobId}") // TODO: not implemented
    public UserJobDto createGroupJob(@PathParam("groupId") String groupId, @PathParam("jobId") String jobId, UserJobDto jobDto) {
        return new UserJobDto();
    }


    // Jobs ============================================================================================================

    @GET
    @UnitOfWork
    @Timed
    @Path("/jobs")
    public Response getJobs(@Auth Optional<AuthCtx> authCtxOptional) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            List<Job> allJobs = jobService.findAllJobs(authCtx);
            List<String> jobIds = new ArrayList<>();
            for (Job job : allJobs) {
                jobIds.add(job.getJobId());
            }
            Map<String, UserInfo> mappedUserInfo = jobService.getUserInfoForJobs(jobIds, jobManagerConfig.getAuth().getUsersEndpoint());
            List<FullJobDto> jobs = new LinkedList<>();
            for (Job job : allJobs) {
                FullJobDto jobDto = FullJobDto.fromJobUserInfo(job, mappedUserInfo.get(job.getJobId()));
                jobs.add(jobDto);
            }
            return Response.ok(jobs).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/jobs/recentlyActive")
    public Response getRecentlyActiveJobs(@Auth Optional<AuthCtx> authCtxOptional) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            List<Job> allJobs = jobService.findRecentlyActiveJobs(authCtx);
            List<String> jobIds = new ArrayList<>();
            for (Job job : allJobs) {
                jobIds.add(job.getJobId());
            }
            Map<String, UserInfo> mappedUserInfo = jobService.getUserInfoForJobs(jobIds, jobManagerConfig.getAuth().getUsersEndpoint());
            List<FullJobDto> jobs = new LinkedList<>();
            for (Job job : allJobs) {
                FullJobDto jobDto = FullJobDto.fromJobUserInfo(job, mappedUserInfo.get(job.getJobId()));
                jobs.add(jobDto);
            }
            return Response.ok(jobs).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}")
    public Response getJob(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Job> jobOptional = jobService.findJobById(authCtx, jobId);
            if(jobOptional.isPresent()) {
                return Response.ok(FullJobDto.fromJob(jobOptional.get())).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("No such job").build();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/attempts")
    public Response getAttempts(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            List<Attempt> allAttempts = jobService.findAllAttempts(authCtx, jobId);
            List<AttemptDto> attempts = new LinkedList<>();
            for (Attempt a : allAttempts) {
                AttemptDto attemptDto = AttemptDto.fromAttempt(a, true, true);
                attempts.add(attemptDto);
            }
            return Response.ok(attempts).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        } catch (NotFoundException e) {
            return noSuchJobError();
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/attempts/{attemptId}")
    public Response getAttempt(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Attempt> attemptOptional = jobService.findAttemptByIds(authCtx, jobId, attemptId);
            if(attemptOptional.isPresent()) {
                return Response.ok(AttemptDto.fromAttempt(attemptOptional.get(), true, true)).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("attempt not found").build();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/attempts/{attemptId}")
    public Response createAttempt(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam ("jobId") String jobId,
            @PathParam("attemptId") String attemptId,
            AttemptDto attemptDto) {
        // TODO: 5/9/17 add attempt priority !!

        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            attemptDto.validateNewAttempt();
            Attempt attempt = jobService.createAttempt(authCtx, jobId, attemptId, attemptDto.getTag(), Optional.ofNullable(attemptDto.getExecutorId()));
            return Response.status(Response.Status.CREATED)
                    .entity(AttemptDto.fromAttempt(attempt, true, true))
                    .build();
        } catch (NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("No such job")
                    .build();
        } catch (AlreadyExistsException e) {
            return Response.status(Response.Status.CONFLICT)
                    .entity("Attempt already exists")
                    .build();
        } catch (JobManagerException e) {
            return Response.status(Response.Status.CONFLICT)
                    .entity("Job is canceled, OR Unknown error")
                    .build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("One or more arguments are not acceptable. Check the correctness of the input Ids.")
                    .build();
        } catch (InvalidDtoException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/jobs/{jobId}/attempts/{attemptId}/state")
    public Response getAttemptState(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Attempt> attemptOptional = jobService.findAttemptByIds(authCtx, jobId, attemptId);
            if(attemptOptional.isPresent()) {
                return Response.ok(attemptOptional.get().getState().name()).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("attempt not found").build();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    @Timed
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/jobs/{jobId}/attempts/{attemptId}/state")
    public Response setAttemptState(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId,
            String state) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            String newState = jobService.updateAttemptState(authCtx, jobId, attemptId, state);
            return Response.status(Response.Status.CREATED).entity(newState).build();
        } catch (NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity("attempt not found").build();
        } catch (IllegalStateTransitionException e) {
            logger.debug("State transition failed: " + e.getMessage());
            return illegalStateTransition(e);
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/cancel")
    public Response cancelJob(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.cancelJob(authCtx, jobId);
        } catch (NotFoundException e) {
            return noSuchJobError();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
        return Response.ok().build();
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/restart")
    public Response restartJob(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("jobId") String jobId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.restartJob(authCtx, jobId);
        } catch (NotFoundException e) {
            return noSuchJobError();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @UnitOfWork
    @Timed
    @Path("/jobs/{jobId}/hold")
    public Response holdJob(
        @Auth Optional<AuthCtx> authCtxOptional,
        @PathParam("jobId") String jobId,
        String doHold) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.holdJob(authCtx, jobId, doHold);
            return Response.status(Response.Status.OK).build();
        } catch (NotFoundException e) {
            return noSuchJobError();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        } catch (BadRequestException e) {
            return badRequest(e.getMessage());
        }
    }

    // Executors =======================================================================================================
    @GET
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/queue")
    public Response getExecutorQueue(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @QueryParam("tags") @NotNull String tags,
            @QueryParam("limit") @DefaultValue("-1") int limit) {

        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Set<String> tagSet = new HashSet<>();
            for(String tag : tags.split(",")) {
                if(!tag.equals("")) {
                    tagSet.add(tag);
                }
            }
            List<ExecutorJobDto> jobDtos = new LinkedList<>();
            for(Attempt attempt : jobService.getExecutorQueue(authCtx, executorId, tagSet, limit)) {
                ExecutorJobDto jobDto = ExecutorJobDto.fromAttempt(attempt);
                jobDto.setUrisByPrefix(jobManagerConfig.getPublicUri());
                jobDtos.add(jobDto);
            }
            return Response.ok(jobDtos).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/executors/queue")
    public Response getAllExecutorsQueue(@Auth Optional<AuthCtx> authCtxOptional) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            List<ExecutorJobDto> jobDtos = new LinkedList<>();
            for(Attempt attempt : jobService.getAllLocallyQueuedAttempts(authCtx)) {
                jobDtos.add(ExecutorJobDto.fromAttempt(attempt));
            }
            return Response.ok(jobDtos).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/queue")
    public Response popExecutorQueue(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @QueryParam("limit") @DefaultValue("1") int limit,
            @QueryParam("tags") String tags) {
        try {
            AuthCtx authCtx = getAuthCtx(authCtxOptional);
            List<ExecutorJobDto> jobDtos = new LinkedList<>();
            Set<String> tagSet = new HashSet<>();
            for(String tag : tags.split(",")) {
                if(!tag.equals("")) {
                    tagSet.add(tag);
                }
            }
            for(Attempt attempt : jobService.popExecutorQueue(authCtx, executorId, tagSet, limit)) {
                ExecutorJobDto jobDto = ExecutorJobDto.fromAttempt(attempt);
                jobDto.setUrisByPrefix(jobManagerConfig.getPublicUri());
                jobDtos.add(jobDto);
            }
            return Response.ok(jobDtos).build();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}")
    public Response getExecutorAttempt(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Attempt> attemptOptional = jobService.findExecutorAttemptByIds(authCtx, executorId, jobId, attemptId);
            if(attemptOptional.isPresent()) {
                AttemptDto attemptDto = AttemptDto.fromAttempt(attemptOptional.get(), true, true);
                return Response.ok(attemptDto).build();
            } else {
                return noSuchAttemptError();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/full")
    public Response getExecutorAttemptFull(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Attempt> attemptOptional = jobService.findExecutorAttemptByIds(authCtx, executorId, jobId, attemptId);
            if(attemptOptional.isPresent()) {
                ExecutorJobDto jobDto = ExecutorJobDto.fromAttempt(attemptOptional.get());
                jobDto.setUrisByPrefix(jobManagerConfig.getPublicUri());
                return Response.ok(jobDto).build();
            } else {
                return noSuchAttemptError();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @GET
    @UnitOfWork
    @Timed
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/state")
    public Response getExecutorAttemptState(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            Optional<Attempt> attemptOptional = jobService.findExecutorAttemptByIds(authCtx, executorId, jobId, attemptId);
            if (attemptOptional.isPresent()) {
                return Response.ok(attemptOptional.get().getState().name()).build();
            } else {
                return noSuchAttemptError();
            }
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    @Timed
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/state")
    public Response setExecutorAttemptState(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId,
            String state) {
        String resultingState;
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            resultingState = jobService.updateAttemptStateForExecutor(authCtx, executorId, jobId, attemptId, state);
            return Response.status(Response.Status.CREATED).entity(resultingState).build();
        } catch (NotFoundException e) {
            return noSuchAttemptError();
        } catch (IllegalStateTransitionException e) {
            logger.debug("State transition failed: " + e.getMessage());
            return illegalStateTransition(e);
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @POST
    @UnitOfWork
    @Timed
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/heartbeat")
    public Response updateAttemptHeartbeat(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.updateHeartbeat(authCtx, executorId, jobId, attemptId);
            return Response.status(Response.Status.CREATED).entity("").build();
        } catch (NotFoundException e) {
            return noSuchAttemptError();
        } catch (StaleUpdateException e) {
            return forbidden(e.getMessage());
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/outputs/{datasetKey}")
    public Response setOutputDataset(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId,
            @PathParam("datasetKey") String datasetKey,
            DatasetRefDto datasetRefDto) {

        DatasetRef dataset = new DatasetRef();
        datasetRefDto.copyToDatasetRef(dataset, datasetKey);
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.setOutputDataset(authCtx, executorId, jobId, attemptId, dataset);
        } catch (NotFoundException e) {
            return noSuchAttemptError();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }

        return resourceCreated(DatasetRefDto.fromReadWriteAccessibleDataset(dataset));
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/executors/{executorId}/jobs/{jobId}/attempts/{attemptId}/properties")
    public Response updateAttemptProperties(
            @Auth Optional<AuthCtx> authCtxOptional,
            @PathParam("executorId") String executorId,
            @PathParam("jobId") String jobId,
            @PathParam("attemptId") String attemptId,
            Map<String,String> properties) {
        try {
            AuthCtx authCtx = this.getAuthCtx(authCtxOptional);
            jobService.updateAttemptProperties(authCtx, attemptId, properties);
        } catch (NotFoundException e) {
            return noSuchAttemptError();
        } catch (AuthorizationException e) {
            return forbidden(e.getMessage());
        }

        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
