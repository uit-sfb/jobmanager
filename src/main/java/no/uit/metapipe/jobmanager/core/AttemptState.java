package no.uit.metapipe.jobmanager.core;

/*
 /$$$$$$$   /$$$$$$
| $$__  $$ /$$__  $$
| $$  \ $$| $$  \ $$
| $$  | $$| $$  | $$
| $$  | $$| $$  | $$
| $$  | $$| $$  | $$
| $$$$$$$/|  $$$$$$/
|_______/  \______/



 /$$   /$$  /$$$$$$  /$$$$$$$$
| $$$ | $$ /$$__  $$|__  $$__/
| $$$$| $$| $$  \ $$   | $$
| $$ $$ $$| $$  | $$   | $$
| $$  $$$$| $$  | $$   | $$
| $$\  $$$| $$  | $$   | $$
| $$ \  $$|  $$$$$$/   | $$
|__/  \__/ \______/    |__/



  /$$$$$$  /$$   /$$  /$$$$$$  /$$   /$$  /$$$$$$  /$$$$$$$$
 /$$__  $$| $$  | $$ /$$__  $$| $$$ | $$ /$$__  $$| $$_____/
| $$  \__/| $$  | $$| $$  \ $$| $$$$| $$| $$  \__/| $$
| $$      | $$$$$$$$| $$$$$$$$| $$ $$ $$| $$ /$$$$| $$$$$
| $$      | $$__  $$| $$__  $$| $$  $$$$| $$|_  $$| $$__/
| $$    $$| $$  | $$| $$  | $$| $$\  $$$| $$  \ $$| $$
|  $$$$$$/| $$  | $$| $$  | $$| $$ \  $$|  $$$$$$/| $$$$$$$$
 \______/ |__/  |__/|__/  |__/|__/  \__/ \______/ |________/


 If another state transition is needed then please discuss it with the team first.

 The AttemptState enum is part of the JobManager's public API so changes can have wide implications as
 clients might make assumptions about it's behavior.
 */


import no.uit.metapipe.jobmanager.exceptions.IllegalStateTransitionException;

public enum AttemptState {
    /**
     * The attempt is in the JobManager queue and is ready to be assigned to an executor.
     * This is the initial state of an attempt.
     */
    QUEUED_LOCALLY {
        @Override
        public boolean canTransitionTo(AttemptState newState) {
            switch (newState) {
                case ASSIGNED_EXECUTOR:
                case CANCELLED:
                    return true;
                default:
                    return false;
            }
        }
    },

    /**
     * The attempt has been assigned to a specific executor.
     * This means that either of these conditions hold true:
     *  1. It has been decided which executor should run the attempt. In this case
     *     the attempt should be in that executor's queue
     *  2. An executor has just read this attempt from it's queue.
     */
    ASSIGNED_EXECUTOR {
        @Override
        public boolean canTransitionTo(AttemptState newState) {
            switch (newState) {
                case QUEUED_EXECUTOR:
                case RUNNING:
                case FINISHED:
                    return true;
                default:
                    return newState.isFailure();
            }
        }

        @Override
        public boolean belongsToExecutor() {
            return true;
        }
    },

    /**
     * The attempt has ben read from the executor queue, but is queued either by the executor
     * or by a job scheduler system that is being used by the executor.
     */
    QUEUED_EXECUTOR {
        @Override
        public boolean canTransitionTo(AttemptState newState) {
            switch (newState) {
                case RUNNING:
                case FINISHED:
                    return true;
                default:
                    return newState.isFailure();
            }
        }

        @Override
        public boolean belongsToExecutor() {
            return true;
        }
    },

    /**
     * The executor has reported that it is processing the attempt
     */
    RUNNING {
        @Override
        public boolean canTransitionTo(AttemptState newState) {
            return newState == FINISHED || newState.isFailure();
        }

        @Override
        public boolean belongsToExecutor() {
            return true;
        }
    },

    /**
     * The executor has reported that the attempt finished successfully
     */
    FINISHED,


    // Admin intervention

    /**
     * An admin (or a part of the system) has cancelled this attempt.
     */
    CANCELLED,

    // Job failure

    /**
     * The attempt has not received a heart-beat from an executor within the
     * set time-out period. The attempt is assumed to have failed.
     *
     * It is assumed that the error is transient and that the job can be retried (and succeed) at
     * a later time without admin intervention.
     */
    TIMED_OUT,

    /**
     * The executor has reported a failure during the workflow execution.
     *
     * Typically caused by an exception being thrown by a tool parser (a bug) or a tool crashing during
     * execution.
     *
     * It is assumed that the failure is systematic and that there is no point in retrying automatically.
     *
     * This state requires an admin (or a developer) to investigate the cause of the error, possibly fix a bug or a
     * deployment issue, and then create a new attempt when the cause has been resolved.
     */
    WORKFLOW_FAILED,

    /**
     * The executor has reported a failure that is not related to the workflow execution itself.
     *
     * This could be caused by a dependency becoming unavailable such as a job scheduler or
     * a compute node going offline during the execution of a job or similar.
     *
     * It is assumed that the error is transient and that the job can be retried (and succeed) at
     * a later time without admin intervention.
     */
    EXECUTOR_FAILED;

    public AttemptState transitionTo(AttemptState newState) throws IllegalStateTransitionException {
        if(this.canTransitionTo(newState)) {
            return newState;
        }
        throw new IllegalStateTransitionException(this.name(), newState.name());
    }

    boolean isFailure() {
        switch (this) {
            case TIMED_OUT:
            case WORKFLOW_FAILED:
            case EXECUTOR_FAILED:
            case CANCELLED:
                return true;
            default:
                return false;
        }
    }

    public boolean isFinal() {
        return this == FINISHED || this.isFailure();
    }

    public boolean canTransitionTo(AttemptState newState) {
        return false;
    }

    public boolean belongsToExecutor() {
        return false;
    }

    public AttemptState getQueuedLocally() throws IllegalStateTransitionException {
        return transitionTo(QUEUED_LOCALLY);
    }

    public AttemptState getAssignedExecutor() throws IllegalStateTransitionException {
        return transitionTo(ASSIGNED_EXECUTOR);
    }

    public AttemptState getQueuedExecutor() throws IllegalStateTransitionException {
        return transitionTo(QUEUED_EXECUTOR);
    }

    public AttemptState getRunning() throws IllegalStateTransitionException {
        return transitionTo(RUNNING);
    }

    public AttemptState getFinished() throws IllegalStateTransitionException {
        return transitionTo(FINISHED);
    }

    public AttemptState getCancelled() throws IllegalStateTransitionException {
        return transitionTo(CANCELLED);
    }

    public AttemptState getTimedOut() throws IllegalStateTransitionException {
        return transitionTo(TIMED_OUT);
    }

    public AttemptState getWorkflowFailed() throws IllegalStateTransitionException {
        return transitionTo(WORKFLOW_FAILED);
    }

    public AttemptState getExecutorFailed() throws IllegalStateTransitionException {
        return transitionTo(EXECUTOR_FAILED);
    }
}
