package no.uit.metapipe.jobmanager.core;

import no.uit.metapipe.jobmanager.exceptions.IllegalStateTransitionException;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static no.uit.metapipe.jobmanager.util.Util.validateId;

@Entity
@org.hibernate.annotations.NamedQueries({
        @NamedQuery(name="findAttemptByAttemptId", query = "SELECT a FROM Attempt a where a.attemptId = :attemptId"),
        @NamedQuery(name="findAttemptByJobIdAndAttemptId", query = "SELECT a FROM Attempt a JOIN a.job as j where a.attemptId = :attemptId AND j.jobId = :jobId"),
        @NamedQuery(name="findAttemptsByJobId", query = "SELECT a FROM Attempt a JOIN a.job as j where j.jobId = :jobId"),
        @NamedQuery(name="findByExecutorIdAndState", query ="SELECT a FROM Attempt a WHERE a.state = :state AND executorId = :executorId ORDER BY a.attemptId DESC"),
        @NamedQuery(name="findByState", query ="SELECT a FROM Attempt a WHERE a.state = :state ORDER BY a.attemptId DESC"),
        @NamedQuery(name="findByStateAndTags", query ="SELECT a FROM Attempt a WHERE a.state = :state AND a.tag in (:tags) ORDER BY a.priority, a.job.creationTimestamp"),
        @NamedQuery(name="findByFinalized", query="SELECT a FROM Attempt a WHERE a.finalized = :finalized")
})
public class Attempt extends BaseEntity {

    @ManyToOne
    private Job job;

    private String executorId;

    @Enumerated(EnumType.STRING)
    private AttemptState state = AttemptState.QUEUED_LOCALLY;

    private String attemptId;

    private String tag;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastHeartbeat;

    @OneToMany
    @MapKey(name="key")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Map<String, DatasetRef> outputDatasets = new HashMap<>();

    private long priority;

    @ElementCollection
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    @CollectionTable(name="Attempt_Properties", joinColumns=@JoinColumn(name="Attempt_id"))
    @Cascade(CascadeType.ALL)
    private Map<String, String> properties = new HashMap<>();

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStarted;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeEnded;

    private boolean finalized = false;

    public static Attempt fromInitialState(AttemptState state) {
        Attempt attempt = new Attempt();
        attempt.state = state;
        return attempt;
    }

    private void updateTimestamps(AttemptState newState) {
        if( this.getTimeStarted() == null && newState == AttemptState.RUNNING) {
            this.setTimeStarted(new Date());
        }

        if(this.getTimeEnded() == null && (newState == AttemptState.FINISHED || newState.isFailure())) {
            this.setTimeEnded(new Date());
        }
    }

    public Duration getRuntime() {
        if(this.getTimeEnded() != null && getTimeStarted() != null) {
            return Duration.between(getTimeStarted().toInstant(), getTimeEnded().toInstant());
        }

        if(getTimeStarted() != null && getTimeEnded() == null) {
            Duration.between(getTimeStarted().toInstant(), Instant.now());
        }

        return Duration.ZERO;
    }

    public Duration getQueueDuration() {
        Instant latestKnownQueued;
        if(getTimeStarted() != null) {
            latestKnownQueued = getTimeStarted().toInstant();
        } else if(getTimeEnded() != null) {
            latestKnownQueued = getTimeEnded().toInstant();
        } else if(getState().isFailure() || getState() == AttemptState.FINISHED) {
            // Attempt was created before we started to track job start- and end timestamps
            return Duration.ZERO;
        } else {
            latestKnownQueued = Instant.now();
        }

        return Duration.between(getCreationTimestamp().toInstant(), latestKnownQueued);
    }

    public void updateHeartbeat() {
        if(!belongsToExecutor()) {
            throw new IllegalStateException("Attempted to update heartbeat on an attempt that no longer belongs to an executor.");
        }
        lastHeartbeat = new Date();
    }

    public boolean canUpdateHeartbeat() {
        return belongsToExecutor();
    }

    public void assignExecutor(String executorId) throws IllegalStateTransitionException {
        setExecutorId(executorId);
        setState(AttemptState.ASSIGNED_EXECUTOR);
        updateHeartbeat();
    }

    public boolean belongsToExecutor() {
        return getState().belongsToExecutor();
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getExecutorId() {
        return executorId;
    }

    public void setExecutorId (String executorId) throws IllegalArgumentException {
        this.executorId = validateId(executorId, true);
    }

    public AttemptState getState() {
        return state;
    }

    public void setState(AttemptState newState) throws IllegalStateTransitionException {
        this.state = this.state.transitionTo(newState);
        this.updateTimestamps(newState);
        this.getJob().recalculateState();
    }

    public String getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(String attemptId) throws IllegalArgumentException {
        this.attemptId = validateId(attemptId, false);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getLastHeartbeat() {
        return lastHeartbeat;
    }

    public void setLastHeartbeat(Date lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public Map<String, DatasetRef> getOutputDatasets() {
        return outputDatasets;
    }

    public void setOutputDatasets(Map<String, DatasetRef> outputDatasets) {
        this.outputDatasets = outputDatasets;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public Date getTimeStarted() {
        return timeStarted;
    }

    public void setTimeStarted(Date timeStarted) {
        this.timeStarted = timeStarted;
    }

    public Date getTimeEnded() {
        return timeEnded;
    }

    public void setTimeEnded(Date timeEnded) {
        this.timeEnded = timeEnded;
    }

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }
}
