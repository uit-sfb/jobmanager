package no.uit.metapipe.jobmanager.core;

import no.uit.metapipe.jobmanager.exceptions.IllegalStateTransitionException;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.Duration;
import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;
import static no.uit.metapipe.jobmanager.util.Util.validateId;

@Entity
@org.hibernate.annotations.NamedQueries({
        @NamedQuery(name = "findAllJobs", query = "SELECT j FROM Job j"),
        @NamedQuery(name = "jobByJobId", query = "SELECT j FROM Job j WHERE j.jobId=:jobId"),
        @NamedQuery(name = "jobByUserId", query = "SELECT j FROM Job j WHERE j.userId=:userId"),
        @NamedQuery(name = "jobsWithNumAttempts", query = "SELECT j FROM Job j WHERE size(j.attempts) = :numAttempts"),
        @NamedQuery(name = "findJobsByState", query = "SELECT j FROM Job j WHERE j.state=:jobState"),
        @NamedQuery(name = "findJobsByStateNull", query = "SELECT j FROM Job j WHERE j.state IS NULL")
})
public class Job extends BaseEntity {

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id")
    @Cascade(CascadeType.ALL)
    @OrderColumn
    private List<Attempt> attempts = new LinkedList<>();

    @ElementCollection
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    @CollectionTable(name = "Job_Properties", joinColumns = @JoinColumn(name = "Job_id"))
    @Cascade(CascadeType.ALL)
    private Map<String, String> properties = new HashMap<>();

    private String userId;
    private String jobId;
    private String groupId;

    @Column(name = "parameters", columnDefinition = "TEXT")
    private String parameters;

    @Enumerated(EnumType.STRING)
    private JobState state = JobState.SUBMITTED;

    private String tag;

    private boolean cancelled;

    private boolean hold = false;

    private long priority;

    @OneToMany
    @MapKey(name = "key")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Map<String, DatasetRef> inputDatasets = new HashMap<>();

    public Duration getTotalRuntime() {
        Duration total = Duration.ZERO;

        for(Attempt attempt : getAttempts()) {
            total = total.plus(attempt.getRuntime());
        }

        return total;
    }

    public Duration getTotalQueueDuration() {
        Duration total = Duration.ZERO;

        for (Attempt attempt : getAttempts()) {
            total = total.plus(attempt.getQueueDuration());
        }

        return total;
    }

    //@Formula("from Job j where j.attempts[size(j.attempts) - 1]")
    //Attempt latestAttempt;

    public Optional<Attempt> getLatestAttempt() {
        if (attempts.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(attempts.get(attempts.size() - 1));
        }
    }

    public void recalculateState() {
        if (this.getHold()) {
            return;
        }
        if (isCancelled()) {
            this.state = JobState.CANCELLED;
            return;
        }

        Optional<Attempt> latestAttemptOptional = getLatestAttempt();

        if (!latestAttemptOptional.isPresent()) {
            // TODO: error?
            this.state = JobState.SUBMITTED;
        }

        if (latestAttemptOptional.isPresent()) {
            Attempt latestAttempt = latestAttemptOptional.get();

            AttemptState attemptState = latestAttempt.getState();
            switch (attemptState) {
                case QUEUED_LOCALLY:
                case ASSIGNED_EXECUTOR:
                case QUEUED_EXECUTOR:
                case RUNNING:
                    this.state = JobState.SUBMITTED;
                    break;
                case FINISHED:
                    this.state = JobState.FINISHED;
                    break;
                default:
                    this.state = JobState.DELAYED;
                    break;
            }
        }
    }

    public JobState getState() {
        if (this.state == null) {
            this.recalculateState();
        }
        return this.state;
    }


    public Map<String, DatasetRef> getJobOutputs() {
        Optional<Attempt> attemptOptional = getLatestAttempt();
        if (attemptOptional.isPresent()) {
            return attemptOptional.get().getOutputDatasets();
        } else {
            return new HashMap<>();
        }
    }

    public Attempt createAttempt(String attemptId) {
        if (isCancelled()) {
            throw new IllegalStateException("Cannot create a new attempt on a job that is cancelled.");
        }
        Attempt attempt = new Attempt();
        attempt.setJob(this);
        attempt.setAttemptId(attemptId);
        attempt.setTag(getTag());
        attempt.setPriority(getPriority());
        attempts.add(attempt);
        return attempt;
    }

    public void cancelAllAttempts() {
        for (Attempt attempt : attempts) {
            try {
                AttemptState cancelled = AttemptState.CANCELLED;
                if (attempt.getState().canTransitionTo(cancelled)) {
                    attempt.setState(cancelled);
                }
            } catch (IllegalStateTransitionException e) {
                throw new IllegalStateException("This should never happen.");
            }
        }
    }

    public boolean hasProperty(String name) {
        return properties.containsKey(name);
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    public void cancel() {
        cancelAllAttempts();
        setCancelled(true);
    }

    public Attempt restart() {
        cancelAllAttempts();
        String newAttemptID = /*"job_" + jobId + "_attempt_" + */ "attempt_" + randomString(10);
        return createAttempt(newAttemptID);
    }

    public List<Attempt> getAttempts() {
        return attempts;
    }

    public void setAttempts(List<Attempt> attempts) {
        this.attempts = attempts;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) throws IllegalArgumentException {
        this.userId = validateId(userId, false);
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) throws IllegalArgumentException {
        this.jobId = validateId(jobId, false);
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) throws IllegalArgumentException {
        this.groupId = validateId(groupId, true);
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Map<String, DatasetRef> getInputDatasets() {
        return inputDatasets;
    }

    public void setInputDatasets(Map<String, DatasetRef> inputDatasets) {
        this.inputDatasets = inputDatasets;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public void setHold(boolean hold) {
        this.hold = hold;
    }

    public boolean getHold() {
        return this.hold;
    }
}
