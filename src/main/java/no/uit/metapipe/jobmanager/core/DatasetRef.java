package no.uit.metapipe.jobmanager.core;

import javax.persistence.*;

@Entity
public class DatasetRef extends BaseEntity {

    String key;

    String url;

    String readBearerToken;
    String writeBearerToken;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReadBearerToken() {
        return readBearerToken;
    }

    public void setReadBearerToken(String readBearerToken) {
        this.readBearerToken = readBearerToken;
    }

    public String getWriteBearerToken() {
        return writeBearerToken;
    }

    public void setWriteBearerToken(String writeBearerToken) {
        this.writeBearerToken = writeBearerToken;
    }
}
