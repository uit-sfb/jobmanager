package no.uit.metapipe.jobmanager.core;

import no.uit.metapipe.jobmanager.config.JobTagConfig;

public class TagInfo {

    public static TagInfo fromJobTagConfig(JobTagConfig c) {
        TagInfo ti = new TagInfo();
        ti.setName(c.getTag());
        ti.setDefaultPriority(c.getPriority());
        return ti;
    }

    String name;
    long defaultPriority;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDefaultPriority() {
        return defaultPriority;
    }

    public void setDefaultPriority(long defaultPriority) {
        this.defaultPriority = defaultPriority;
    }
}
