package no.uit.metapipe.jobmanager.core;

public enum JobState {
    /**
     * The user has submitted a job.
     *
     * The job is either enqueued or is processing.
     */
    SUBMITTED,

    /**
     * The job has succeeded.
     */
    FINISHED,

    /**
     * One or more attempts have failed.
     *
     * The job will either be retried until it succeeds or an admin will cancel it.
     */
    DELAYED,

    /**
     * An admin has cancelled the job.
     */
    CANCELLED;
}
