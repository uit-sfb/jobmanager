package no.uit.metapipe.jobmanager.service;

import no.uit.metapipe.jobmanager.api.UserInfo;
import no.uit.metapipe.jobmanager.core.*;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.DatasetRefRepo;
import no.uit.metapipe.jobmanager.db.JobRepo;
import no.uit.metapipe.jobmanager.db.TagInfoRepo;
import no.uit.metapipe.jobmanager.exceptions.*;
import no.uit.metapipe.jobmanager.exceptions.NotFoundException;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.*;

import static no.uit.metapipe.jobmanager.util.Util.randomString;

public class JobManagerService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    public static final long DEFAULT_JOB_PRIORITY = 1000;
    private final JobRepo jobRepo;
    private final AttemptRepo attemptRepo;
    private final DatasetRefRepo datasetRefRepo;
    private final TagInfoRepo tagInfoRepo;

    public JobManagerService(JobRepo jobRepo, AttemptRepo attemptRepo, DatasetRefRepo datasetRefRepo, TagInfoRepo tagInfoRepo) {
        this.jobRepo = jobRepo;
        this.attemptRepo = attemptRepo;
        this.datasetRefRepo = datasetRefRepo;
        this.tagInfoRepo = tagInfoRepo;
    }

    public List<Job> findAllJobs(AuthCtx authCtx) throws AuthorizationException {
        if(!authCtx.canReadAllJobs()) {
            throw new AuthorizationException();
        }
        return jobRepo.findAll();
    }

    public List<Job> findRecentlyActiveJobs(AuthCtx authCtx) throws AuthorizationException {
        if(!authCtx.canReadRecentlyActiveJobs()) {
            throw new AuthorizationException();
        }
        return jobRepo.findRecentlyActiveJobs();
    }

    public List<Attempt> findAllAttempts(AuthCtx authCtx, String jobId) throws NotFoundException, AuthorizationException {
        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            throw new NotFoundException();
        }

        Job job = jobOptional.get();

        if(!authCtx.canFindAttemptsByJob(job)) {
            throw new AuthorizationException();
        }

        return job.getAttempts();
    }

    public Job createJob(Job job) throws AlreadyExistsException {
        Optional<Job> jobOptional = jobRepo.findByJobId(job.getJobId());
        if (jobOptional.isPresent()) {
            throw new AlreadyExistsException();
        }

        long jobPriority;

        Optional<TagInfo> tagInfoOptional = tagInfoRepo.findByTagName(job.getTag());
        if (tagInfoOptional.isPresent()) {
            jobPriority = tagInfoOptional.get().getDefaultPriority();
        } else {
            jobPriority = DEFAULT_JOB_PRIORITY;
        }

        job.setPriority(jobPriority);

        return jobRepo.saveOrUpdate(job);
    }

    public Attempt createAttempt(AuthCtx authCtx, String jobId, String attemptId, String tag, Optional<String> executorIdOptional) throws JobManagerException, AuthorizationException {
        // TODO: 5/9/17 Refactor: use job.createAttempt() to implement this method

        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            throw new NotFoundException();
        }

        Job job = jobOptional.get();
        if(!authCtx.canCreateAttemptForJob(job)) {
            throw new AuthorizationException();
        }

        Optional<Attempt> attemptOptional = attemptRepo.findByJobIdAndAttemptId(jobId, attemptId);
        if(attemptOptional.isPresent()) {
            throw new AlreadyExistsException();
        }

        if(job.isCancelled()) {
            throw new JobManagerException();
        }

        Attempt attempt = new Attempt();
        attempt.setTag(tag);
        attempt.setPriority(job.getPriority());

        attempt.setAttemptId(attemptId);
        executorIdOptional.ifPresent(attempt::setExecutorId);

        attempt.setJob(job);
        job.getAttempts().add(attempt);

        attemptRepo.saveOrUpdate(attempt);
        jobRepo.saveOrUpdate(job);

        return attempt;
    }

    public Optional<Attempt> findExecutorAttemptByIds(AuthCtx authCtx, String executorId, String jobId, String attemptId) throws AuthorizationException {
        if(!authCtx.canFindExecutorAttemptByIds(executorId, jobId, attemptId)) {
            throw new AuthorizationException();
        }

        Optional<Attempt> attemptOptional = attemptRepo.findByJobIdAndAttemptId(jobId, attemptId);
        if(attemptOptional.isPresent()) {
            Attempt attempt = attemptOptional.get();
            if(attempt.getExecutorId() != null && attempt.getExecutorId().equals(executorId)) {
                return Optional.of(attempt);
            }
        }
        return Optional.empty();
    }

    public Optional<Attempt> findAttemptByIds(AuthCtx authCtx, String jobId, String attemptId) throws AuthorizationException {
        Optional<Attempt> attemptOptional = attemptRepo.findByJobIdAndAttemptId(jobId, attemptId);

        if(!attemptOptional.isPresent()) {
            return Optional.empty();
        }

        Attempt attempt = attemptOptional.get();

        if(!authCtx.canReadAttempt(attempt)) {
            throw new AuthorizationException();
        }

        if(!authCtx.canReadAttempt(attempt)) {
            throw new AuthorizationException();
        }

        return attemptOptional;
    }

    public String updateAttemptState(AuthCtx authCtx, String jobId, String attemptId, String state) throws NotFoundException, IllegalStateTransitionException, AuthorizationException {
        Optional<Attempt> attemptOptional = attemptRepo.findByJobIdAndAttemptId(jobId, attemptId);
        if(!attemptOptional.isPresent()) {
            throw new NotFoundException();
        }

        AttemptState newState = AttemptState.valueOf(state);
        Attempt attempt = attemptOptional.get();

        if(!authCtx.canUpdateAttemptState(attempt)) {
            throw new AuthorizationException();
        }

        attempt.setState(newState);
        attemptRepo.saveOrUpdate(attempt);
        return newState.name();
    }

    public List<Attempt> getExecutorQueue(AuthCtx authCtx, String executorId, Set<String> tags) throws AuthorizationException {
        return getExecutorQueue(authCtx, executorId, tags, -1);
    }

    public List<Attempt> getExecutorQueue(AuthCtx authCtx, String executorId, Set<String> tags, int numElements) throws AuthorizationException {
        if(authCtx.canGetExecutorQueue(executorId)) {
            return attemptRepo.findByStateAndTags(AttemptState.QUEUED_LOCALLY, tags, numElements);
        } else {
            throw new AuthorizationException("Not authorized");
        }
    }

    public List<Attempt> getAllLocallyQueuedAttempts(AuthCtx authCtx) throws AuthorizationException {
        if (authCtx.canGetAllLocallyQueuedAttempts()) {
            return attemptRepo.findByState(AttemptState.QUEUED_LOCALLY, -1);
        } else {
            throw new AuthorizationException();
        }
    }

    public List<Attempt> popExecutorQueue(AuthCtx authCtx, String executorId, Set<String> tags, int numElements) throws AuthorizationException {

        if(!authCtx.canPopExecutorQueue(executorId)) {
            throw new AuthorizationException("Missing required scope");
        }

        List<Attempt> attempts = getExecutorQueue(authCtx, executorId, tags, numElements);
        for(Attempt attempt : attempts) {
            try {
                attempt.assignExecutor(executorId);
            } catch (IllegalStateTransitionException illegalStateTransitionException) {
                // We have already selected attempts that are QUEUED_LOCALLY so this should not happen.
                throw new IllegalStateException("This state transition is assumed to be valid.");
            }
            attemptRepo.saveOrUpdate(attempt);
        }
        return attempts;
    }

    public Optional<Job> findJobById(AuthCtx authCtx, String jobId) throws AuthorizationException {
        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if (!jobOptional.isPresent()) {
            return Optional.empty();
        }

        Job job = jobOptional.get();

        if(!authCtx.canReadJob(job)) {
            throw new AuthorizationException();
        }

        return jobOptional;
    }

    public List<Job> findJobsByUserId(AuthCtx authCtx, String userId) throws AuthorizationException {
        if(!authCtx.canReadUserJobs(userId)) {
            throw new AuthorizationException();
        }
        return jobRepo.findByUserId(userId);
    }

    public Optional<Job> findJobByUserIdAndJobId(AuthCtx authCtx, String userId, String jobId) throws AuthorizationException {
        if(!authCtx.canReadUserJobs(userId)) {
            throw new AuthorizationException();
        }

        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            return Optional.empty();
        }

        Job job = jobOptional.get();
        if(job.getUserId().equals(userId)) {
            return Optional.of(job);
        } else {
            return Optional.empty();
        }
    }

    public String updateAttemptStateForExecutor(AuthCtx authCtx, String executorId, String jobId, String attemptId, String state) throws NotFoundException, IllegalStateTransitionException, AuthorizationException {
        Optional<Attempt> attemptOptional = findExecutorAttemptByIds(authCtx, executorId, jobId, attemptId); // todo: refactor: mindless code reuse. Use attemptRepo instead.
        if(!attemptOptional.isPresent()) {
            throw new NotFoundException();
        }

        Attempt attempt = attemptOptional.get();

        if(!authCtx.canUpdateAttemptState(attempt)) {
            throw new AuthorizationException();
        }

        AttemptState newState = AttemptState.valueOf(state.toUpperCase());

        attempt.setState(newState);
        attemptRepo.saveOrUpdate(attempt);
        return newState.name();
    }

    public void cancelJob(AuthCtx authCtx, String jobId) throws NotFoundException, AuthorizationException {
        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            throw new NotFoundException();
        }

        Job job = jobOptional.get();

        if(!authCtx.canCancelJob(job)) {
            throw new AuthorizationException();
        }

        job.cancel();
        jobRepo.saveOrUpdate(job);
    }

    public Attempt restartJob(AuthCtx authCtx, String jobId) throws NotFoundException, AuthorizationException {
        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            throw new NotFoundException();
        }
        Job job = jobOptional.get();

        if(!authCtx.canRestartJob(job)) {
            throw new AuthorizationException();
        }

        Attempt newAttempt = job.restart();
        jobRepo.saveOrUpdate(job);
        return newAttempt;
    }

    public void setOutputDataset(AuthCtx authCtx, String executorId, String jobId, String attemptId, DatasetRef dataset) throws NotFoundException, AuthorizationException {
        Optional<Attempt> attemptOptional = findExecutorAttemptByIds(authCtx, executorId, jobId, attemptId);
        if(!attemptOptional.isPresent()) {
            throw new NotFoundException();
        }

        Attempt attempt = attemptOptional.get();

        if(!authCtx.canSetOutputDataset(attempt)) {
            throw new AuthorizationException();
        }


        attempt.getOutputDatasets().put(dataset.getKey(), dataset);

        datasetRefRepo.saveOrUpdate(dataset);
        attemptRepo.saveOrUpdate(attempt);
    }

    public void updateHeartbeat(AuthCtx authCtx, String executorId, String jobId, String attemptId) throws NotFoundException, StaleUpdateException, AuthorizationException {
        // TODO: 5/29/17 Refactor: Simplify method signature: remove executorId and jobId (now handled by authContext)

        Optional<Attempt> attemptOptional = attemptRepo.findByJobIdAndAttemptId(jobId, attemptId);
        if(!attemptOptional.isPresent()) {
            throw new NotFoundException();
        }
        Attempt attempt = attemptOptional.get();
        if(!attempt.getExecutorId().equals(executorId)) {
            throw new NotFoundException();
        }

        if(!authCtx.canUpdateHeartbeat(attempt)) {
            throw new AuthorizationException();
        }

        if(!attempt.canUpdateHeartbeat()) {
            throw new StaleUpdateException("Heartbeat can no longer be updated.");
        }
        attempt.updateHeartbeat();
        attemptRepo.saveOrUpdate(attempt);
    }

    public void updateAttemptProperties(AuthCtx authCtx, String attemptId, Map<String, String> properties) throws NotFoundException, AuthorizationException {
        Attempt attempt = attemptRepo.findByAttemptId(attemptId).orElseThrow(() -> new NotFoundException());

        if(!authCtx.canUpdateAttemptProperties(attempt)) {
            throw new AuthorizationException();
        }

        Map<String, String> attemptProperties = attempt.getProperties();
        for(String key : properties.keySet()) {
            attemptProperties.put(key, properties.get(key));
        }

        attemptRepo.saveOrUpdate(attempt);
    }

    public void holdJob(AuthCtx authCtx, String jobId, String doHold) throws NotFoundException, AuthorizationException, BadRequestException {
        if (!doHold.equalsIgnoreCase("true") && !doHold.toLowerCase().equalsIgnoreCase("false")) {
            throw new BadRequestException("Value was not true or false, it was " + doHold);
        }
        boolean bHold = Boolean.parseBoolean(doHold);
        Optional<Job> jobOptional = jobRepo.findByJobId(jobId);
        if(!jobOptional.isPresent()) {
            throw new NotFoundException();
        }

        Job job = jobOptional.get();

        if(!authCtx.canHoldJob(job)) {
            throw new AuthorizationException();
        }
        job.setHold(bHold);
    }

    public Map<String, UserInfo> getUserInfoForJobs(List<String> jobIds, String usersEndpoint) {
        Map<String, UserInfo> userInfoMap = new HashMap<>();

        if (usersEndpoint == null) {
            logger.error("User endpoint not specified");
            return userInfoMap;
        }

        StringBuilder query = new StringBuilder();
        List<Job> jobs = new ArrayList<>();
        for (int i = 0; i < jobIds.size(); i++) {
            Optional<Job> jobOptional = jobRepo.findByJobId(jobIds.get(i));
            if (jobOptional.isPresent()) {
                Job job = jobOptional.get();
                String userId = job.getUserId();
                if (i == jobIds.size() - 1) {
                    query.append("name=").append(userId);
                } else {
                    query.append("name=").append(userId).append("&");
                }
                jobs.add(job);
            }
        }
        try {

            Client client = ClientBuilder.newClient();
            Response resp = client.target(String.format("%s?%s", usersEndpoint, query))
                    .request().get();
            if (resp.getStatus() != Response.Status.OK.getStatusCode()) {
                logger.warn(String.format("Received status code %d, user info is not populated", resp.getStatus()));
                return userInfoMap;
            }
            List<UserInfo> userInfoList = resp.readEntity(new GenericType<List<UserInfo>>(){});

            for (UserInfo userInfo : userInfoList) {
                for (Job job : jobs) {
                    if (userInfo.getSub().equals(job.getUserId())) {
                        userInfoMap.put(job.getJobId(), userInfo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoMap;
    }
}
