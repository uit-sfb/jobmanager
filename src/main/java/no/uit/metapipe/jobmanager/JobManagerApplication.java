package no.uit.metapipe.jobmanager;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import no.uit.metapipe.jobmanager.config.JobManagerConfig;
import no.uit.metapipe.jobmanager.core.Attempt;
import no.uit.metapipe.jobmanager.core.DatasetRef;
import no.uit.metapipe.jobmanager.core.Job;
import no.uit.metapipe.jobmanager.db.AttemptRepo;
import no.uit.metapipe.jobmanager.db.DatasetRefRepo;
import no.uit.metapipe.jobmanager.db.TagInfoRepo;
import no.uit.metapipe.jobmanager.resources.JobManagerResource;
import no.uit.metapipe.jobmanager.service.JobManagerService;
import no.uit.metapipe.jobmanager.db.JobRepo;
import no.uit.metapipe.jobmanager.util.auth.AuthCtx;
import no.uit.metapipe.jobmanager.util.auth.OAuth2IntrospectionAuthenticator;
import no.uit.metapipe.jobmanager.workers.JobStateManager;
import no.uit.metapipe.jobmanager.workers.StateObserver;
import no.uit.metapipe.jobmanager.workers.StateObserverImpl;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.security.Principal;
import java.util.EnumSet;

public class JobManagerApplication extends Application<JobManagerConfig> {
    public static void main(final String[] args) throws Exception {
        new JobManagerApplication().run(args);
    }

    private final HibernateBundle<JobManagerConfig> hibernateBundle =
            new HibernateBundle<JobManagerConfig>(Job.class, Attempt.class, DatasetRef.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(JobManagerConfig jobManagerConfig) {
                    return jobManagerConfig.getDataSourceFactory();
                }
            };

    private final FlywayBundle<JobManagerConfig> flywayBundle = new FlywayBundle<JobManagerConfig>() {
        @Override
        public PooledDataSourceFactory getDataSourceFactory(JobManagerConfig jobManagerConfig) {
            return jobManagerConfig.getDataSourceFactory();
        }

        @Override
        public FlywayFactory getFlywayFactory(JobManagerConfig configuration) {
            return configuration.getFlywayFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<JobManagerConfig> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(flywayBundle);
        bootstrap.addBundle(new AssetsBundle("/simpleGUI", "/gui"));
        bootstrap.addBundle(new AssetsBundle("/simpleGUI", "/gui", "simpleGUI.html"));

    }

    @Override
    public void run(JobManagerConfig jobManagerConfig, Environment environment) throws Exception {
        configureCORS(environment);

        DatasetRefRepo datasetRefRepo = new DatasetRefRepo(hibernateBundle.getSessionFactory());
        JobRepo jobRepo = new JobRepo(hibernateBundle.getSessionFactory());
        AttemptRepo attemptRepo = new AttemptRepo(hibernateBundle.getSessionFactory());
        TagInfoRepo tagInfoRepo = new TagInfoRepo(jobManagerConfig.getTags());

        JobManagerService jobManagerService = new JobManagerService(jobRepo, attemptRepo, datasetRefRepo, tagInfoRepo);

        StateObserver jobStateObserver = new StateObserverImpl(jobRepo, attemptRepo, jobManagerConfig.getStateManagerConfig(), jobManagerService);
        JobStateManager stateManager = new JobStateManager(hibernateBundle,
                jobManagerConfig.getStateManagerConfig(),
                jobRepo, attemptRepo, jobStateObserver);
        stateManager.register(environment);

        stateManager.runNow(); // provides quicker feedback if something is wrong

        AuthFilter<String, Principal> oauthCredentialAuthFilter = new OAuthCredentialAuthFilter.Builder<>()
                .setAuthenticator(new OAuth2IntrospectionAuthenticator(jobManagerConfig.getAuth()))
                .setPrefix("Bearer")
                .buildAuthFilter();

        environment.jersey().register(new AuthDynamicFeature(oauthCredentialAuthFilter));
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(AuthCtx.class));
        environment.jersey().register(new JobManagerResource(jobManagerService, jobManagerConfig));
    }

    public static void configureCORS(Environment environment) {
//         Solution taken from: http://stackoverflow.com/a/25801822/1390113

        FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin,Authorization,Pragma,Cache-Control");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }
}
